DSSend is a program for sending and receiving systems over MIDI to the Korg DSS-1 synthesizer. Source code is provided under GNU license.


![DSSend.jpg](https://bitbucket.org/repo/MKA8KM/images/4112874293-DSSend.jpg)


DSSend is written in C++. To build the source code you will also need the following libraries which are available free elsewhere:

- Boost
- JUCE  (for the GUI)
- minizip
- zlib

You should be able to find those libraries easily by searching. After installing those you'll need to adjust the header and library paths to your particular locations. Do this by opening the file DSSend.jucer in JUCE's Introjucer program (Introjucer also needs to be built). Change the paths as needed, save and then open the project in Visual Studio.

I have built DSSend most recently using Visual Studio 2015 Update 1, Boost version 1.57, and JUCE version 4.1. (See the Download section.) For some reason when building with Boost version 1.59 DSSend crashes randomly when loading files and I haven't figured out the cause. At this time Boost 1.60 is available but I haven't tried that yet. So I recommend using 1.57 if you have any problems.

Currently the code only supports the Windows platform. It is almost cross-platform except for the MIDI code (which I wrote myself long ago). It should be pretty straightforward to change this to use the MIDI support available in JUCE and then the program should be fully cross-platform. I just never got around to doing that.

Neither the DSM-1 nor modified versions of the DSS-1 are supported, simply because I didn't have them to work with. Sorry.

Jim