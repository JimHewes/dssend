/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SOUNDFONT_H_INCLUDED
#define SOUNDFONT_H_INCLUDED

#include <string>
#include <vector>
#include <memory>
#include <algorithm>	// for std::max
#include <cstdint>
#include <cassert>

class Chunk
{
public:
	Chunk(const std::string& name = "None")
	{
		SetName(name);
	}
	virtual ~Chunk(){};

	std::string GetName() const
	{
		assert(m_name.length() == 4);
		return m_name;
	}
	void SetName(const std::string& name)
	{
		static const std::string pad = "    ";
		m_name = name.substr(0, 4);
		if (m_name.length() < 4)	// ensure length of 4;
		{
			m_name += pad.substr(0, 4 - m_name.length());
		}
	}

	virtual uint32_t GetSize() const = 0;
	virtual void ToBinary(std::ostream& s) const = 0;

private:
	std::string m_name;		///< The name of this chunk. Always 4 bytes in length.
};

using ChunkUPtr = std::unique_ptr<Chunk>;


class ChunkList : public Chunk
{
public:
	ChunkList(const std::string& name = "None", const std::string& typeName = "None" )
		:Chunk(name)
	{
		SetTypeName(typeName);
	}

	/** Returns the total sizes in byte for this chunk

		This function is purposely named GetSize() rather than GetLength() to distinguish it from the length value that 
		gets written out to a file, which is eight bytes less than the total size of the chunk.
	*/
	uint32_t GetSize() const override
	{
		uint32_t size = 0;		// 4 for the list type name. (The name and length variables themselves don't count as part of the length.
		for (auto& pChunk : m_list)
		{
			size += pChunk->GetSize();
		}

		// It might seem as though only 4 should be added here to account for the list typename because the name and length 
		// are not included in a chunk's reported length. But this GetSize() function is meant to be called by parent
		// chunks that need to calulate the size of everything they are including. So that means every byte.
		// When chunks implement ToBinary() they can write out the correct value to the stream which is 8 bytes less.
		size += 12;
		return size;
	}

	void AddChunk(ChunkUPtr&& pChunk)
	{
		m_list.push_back(std::move(pChunk));
	}

	void SetTypeName(const std::string& name)
	{
		static const std::string pad = "    ";
		m_typeName = name.substr(0, 4);
		if (m_typeName.length() < 4)	// ensure length of 4;
		{
			m_typeName += pad.substr(0, 4 - m_typeName.length());
		}
	}

	void ToBinary(std::ostream& s) const override
	{
		uint32_t length = GetSize() - 8;
		assert(m_typeName.length() == 4);

		s.write((char*)&(GetName()[0]), 4);
		s.write((char*)&length, sizeof(uint32_t));
		s.write((char*)&m_typeName[0], 4);

		for (auto& pChunk : m_list)
		{
			pChunk->ToBinary(s);
		}
	};

	Chunk* GetChunkPtr(size_t index)
	{
		return m_list[index].get();
	}

	size_t GetNumberOfChunks()
	{
		return m_list.size();
	}

private:
	std::string				m_typeName;	///< Chunk type name. Must be 4 characters.
	std::vector<ChunkUPtr>	m_list;
};

class ChunkRiffRoot : public ChunkList
{
public:
	ChunkRiffRoot(const std::string& typeName = "None")
		:ChunkList("RIFF", typeName)
	{
		SetTypeName(typeName);
	}
};

class ChunkSFRoot : public ChunkRiffRoot
{
public:
	ChunkSFRoot()
		:ChunkRiffRoot("sfbk")
	{}
};


class ChunkRiffList : public ChunkList
{
public:
	ChunkRiffList(const std::string& typeName = "None")
		:ChunkList("LIST", typeName)
	{
		SetTypeName(typeName);
	}
};
using ChunkRiffListUPtr = std::unique_ptr<ChunkRiffList>;

class ChunkSFInfo : public ChunkRiffList
{
public:
	ChunkSFInfo()
		:ChunkRiffList("INFO")
	{}
};
using ChunkSFInfoUPtr = std::unique_ptr<ChunkSFInfo>;

class ChunkSFsdta : public ChunkRiffList
{
public:
	ChunkSFsdta()
		:ChunkRiffList("sdta")
	{}
};
using ChunkSFsdtaUPtr = std::unique_ptr<ChunkSFsdta>;


class ChunkSFpdta : public ChunkRiffList
{
public:
	ChunkSFpdta()
		:ChunkRiffList("pdta")
	{}
};
using ChunkSFpdtaUPtr = std::unique_ptr<ChunkSFpdta>;


class Chunk_ifil : public Chunk
{
public:
	Chunk_ifil(uint16_t majorVersion, uint16_t minorVersion)
		: Chunk("ifil")
		, m_majorVersion(majorVersion)
		, m_minorVersion(minorVersion)
	{}

	uint32_t GetSize() const override
	{
		return 12;
	}
	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = 4;
		s.write((char*)&length, sizeof(uint32_t));
		s.write((char*)&m_majorVersion, sizeof(uint16_t));
		s.write((char*)&m_minorVersion, sizeof(uint16_t));
	}

private:
	uint16_t m_majorVersion;
	uint16_t m_minorVersion;
};
using Chunk_ifilUPtr = std::unique_ptr<Chunk_ifil>;

class ChunkString : public Chunk
{
public:
	ChunkString(const std::string& name, const std::string& text)
		: Chunk(name)
		, m_text(text)
	{}

	uint32_t GetSize() const override
	{
		return 8 + m_text.length() + ((m_text.length() % 2 == 0)? 2 : 1);
	}
	void ToBinary(std::ostream& s) const override
	{
		assert(!m_text.empty());

		s.write((char*)&(GetName()[0]), 4);

		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		s.write((char*)m_text.c_str(), m_text.length() + 1);
		if (m_text.length() % 2 == 0)
		{
			s.put('\0');	// add padding byte
		}
	}

private:
	std::string m_text;
};
using ChunkStringUPtr = std::unique_ptr<ChunkString>;

using SampleInfo = struct
{
	uint32_t	start;
	uint32_t	end;
	uint32_t	loopStart;
	uint32_t	loopEnd;
	bool		loopEnabled;
} ;

class Chunk_smpl : public Chunk
{
public:
	Chunk_smpl()
		: Chunk("smpl")
	{}

	uint32_t GetSize() const override
	{
		return 8 + m_data.size() * 2;
	}
	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = m_data.size() * 2;
		s.write((char*)&length, sizeof(uint32_t));
		s.write((char*)&m_data[0], length);
	}
	uint32_t GetNumberOfSamples()
	{
		return m_data.size();
	}
	SampleInfo AddSampleData(std::vector<int16_t> data, bool loopEnabled, uint32_t loopStart, uint32_t loopEnd);

private:
	std::vector<int16_t> m_data;
};
using Chunk_smplUPtr = std::unique_ptr<Chunk_smpl>;

struct PresetHeader
{

	std::string presetName;
	uint16_t preset;
	uint16_t bank;
	uint16_t presetBagNdx;
	uint32_t library;
	uint32_t genre;
	uint32_t morphology;
};

class Chunk_phdr : public Chunk
{
public:
	Chunk_phdr()
		: Chunk("phdr")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 38);
	}

	uint16_t AddPreset(const std::string& name, uint16_t presetNum, uint16_t bankNum, uint16_t pbagIndex)
	{
		m_list.push_back(PresetHeader{ name, presetNum, bankNum, pbagIndex, 0, 0, 0 });
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator(uint16_t bagIndex)
	{
		PresetHeader item{ { "EOP" }, 0, 0, bagIndex, 0, 0, 0 };
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& hdr : m_list)
		{
			// Ensure preset name is padded with zero to 20 characters
			s.write(hdr.presetName.c_str(), hdr.presetName.length());
			for (size_t i = 0; i < 20 - hdr.presetName.length(); ++i)
			{
				s.put('\0');
			}

			s.write((char*)&hdr.preset, sizeof(uint16_t));
			s.write((char*)&hdr.bank, sizeof(uint16_t));
			s.write((char*)&hdr.presetBagNdx, sizeof(uint16_t));
			s.write((char*)&hdr.library, sizeof(uint32_t));
			s.write((char*)&hdr.genre, sizeof(uint32_t));
			s.write((char*)&hdr.morphology, sizeof(uint32_t));
		}

	}

private:

	std::vector<PresetHeader> m_list;
};
using Chunk_phdrUPtr = std::unique_ptr<Chunk_phdr>;


using PresetZone = struct
{
	uint16_t genIndex;
	uint16_t modIndex;
} ;

class Chunk_pbag : public Chunk
{
public:
	Chunk_pbag()
		: Chunk("pbag")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 4);
	}

	uint16_t AddPresetZone(uint16_t generatorIndex, uint16_t modulatorIndex)
	{
		m_list.push_back(PresetZone{ generatorIndex, modulatorIndex});
		return uint16_t(m_list.size() - 1);	// return index.
	}
	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& pz : m_list)
		{
			s.write((char*)&pz, 4);
		}
	}

private:
	std::vector<PresetZone> m_list;
};
using Chunk_pbagUPtr = std::unique_ptr<Chunk_pbag>;


using RangesType = struct
{
	uint8_t byLo;
	uint8_t byHi;
} ;

using GenAmountType = union
{
	RangesType	ranges;
	int16_t		signedAmount;
	uint16_t	unsignedAmount;
} ;

enum class SFGenerator : uint16_t 
{
	none = 0,
	startAddrsOffset = 0,
	pan = 17,
	sustainVolEnv = 37,
	releaseVolEnv = 38,
	instrument = 41,
	keyRange = 43,
	keynum = 46,
	initialAttenuation = 48,
	sampleID = 53,
	sampleModes = 54,
	scaleTuning = 56,
	endOper = 60
};

using GeneratorItem = struct
{
	SFGenerator		gnOper;
	GenAmountType	genAmount;
} ;


class Chunk_pgen : public Chunk
{
public:
	Chunk_pgen()
		: Chunk("pgen")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 4);
	}

	uint16_t AddGenerator(SFGenerator generatorOperator, GenAmountType generatorAmount)
	{
		m_list.push_back(GeneratorItem{ generatorOperator, generatorAmount });
		return uint16_t(m_list.size() - 1);	// return index.
	}
	uint16_t AddGeneratorSigned(SFGenerator generatorOperator, int16_t generatorAmount)
	{
		GeneratorItem item;
		item.gnOper = generatorOperator;
		item.genAmount.signedAmount = generatorAmount;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddGeneratorUnsigned(SFGenerator generatorOperator, uint16_t generatorAmount)
	{
		GeneratorItem item;
		item.gnOper = generatorOperator;
		item.genAmount.unsignedAmount = generatorAmount;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddGeneratorRange(SFGenerator generatorOperator, uint8_t byLo, uint8_t byHi)
	{
		GeneratorItem item;
		item.gnOper = generatorOperator;
		item.genAmount.ranges.byLo = byLo;
		item.genAmount.ranges.byHi = byHi;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator()
	{
		GeneratorItem item{};
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& item : m_list)
		{
			s.write((char*)&item, 4);
		}
	}

private:
	std::vector<GeneratorItem> m_list;

};
using Chunk_pgenUPtr = std::unique_ptr<Chunk_pgen>;


using SFModulator = uint16_t;
using SFTransform = uint16_t;

using ModulatorItem = struct
{
	SFModulator sfModSrcOper;
	SFGenerator sfModDestOper;
	uint16_t modAmount;
	SFModulator sfModAmtSrcOper;
	SFTransform sfModTransOper;
} ;

class Chunk_pmod : public Chunk
{
public:
	Chunk_pmod()
		: Chunk("pmod")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 10);
	}

	uint16_t AddModulator(SFModulator sourceOper, SFGenerator destOperator, uint16_t modAmount, SFModulator modAmountSourceOper, SFTransform modTransformOper)
	{
		m_list.push_back(ModulatorItem{ sourceOper, destOperator, modAmount, modAmountSourceOper, modTransformOper });
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator()
	{
		ModulatorItem item{};
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& item : m_list)
		{
			s.write((char*)&item, 10);
		}
	}

private:
	std::vector<ModulatorItem> m_list;
};
using Chunk_pmodUPtr = std::unique_ptr<Chunk_pmod>;

using InstrumentItem = struct
{
	char		name[20];
	uint16_t	bagindex;
} ;

class Chunk_inst : public Chunk
{
public:
	Chunk_inst()
		: Chunk("inst")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 22);
	}

	uint16_t AddInstrument(const std::string& name, uint16_t bagIndex)
	{
		InstrumentItem inst{}; // empty braces means default initialization
		memcpy(&inst.name[0], name.c_str(), std::min((int)name.length(), 20));
		inst.bagindex = bagIndex;
		m_list.push_back(inst);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator(uint16_t bagIndex)
	{
		InstrumentItem item{ { "EOI" }, bagIndex };
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& item : m_list)
		{
			s.write((char*)&item, 22);
		}
	}

private:
	std::vector<InstrumentItem> m_list;
};
using Chunk_instUPtr = std::unique_ptr<Chunk_inst>;

using InstrumentZone = struct
{
	uint16_t genIndex;
	uint16_t modIndex;
};

class Chunk_ibag : public Chunk
{
public:
	Chunk_ibag()
		: Chunk("ibag")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 4);
	}

	uint16_t AddInstrumentZone(uint16_t generatorIndex, uint16_t modulatorIndex)
	{
		m_list.push_back(InstrumentZone{ generatorIndex, modulatorIndex });
		return uint16_t(m_list.size() - 1);	// return index.
	}
	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& pz : m_list)
		{
			s.write((char*)&pz, 4);
		}
	}

private:
	std::vector<InstrumentZone> m_list;
};

using Chunk_ibagUPtr = std::unique_ptr<Chunk_ibag>;


class Chunk_igen : public Chunk
{
public:
	Chunk_igen()
		: Chunk("igen")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 4);
	}

	uint16_t AddGenerator(SFGenerator generatorOperator, GenAmountType generatorAmount)
	{
		m_list.push_back(GeneratorItem{ generatorOperator, generatorAmount });
		return uint16_t(m_list.size() - 1);	// return index.
	}
	uint16_t AddGeneratorSigned(SFGenerator generatorOperator, int16_t generatorAmount)
	{
		GeneratorItem item;
		item.gnOper = generatorOperator;
		item.genAmount.signedAmount = generatorAmount;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddGeneratorUnsigned(SFGenerator generatorOperator, uint16_t generatorAmount)
	{
		GeneratorItem item;
		item.gnOper = generatorOperator;
		item.genAmount.unsignedAmount = generatorAmount;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}
	
	uint16_t AddGeneratorRange(SFGenerator generatorOperator, uint8_t byLo, uint8_t byHi)
	{
		GeneratorItem item;
		item.gnOper = generatorOperator;
		item.genAmount.ranges.byLo = byLo;
		item.genAmount.ranges.byHi = byHi;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator()
	{
		GeneratorItem item{};
		item.gnOper = SFGenerator::endOper;
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& item : m_list)
		{
			s.write((char*)&item, 4);
		}
	}

private:
	std::vector<GeneratorItem> m_list;
};
using Chunk_igenUPtr = std::unique_ptr<Chunk_igen>;


class Chunk_imod : public Chunk
{
public:
	Chunk_imod()
		: Chunk("imod")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 10);
	}

	uint16_t AddModulator(SFModulator sourceOper, SFGenerator destOperator, uint16_t modAmount, SFModulator modAmountSourceOper, SFTransform modTransformOper)
	{
		m_list.push_back(ModulatorItem{ sourceOper, destOperator, modAmount, modAmountSourceOper, modTransformOper });
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator()
	{
		ModulatorItem item{};
		m_list.push_back(item);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& item : m_list)
		{
			s.write((char*)&item, 10);
		}
	}

private:
	std::vector<ModulatorItem> m_list;
};
using Chunk_imodUPtr = std::unique_ptr<Chunk_imod>;


enum class SFSampleLink : uint16_t
{
	monoSample = 1,
	rightSample = 2,
	leftSample = 4,
	linkedSample = 8,
	RomMonoSample = 32769,
	RomRightSample = 32770,
	RomLeftSample = 32772,
	RomLinkedSample = 32776
};

using SampleItem = struct
{
	char name[20];
	uint32_t	start;
	uint32_t	end;
	uint32_t	loopStart;
	uint32_t	loopEnd;
	uint32_t	sampleRate;
	uint8_t		originalPitch;
	char		pitchCorrection;
	uint16_t	sampleLink;
	SFSampleLink sampleType;
};


class Chunk_shdr : public Chunk
{
public:
	Chunk_shdr()
		: Chunk("shdr")
	{}

	uint32_t GetSize() const override
	{
		return 8 + (m_list.size() * 46);
	}

	uint16_t AddSampleHdr(const SampleItem& sampleItem)
	{
		m_list.push_back(sampleItem);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddSampleHdr(const std::string& name, uint16_t start, uint16_t end, uint16_t loopStart, uint16_t loopEnd, 
		uint16_t sampleRate, uint8_t originalPitch, char pitchCorrection, uint16_t sampleLink, SFSampleLink sampleType)
	{
		SampleItem smpl{ {}, start, end, loopStart, loopEnd, sampleRate, originalPitch, pitchCorrection, sampleLink, sampleType };
		memcpy(&smpl.name[0], name.c_str(), std::min((int)name.length(), 20));
		m_list.push_back(smpl);
		return uint16_t(m_list.size() - 1);	// return index.
	}

	uint16_t AddTerminator()
	{
		SampleItem smpl{ { "EOS" } };
		m_list.push_back(smpl);
		return uint16_t(m_list.size() - 1);	// return index.
	}
	
	void ToBinary(std::ostream& s) const override
	{
		s.write((char*)&(GetName()[0]), 4);
		uint32_t length = GetSize() - 8;
		s.write((char*)&length, sizeof(uint32_t));
		for (auto& item : m_list)
		{
			s.write((char*)&item, 46);	// we cannot use sizeof(SampleItem) here because the compiler pads the structure with 2 extra bytes at the end.
		}
	}

private:
	std::vector<SampleItem> m_list;

};
using Chunk_shdrUPtr = std::unique_ptr<Chunk_shdr>;


#endif  // SOUNDFONT_H_INCLUDED
