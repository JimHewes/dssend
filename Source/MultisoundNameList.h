/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef MULTISOUNDNAMELIST_H_INCLUDED
#define MULTISOUNDNAMELIST_H_INCLUDED

#include <string>
#include <vector>
#include <cstdint>
class DSS1;
class BSONDocument;

struct MultisoundNameListItem
{
	MultisoundNameListItem() :length(0){};
	MultisoundNameListItem(const char* pName, uint32_t len)
		:name(pName, 8)
		, length(len)
	{}
	MultisoundNameListItem(std::string itemName, uint32_t len)
		:name(itemName)
		, length(len)
	{
		assert(name.length() == 8);
	}

	MultisoundNameListItem& operator=(const MultisoundNameListItem& other)
	{
		name = other.name;
		length = other.length;
		return *this;
	}

	static uint32_t  dataSizeInBytes()
	{
		return 14;	// the size that will be transferred over MIDI
	}

	std::string name;
	uint32_t	length;
};

/**
The DSS-1 manual refers to this list of multisound names/lengths as simply a multisound list.
But since we also keep a list of actual Multisounds elsewhere, we'll refer to this list as
a MultisoundNameList to distinguish it.
*/
class MultisoundNameList : public std::vector< MultisoundNameListItem >
{
public:
	void FromDevice(DSS1* pDSS1);
	void ToDevice(DSS1* pDSS1);
	uint32_t GetListDataSize()
	{
		return size() *  MultisoundNameListItem::dataSizeInBytes();
	}

	/**	Gets the maximum size of a MultisoundNameList; this will be 16 MultisoundNameListItems.
	
		This is a bit of a hack for reporting the transfer progress when receiving from the DSS-1. We don't 
		know how big the list is until after we get it. So the maximum size is assumed.
	*/
	uint32_t GetMaxListDataSize()
	{
		return 16 *  MultisoundNameListItem::dataSizeInBytes();
	}
	uint32_t GetPCMDataSizeInSamples();

	BSONDocument ToBSON();
	void FromBSON(const BSONDocument& doc);
	void FromNative(std::vector<uint8_t>& buf);

private:
	uint8_t m_numberOfMultisoundNames;
};



#endif  // MULTISOUNDNAMELIST_H_INCLUDED
