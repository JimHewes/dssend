/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "Program.h"
#include "ProgramParams.h"
#include "System.h"

using std::string;
using std::vector;


/**	Creates an empty program with and I doubt it will be needed. So I might remove it in the future.
*/
Program::Program(const std::string& name)
	:m_programParams(name)
{
}

/**	Creates a program with params but no multisounds and I doubt it will be needed and so I might remove it in the future.
*/
Program::Program(vector<uint8_t>& data, const string& name)
	: m_programParams(data, name)
{
}

/**	Creates a program with params but no multisounds and I doubt it will be needed and so I might remove it in the future.
*/
Program::Program(vector<uint8_t>&& data, const string& name)
	: m_programParams(std::move(data), name)
{
}

/** Copy constructor
*/
Program::Program(const Program& other)
{
	if (this != &other)
	{
		m_programParams = other.m_programParams;
		if (other.m_multisoundList.size() > 0 && other.m_multisoundList[0] != nullptr)
		{
			m_multisoundList.push_back(std::make_unique<Multisound>(*m_multisoundList[0]));
		}
		if (other.m_multisoundList.size() > 1 && other.m_multisoundList[1] != nullptr)
		{
			m_multisoundList.push_back(std::make_unique<Multisound>(*m_multisoundList[1]));
		}
	}
}


/** Move constructor
*/
Program::Program(const Program&& other)
{
	if (this != &other)
	{
		m_programParams = std::move(other.m_programParams);
		m_multisoundList = std::move(m_multisoundList);
	}
}

/** This could be written to get from the DSS1 all of program, multisound list, and multisounds.
	But so far, not completed.
*/
Program::Program(DSS1* pDSS1, uint8_t programIndex, const std::string& name)
{
	pDSS1;
	name;
	programIndex;
	// FromDevice(pDSS1, programIndex, name, nullptr);
}


/**	Constructs a Program in which both oscillators refer to the same Multisound so there
	is only one Multisound.

	The new Program is constructed from a copy of the params and a copy of the Multisound.
	The Program parameters will be changed so that both oscillators have a multisound index 
	of 0, meaning they both refer to the given Multisound.

*/
Program::Program(const ProgramParams& params, MultisoundUPtr& multisound)
{
	m_programParams = params;
	m_multisoundList.push_back(std::make_unique<Multisound>(*multisound));
	m_programParams.SetParameter(OSC1MULTISOUNDNUMBER, 0);
}

/**	Constructs a Program in which each oscillator refers to a different Multisound so there
	are two Multisounds.

	The new Program is constructed from a copy of the params and a copy of the Multisound.
	The Program parameters will be changed so that oscillator 1 will refer to multisound1 (index
	of 0) and oscillator 2 will refer to multisound2 (index	of 1).
	*/
Program::Program(const ProgramParams& params, MultisoundUPtr& multisound1, MultisoundUPtr& multisound2)
{
	m_programParams = params;
	m_multisoundList.push_back(std::make_unique<Multisound>(*multisound1));
	m_multisoundList.push_back(std::make_unique<Multisound>(*multisound2));
	m_programParams.SetParameter(OSC1MULTISOUNDNUMBER, 0);
	m_programParams.SetParameter(OSC2MULTISOUNDNUMBER, 1);
}




Program& Program::operator=(const Program& other)
{
	if (this != &other)
	{
		m_programParams = other.m_programParams;
		if (other.m_multisoundList.size() > 0 && other.m_multisoundList[0] != nullptr)
		{
			m_multisoundList.push_back(std::make_unique<Multisound>(*m_multisoundList[0]));
		}
		if (other.m_multisoundList.size() > 1 && other.m_multisoundList[1] != nullptr)
		{
			m_multisoundList.push_back(std::make_unique<Multisound>(*m_multisoundList[1]));
		}
	}
	return *this;
}

Program& Program::operator=(const Program&& other)
{
	if (this != &other)
	{
		m_programParams = std::move(other.m_programParams);
		m_multisoundList = std::move(m_multisoundList);
	}
	return *this;
}


#if 0
bool Program::TransferToSystem(System* pSystem)
{
	// Check that the multisounds we have will fit.



	// There should be at least one multisound because this function is only meant to be called by a System that is adding the 
	// program, and so the Program should not have been owned by any System and the Program should own it's own Multisounds.
	assert(m_multisoundList[0] || m_multisoundList[1]);

	if (GetParameter(OSC1MULTISOUNDNUMBER) == GetParameter(OSC2MULTISOUNDNUMBER))
	{
		assert(m_multisoundList.size() == 1);
		int newIndex = pSystem->AddMultisound(std::move(m_multisoundList[0]));
	}
}
#endif // 0


uint32_t Program::GetUsedPCMSpaceInSamples()
{
	uint32_t totalSizeInSamples = 0;
	if (m_multisoundList[0])
	{
		totalSizeInSamples += m_multisoundList[0]->GetPCMDataLengthInSamples();
	}
	if (m_multisoundList[1])
	{
		totalSizeInSamples += m_multisoundList[1]->GetPCMDataLengthInSamples();
	}
	return totalSizeInSamples;
}


MultisoundUPtr Program::GetMultisoundCopy(int multisoundIndex)
{
	assert(multisoundIndex != 0 && multisoundIndex != 1);

	return std::make_unique<Multisound>(*m_multisoundList[multisoundIndex]);
}

Multisound* Program::GetMultisoundRef(int multisoundIndex)
{
	assert(multisoundIndex != 0 && multisoundIndex != 1);

	return m_multisoundList[multisoundIndex].get();
}


#if 0

BSONDocument Program::ToBSON()
{
	BSONDocument doc;

	doc.AddElementString("Name", m_name);
	doc.AddElementBinary("Params", m_programData);

	return doc;
}
#endif // 0




