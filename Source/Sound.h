/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SOUND_H_INCLUDED
#define SOUND_H_INCLUDED

#include <ostream>
#include <vector>
#include <cstdint>

/** The Sound class represents a Sound that's part of a DSS-1 Multisound; it is meant to exist only outside of a Multisound and never inside one.

*/
class Sound
{
public:
	Sound();
	Sound(const Sound& other);
	Sound(const Sound&& other);
	Sound(const std::string& name, uint32_t PCMOffset, const std::vector<uint8_t>& params, const std::vector<uint8_t>& pcmData);
	Sound(std::string&& name, uint32_t PCMOffset, std::vector<uint8_t>&& params, std::vector<uint8_t>&& pcmData);

	void SetName(const std::string& name);
	void SetName(std::string&& name);
	void SetParams(const std::vector<uint8_t>& params);
	void SetParams(const std::vector<uint8_t>&& params);
	void SetPCMData(const std::vector<uint8_t>& pcmData);
	void SetPCMData(const std::vector<uint8_t>&& pcmData);

	std::string GetName();
	int8_t GetRelativeTune();
	int8_t GetRelativeLevel();
	bool IsTransposed();
	uint32_t GetSamplesPerSecond();
	uint8_t GetTopMIDIKey();
	uint8_t GetOriginalMIDIKey();
	uint32_t GetLoopStart();
	uint32_t GetLoopLength();
	std::vector<int16_t> GetPCMData();

	void ToWave(std::ostream& os);

private:
	std::string				m_name;		///< Sound names on a floppy disk are limited to 8 chars, but here we allow any length since these names are never transfered over MIDI to the DSS-1.
	std::vector<uint8_t>	m_params;	///< sound parameters in the same format as it comes from the DSS-1 sysex. Always exactly 36 bytes.
	std::vector<uint8_t>	m_pcmData;	///< PCM data as extracted from a Multisound in the same format as it comes from the DSS-1 sysex.
};




#endif  // SOUND_H_INCLUDED
