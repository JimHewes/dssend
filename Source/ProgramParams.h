/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef PROGRAMPARAMS_H_INCLUDED
#define PROGRAMPARAMS_H_INCLUDED

#include <cassert>
#include <cstdint>
#include <string>
#include <vector>
#include <memory>
#include "DSS1.h"

class BSONDocument;

// Note that these are not parameter numbers but offsets into the parameter array.
// So DDL-1 and DDL-2 times must be set in two calls.
enum ProgramParameter
{
	OSC1MIX = 0,
	OSC2MIX = 1,
	DDL1TIMELO = 46,
	DDL1TIMEHI = 47,
	DDL2TIMELO = 53,
	DDL2TIMEHI = 54,
	OSC1MULTISOUNDNUMBER = 60,
	OSC2MULTISOUNDNUMBER = 61,


};
#define DEFAULT_NAME "!NO-NAME"

/** The ProgramParams contains the 80 paramsters of a DSS-1 program without the Multisounds.

	The difference between the Program and ProgramParams classes is that a PRogram will contain a full 
	Multisound when it is passed around outside of a system. But ProgramParams is used when a program is 
	contained within a system as it only has a reference to a Multisound in the system. More than one Program can
	refer to the same multisound in a system, which saves space.\n
	\n
	There is one one strange parameter in the set of 80 that can be retrieved from the DSS-1. That is the 
	MAX OSC BEND RANGE parameter at offset 62. This parameter doesn't correspond to any setting that you can 
	change from the front panel. It is the maximum number of semitones that the sound can be 
	pitch bent and it depends on the original sample rate of both of the Multisounds used by a program (which 
	can be different.) The higher the sample rate, the less far the pitch can be bent. So if the sample rate is 
	32KHz for example, the maximum pitch bend range is 12 semitones. 
	If I load the System A from the first factory disk, the first program "Piano 1" has this parameter set to 
	the value 3. If I send the program back to the DSS-1 and retrieve it again. The value is changed to 12.
	It seems that the DSS-1 always "fixes up" this value when I send it over MIDI. Somehow that value managed 
	to be 3 when it was loaded from the floppy because that's the value I get when I retrieve the System immedately 
	after loding from floppy. And that doesn't really limit the pitch bend range because 
	after loading from floppy I can still adjust the pitch bend range of the Piano 1 program up to 12. 
	I hypothesized that this value only gets adjusted by the DSS-1 if you send the Multisound List or 
	Multisounds AFTER sending the Program---that is when it would need to fix the value if the new Multisounds 
	have a different sample rate. But I tried sending programs both first AND last, retrieving them again, and 
	the value always gets fixed up. So it seems there is no way to control it, and it doesn't have any outward 
	effect anyway. So I'm not going to worry about it. Just be warned that if you send some System to the DSS-1 
	and retrieve it back again, you may not get exactly the same bytes back for this reason. (This parameter is 
	the only exception though.) \n 
	I assume the purpose of this parameter is to to inform some external editor---which has only retrieved the 
	Programs and Multisound list but doesn't have the Multisould Params---how far it's allowed to adjust the 
	pitch bend range in the Program. So if this is true, then it's really meant to be just a read-only value. 
	(Although this doesn't explain why the Piano 1 program of System A on factory disk 1 returns a 3, which is not 
	really a valid value anyway, It should be 7, 12, 19, or 24.)\n
	\n
	I don't know what happens if you try to send the DSS-1 a Program with an out-of-range pitch bend parameter. 
	I haven't tried it. Does it just fix it? Or does it reject the transfer or the following write operation?

@nosubgrouping */
class ProgramParams
{
public:

	/** @name Constructors
	* **/
	//@{
	ProgramParams(const std::string& name = DEFAULT_NAME);
	ProgramParams(std::vector<uint8_t>& data, const std::string& name = DEFAULT_NAME);	///< Creates a program with no multisounds
	ProgramParams(std::vector<uint8_t>&& data, const std::string& name = DEFAULT_NAME);
	ProgramParams(const ProgramParams& other);	// copy
	ProgramParams(const ProgramParams&& other);
	ProgramParams(DSS1* pDSS1,uint8_t programIndex,const std::string& name);	// construct a Program by streaming from the DSS1 device
	ProgramParams(uint8_t programIndex,int systemIndex,std::vector<uint8_t>& buf);	// construct a Program from the native DS1 data in the buffer
	//@}

	ProgramParams& operator=(const ProgramParams& other);
	ProgramParams& operator=(const ProgramParams&& other);

	/** @brief	This is a helper function containing common code and is called by contructors
	*			to initialize the object. It allocate members that need to exist for the
	*			object to be valid.
	*/

	/** This property is intended to give some estimation, for the purposes	of progress status, of how long it takes to 
		transfer the data to and from the device.
	*/
	static int DataSizeInBytes()
	{
		return 80;
	}

	bool IsUsed()
	{
		return 0 != m_name.compare(DEFAULT_NAME);
	}

	/** @brief	Sets the contents of this Program by streaming from the DSS1 device.

		@param[in] pDSS1			A pointer to the DSS1 device to stream the data from.
		@param[in] programIndex		The program index (zero-based, i.e. program number - 1) in the DSS1 from which to read the data.
		@param[in] name				The name of the program. This must be exactly 8 ASCII characters. The name can be obtained using the DSS1::GetProgramNameList() function.
		@param[in] callback			A function of type ProgressCallback that will be called periodically to report the progress of the transfer. Programs are small
									and so there will only be one call but the callback exists for consistency with multisounds.
		@retval true	if the process completed.
		@retval false	if it was canceled by the callback function.
	*/
	void FromDevice(DSS1* pDSS1, uint8_t programIndex, const std::string& name);


	/** @brief Downloads the Program to the DSS-1
	
		@param[in] pDSS1		A pointer to the DSS1 device to stream the data from.
		@param programIndex		The zero-based program slot in which to store this program. Note that the
								program number isn't a property of this object; it only comes into play
								when you Send or Receive this program to the DSS-1 (and follow it up with a Write Request)
		@param[in] callback		A function of type ProgressCallback that will be called periodically to report the progress of the transfer. Programs are small
								and so there will only be one call but the callback exists for consistency with multisounds.
		@retval true	if the process completed.
		@retval false	if it was canceled by the callback function.

	*/
	void ToDevice(DSS1* pDSS1, uint8_t programIndex, ProgressCallback callback);

	//static std::vector<std::string> GetProgramNameList(MidiIoDevice& device, uint8_t channel);

	BSONDocument ToBSON();
	void FromBSON(const BSONDocument& doc);
	void FromNative(uint8_t programIndex,int systemIndex,std::vector<uint8_t>& buf);

	void SetParameter(ProgramParameter param, uint8_t value)
	{
		m_programData[param] = value;
	}
	uint8_t GetParameter(ProgramParameter param)
	{
		return m_programData[param];
	}
	void SetName(const std::string& name)
	{
		assert(name.length() == 8);
		m_name = name;
	}
	std::string GetName()
	{
		return m_name;	
	}


	std::vector<uint8_t> GetData()
	{
		return m_programData;
	}

private:
	/** @brief The name of this program, which is not contained anywhere in the program data.
	This must be exactly 8 ASCII character long. Pad with spaces if necessary.
	*/
	std::string m_name;

	std::vector<uint8_t> m_programData;
};

using ProgramParamsUPtr = std::unique_ptr<ProgramParams>;


class ProgramParamsList : public std::vector< ProgramParams >
{
public:
	ProgramParamsList()
	{
		/** The program list holds 32 programs. This vector is always 32 slots in size because the DSS-1 always has 32 programs even
		if they hold initialized data. The way to tell if a program is unused is if it has the name "!NO-NAME".
		*/
		resize(32);
	}
	ProgramParamsList(const BSONDocument& doc) { FromBSON(doc); }
	ProgramParamsList(int index, std::vector<uint8_t>& buf) { FromNative(index, buf); }

	/** Retrieves all 32 programs from the DSS-1.

		@param[in] pDSS1	A pointer to the DSS1 device to stream the data from.
		@retval true		If the process completed.
		@retval false		If it was canceled by the callback function.
	*/		bool FromDevice(DSS1* pDSS1, ProgressCallback callback);

	/** Downloads all the program in the program list to the DSS-1.

		@param[in] pDSS1	A pointer to the DSS1 device to stream the data from.
		@retval true		If the process completed.
		@retval false		If it was canceled by the callback function.
	*/	
	bool ToDevice(DSS1* pDSS1, ProgressCallback callback);

	BSONDocument ToBSON();
	void FromBSON(const BSONDocument& doc);
	void FromNative(int index, std::vector<uint8_t>& buf);
};

#endif  // PROGRAMPARAMS_H_INCLUDED
