/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#if defined(_DEBUG)
#include "../JuceLibraryCode/JuceHeader.h"	// include just for DBG()
#else
#define DBG(x)
#endif

#include <fstream>
#include <boost/locale.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <algorithm>
#include "Multisound.h"
#include "Sound.h"
#include "Soundfont.h"
using boost::filesystem::ifstream;
using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::gregorian::day_clock;
using std::vector;
using std::string;



Multisound::Multisound()
	:m_multisoundParams(18)
{
	// set default multisound name
	memcpy(&m_multisoundParams[1], "Init    ", 8);

}

Multisound::Multisound(const Multisound& other)
{
	m_multisoundParams = other.m_multisoundParams;
	m_multisoundPCMData = other.m_multisoundPCMData;
}

int Multisound::SoundWordLength(int soundIndex)
{
	return ToLength(&m_multisoundParams[(soundIndex * 36) + 22]);
}

int Multisound::SoundPlayStartAddress(int soundIndex)
{
	return ToAddress(&m_multisoundParams[(soundIndex * 36) + 28]);
}

uint32_t Multisound::SoundPCMStartAddress(int soundIndex)
{
	assert(soundIndex < GetNumberOfSounds());
	uint32_t startAddress = 0;
	for (int i = 0; i < soundIndex; ++i)
	{
		startAddress += SoundWordLength(i);
	}

	return startAddress;
}


/**	Returns multisound PCM data length in samples. Each sample is two bytes.
*/
int Multisound::GetPCMDataLengthInSamples()
{
	return ToLength(&m_multisoundParams[9]);
}

uint8_t Multisound::GetNumberOfSounds()
{
	return m_multisoundParams[15] & 0x3F;
}

Sound Multisound::GetSound(uint8_t index)
{
	assert(index < GetNumberOfSounds());
	
	auto params = std::vector<uint8_t>(36);
	auto indexToSoundParams = 17 + (index * 36);
	memcpy(&params[0], &m_multisoundParams[indexToSoundParams], params.size());

	auto pcmData = std::vector<uint8_t>(2 * SoundWordLength(index));
	auto indexToPCMData = SoundPCMStartAddress(index) * 2;
	memcpy(&pcmData[0], &m_multisoundPCMData[indexToPCMData], pcmData.size());

	Sound snd("", SoundPCMStartAddress(index), std::move(params), std::move(pcmData));

	// Sounds within Multisounds don't have names so let's make the name the same as the Multisound with the index appended.
	char* noteNames[12] = { "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B" };

	std::ostringstream os;
	os << "[" << GetName() << "]" << "_" << noteNames[snd.GetOriginalMIDIKey() % 12] << (int)(snd.GetOriginalMIDIKey() / 12) - 1;
	snd.SetName(os.str());
	return snd;
}

// uint8_t Multisound::GetSoundBottomMIDIKey(uint8_t originalKey)
// {
// 	uint8_t numSounds = GetNumberOfSounds();
// 
// 	for (auto snd = 0; snd < numSounds; ++snd)
// 	{
// 
// 	}
// 	return 0;
// }


bool Multisound::GetLoopEnabled()
{
	return (m_multisoundParams[15] & 0xC0) == 0x40;
}

bool Multisound::Equals(Multisound* pOther)
{
	bool result = false;
	if ((m_multisoundParams.size() == pOther->m_multisoundParams.size()) &&
		(m_multisoundPCMData.size() == pOther->m_multisoundPCMData.size()))
	{
		// The first byte is the Multisound number and might not be the same for two Multisounds that are otherwise the same.
		// It depends on which System the Multisound is being used by. So don't count that in the comparison.
		if (0 == memcmp(&m_multisoundParams[1], &pOther->m_multisoundParams[1], m_multisoundParams.size() - 1))
		{
			if (0 == memcmp(&m_multisoundPCMData[0], &pOther->m_multisoundPCMData[0], m_multisoundPCMData.size()))
			{
				result = true;
			}
		}
	}

#if 0	// for debugging
	int paramBytesDifferent = 0;
	int pcmBytesDifferent = 0;
	if (!result)
	{
		for (uint16_t i = 0; i < m_multisoundParams.size(); ++i)
		{
			if (m_multisoundParams[i] != pOther->m_multisoundParams[i])
			{
				paramBytesDifferent++;
			}
		}
		for (uint16_t i = 0; i < m_multisoundPCMData.size(); ++i)
		{
			if (m_multisoundPCMData[i] != pOther->m_multisoundPCMData[i])
			{
				pcmBytesDifferent++;
			}
		}

	}
#endif

	return result;
}

void Multisound::DumpParams(uint32_t absolutePCMDataStartAddress)
{
	std::ostringstream os;

	os << "Multisound " << (int)(m_multisoundParams[0]) << "\n";
	os << "  Name: " << GetName() << "\n";
	os << "  Length (samples): " << GetPCMDataLengthInSamples() << "\n";

	auto realStartAddress = absolutePCMDataStartAddress;
	uint32_t totalSoundWordLength = 0;
	for (int s = 0; s < GetNumberOfSounds(); ++s)
	{
		os << "  Sound " << s + 1 << "\n";
		os << "     RealStartAddress:      " << realStartAddress << "\n";
		os << "     SoundPlayStartAddress: " << SoundPlayStartAddress(s) << "\n";
		os << "     SoundWordLength:       " << SoundWordLength(s) << "\n";
		realStartAddress += SoundWordLength(s);
		totalSoundWordLength += SoundWordLength(s);
	}
	os << "  Total SoundWordLength: " << totalSoundWordLength << "\n";
	DBG(os.str().c_str());	// DBG() adds a linefeed
}

bool Multisound::FromDevice(DSS1* pDSS1, uint8_t multisoundIndex, uint32_t absolutePCMDataStartAddress, ProgressCallback callback)
{

	m_multisoundParams = pDSS1->GetMultisoundParamData(multisoundIndex);
	
	DumpParams(absolutePCMDataStartAddress);

	// This is just a check to confirm that the multisound length as specified in the multisound parameters 
	// is the same as when we add up all the length of all the individual sounds.
	// However....sometimes they are not equal. In disk KSDU-38, System B there is a multisound where they are not equal.
	// So we can't consider this an error. But in this case the total sound word length is less than the PCM data length.
	// This means they just wasted space and as long as we get all the PCM data it should be fine. Just output a debug message.
	// What we need to consider an error is when the total sound length is greater than the PCM data. In that case do an assert.
	uint32_t totalSoundWordLength = 0;
	uint32_t totalPCMDataLengthInSamples = GetPCMDataLengthInSamples();
	for (int i = 0; i < GetNumberOfSounds(); ++i)
	{
		totalSoundWordLength += SoundWordLength(i);
	}
	if (totalSoundWordLength != totalPCMDataLengthInSamples)
	{
		string msg = "Total sound length is not equal to PCM data length in Multisound ";
		msg += GetName();
		DBG(msg);
	}
	assert(totalSoundWordLength <= totalPCMDataLengthInSamples);


	m_multisoundPCMData.resize(totalPCMDataLengthInSamples * 2);	// Eacc sample is two bytes

	// Divide up the total PCM transfer into smaller chuncks so we can report progress regularly.
	const uint32_t samplesPerChunk = 2000;
	uint32_t bufferIndex = 0;
	uint32_t samplesThisChunk = 0;
	uint32_t PCMDataLengthInSamples = totalPCMDataLengthInSamples;
	static char msg[] = "Multisound: xxxxxxxx";
	//msg[11] = ((int)(multisoundIndex / 10)) | 0x30;
	//msg[12] = (multisoundIndex % 10) | 0x30;
	memcpy(&msg[12], GetName().c_str(), 8);

	bool continueReceive = true;
			
	while (PCMDataLengthInSamples > 0)
	{
		samplesThisChunk = (PCMDataLengthInSamples < samplesPerChunk) ? PCMDataLengthInSamples : samplesPerChunk;

#if defined(_DEBUG)
		std::ostringstream os;
		os << "Reading PCM data starting at [" << absolutePCMDataStartAddress << "] for length " << samplesThisChunk;
		DBG(os.str().c_str());	// DBG() adds a linefeed.
#endif

		if (callback)
		{
			continueReceive = callback(msg, (totalPCMDataLengthInSamples - PCMDataLengthInSamples) * 2, totalPCMDataLengthInSamples * 2, false);
			if (!continueReceive)
			{
				break;
			}
		}
		pDSS1->GetPCMData(absolutePCMDataStartAddress, samplesThisChunk, m_multisoundPCMData, bufferIndex);

		bufferIndex += samplesThisChunk * 2;
		absolutePCMDataStartAddress += samplesThisChunk;
		PCMDataLengthInSamples -= samplesThisChunk;
	}

	if (callback && continueReceive)
	{
		continueReceive = callback(msg, (totalPCMDataLengthInSamples - PCMDataLengthInSamples) * 2, totalPCMDataLengthInSamples * 2, true);
	}

	return continueReceive;
}

bool Multisound::ToDevice(DSS1* pDSS1, uint8_t multisoundIndex, uint32_t absolutePCMDataStartAddress, ProgressCallback callback)
{
	assert(m_multisoundParams[0] == multisoundIndex);
	assert(size_t(GetPCMDataLengthInSamples() * 2) == m_multisoundPCMData.size());

	m_multisoundParams[0] = multisoundIndex;
	pDSS1->SendMultisoundParamData(m_multisoundParams);


	// Divide up the total PCM transfer into smaller chuncks so we can report progress regularly.
	const uint32_t samplesPerChunk = 2000;
	uint32_t bufferIndex = 0;
	uint32_t samplesThisChunk = 0;
	uint32_t totalPCMDataLengthInSamples = GetPCMDataLengthInSamples();
	uint32_t PCMDataLengthInSamples = totalPCMDataLengthInSamples;
	static char msg[] = "Multisound: xxxxxxxx";
	//msg[11] = ((int)(multisoundIndex / 10)) | 0x30;
	//msg[12] = (multisoundIndex % 10) | 0x30;
	memcpy(&msg[12], GetName().c_str(), 8);
	bool continueSend = true;	// default to true because PCMDataLengthInSamples might be zero for multisounds named "?NO_MSND". (Yes, native files sometimes have these.)
	while (PCMDataLengthInSamples > 0)
	{
		samplesThisChunk = (PCMDataLengthInSamples < samplesPerChunk) ? PCMDataLengthInSamples : samplesPerChunk;

#if defined(_DEBUG)
		std::ostringstream os;
		os << "Writing PCM data starting at [" << absolutePCMDataStartAddress << "] for length " << samplesThisChunk << "\n";
		DBG(os.str().c_str());
#endif

		if (callback)
		{
			continueSend = callback(msg, (totalPCMDataLengthInSamples - PCMDataLengthInSamples) * 2, totalPCMDataLengthInSamples * 2, false);
			if (!continueSend)
			{
				break;
			}
		}
		pDSS1->SendPCMData(absolutePCMDataStartAddress, samplesThisChunk, m_multisoundPCMData, bufferIndex);

		bufferIndex += samplesThisChunk * 2;
		absolutePCMDataStartAddress += samplesThisChunk;
		PCMDataLengthInSamples -= samplesThisChunk;

	}
	if (callback && continueSend)
	{
		continueSend = callback(msg, (totalPCMDataLengthInSamples - PCMDataLengthInSamples) * 2, totalPCMDataLengthInSamples * 2, true);
	}
	return continueSend;
}

BSONDocument Multisound::ToBSON()
{
	BSONDocument doc;

	doc.AddElementBinary("Params", m_multisoundParams);
	doc.AddElementBinary("PCMData", m_multisoundPCMData);

	return doc;
}

void Multisound::FromBSON(const BSONDocument& doc)
{
	BSONElementBinary* pParamsElement = (BSONElementBinary*)doc.GetElement("Params");
	m_multisoundParams = pParamsElement->GetData();

	BSONElementBinary* pPCMElement = (BSONElementBinary*)doc.GetElement("PCMData");
	m_multisoundPCMData = pPCMElement->GetData();

	DumpParams(0);

}

void Conv3ByteTo6Byte(uint8_t* pDest,uint8_t* pSrc)
{
	*pDest++ = (uint8_t)((*pSrc & 0xFE) >> 1);
	*pDest++ = (uint8_t)((*pSrc++ & 0x01) << 6);
	*pDest++ = (uint8_t)((*pSrc & 0xFE) >> 1);
	*pDest++ = (uint8_t)((*pSrc++ & 0x01) << 6);
	*pDest++ = (uint8_t)((*pSrc & 0x06) >> 1);
	*pDest = (uint8_t)((*pSrc & 0x01) << 6);
}

void Multisound::FromNative(const std::string& filepath,uint8_t multisoundNumber, const string& name)
{
	m_multisoundParams.clear();
	m_multisoundPCMData.clear();
	m_multisoundParams.resize(17, 0);	// clear to zero

	m_multisoundParams[0] = multisoundNumber;

	// The multisound name from the file (at index 1) should not be used because it is the same 
	// as the filename itself, and the filename might have been altered due to illegal characters in 
	// the multisound name. For example, ?NO-NAME is converted to !NO-NAME for the .DMS filename 
	// because ? is an illegal filename character on Windows. But we want to use the real name.
	memcpy(&m_multisoundParams[1], name.data(), 8);			// name

	ifstream inFile(filepath,std::ios::binary | std::ios::in);
	if (!inFile)
	{
		return;
//		throw std::runtime_error(string("Unable to open the file: ").append(filepath));
	}

	inFile.seekg(0,inFile.end);
	uint32_t realFileSize = (uint32_t)inFile.tellg();	// tellg() actually returns a file position, but as long as we open in binary mode it should be equal to the file offset.
	inFile.seekg(0,inFile.beg);

	if (realFileSize == 0)
	{
		return;
//		throw std::runtime_error(string("File size is 0 of file: ").append(filepath));
	}

	vector<uint8_t> buf;
	buf.resize(realFileSize);
	inFile.read((char*)&buf[0],realFileSize);
	inFile.close();

	Conv3ByteTo6Byte(&m_multisoundParams[9],&buf[8]);	// length
	int length = buf[8] + (buf[9] << 8) + (buf[10] << 16);
	m_multisoundParams[15] = buf[11];					// loop, #sounds
	int numSounds = buf[11] & 0x3F;
	m_multisoundParams[16] = buf[12];					// max interval

	int destIndex = 17;
	int srcIndex = 13;
	m_multisoundParams.resize(17 + numSounds * 36);
	for (int snd = 0; snd < numSounds; ++snd)
	{
		for (int i = 0; i < 5; ++i)
		{
			m_multisoundParams[destIndex++] = buf[srcIndex++];
		}
		for (int i = 0; i < 5; ++i)
		{
			Conv3ByteTo6Byte(&m_multisoundParams[destIndex],&buf[srcIndex]);
			destIndex += 6;
			srcIndex += 3;
		}
		m_multisoundParams[destIndex++] = buf[srcIndex++];	// sampling freq
	}
	
	m_multisoundPCMData.resize(length * 2);
	destIndex = 0;
	srcIndex = 349;

	for (int i = 0; i < length; ++i)
	{
		m_multisoundPCMData[destIndex++] = (buf[srcIndex] >> 2) + ((buf[srcIndex + 1] & 1) << 6);
		srcIndex++;
		m_multisoundPCMData[destIndex++] = (buf[srcIndex++] >> 1);
	}

	DumpParams(0);
}


void Multisound::ToSoundfont(std::ostream& os)
{
	Chunk_shdrUPtr pShdr = std::make_unique<Chunk_shdr>();
	Chunk_smplUPtr pSmpl = std::make_unique<Chunk_smpl>();
	Chunk_ibagUPtr pIbag = std::make_unique<Chunk_ibag>();
	Chunk_igenUPtr pIgen = std::make_unique<Chunk_igen>();
	Chunk_imodUPtr pImod = std::make_unique<Chunk_imod>();
	Chunk_instUPtr pInst = std::make_unique<Chunk_inst>();

	// Add general instrument zone
	pIgen->AddGeneratorSigned(SFGenerator::releaseVolEnv, 3600);	// release time 8 seconds
	pIgen->AddGeneratorSigned(SFGenerator::sustainVolEnv, 0);		// sustain level at max

	uint8_t currentBottomKey = 0;
	uint8_t numSounds = GetNumberOfSounds();
	for (uint8_t s = 0; s < numSounds; ++s)
	{
		Sound snd = GetSound(s);

		SampleInfo sampleInfo = pSmpl->AddSampleData(	snd.GetPCMData(), 
														true, 
														snd.GetLoopStart(), 
														snd.GetLoopStart() + snd.GetLoopLength());

		auto item = SampleItem{};
		auto nameLength = std::min(snd.GetName().length(), (size_t)20) ;
		memcpy(&item.name[0], snd.GetName().c_str(), nameLength);
		if (nameLength < 20)
		{
			memset(&item.name[nameLength], 0, 20 - nameLength);	// pad with 0
		}

		item.start = sampleInfo.start;
		item.end = sampleInfo.end;
		item.loopStart = sampleInfo.loopStart;
		item.loopEnd = sampleInfo.loopEnd;
		item.originalPitch = snd.GetOriginalMIDIKey();
		item.sampleRate = snd.GetSamplesPerSecond();
		item.pitchCorrection = snd.GetRelativeTune();
		item.sampleLink = 0;	// not used
		item.sampleType = SFSampleLink::monoSample;

		uint16_t sampleIndex = pShdr->AddSampleHdr(item);

		uint16_t iGenIndex = pIgen->AddGeneratorRange(SFGenerator::keyRange, currentBottomKey, snd.GetTopMIDIKey());
		currentBottomKey = snd.GetTopMIDIKey() + 1;
		//pIgen->AddGeneratorUnsigned(SFGenerator::keynum, snd.GetOriginalMIDIKey());
		pIgen->AddGeneratorUnsigned(SFGenerator::scaleTuning, snd.IsTransposed() ? 100 : 0);
		pIgen->AddGeneratorUnsigned(SFGenerator::initialAttenuation, (uint16_t)round((64 - snd.GetRelativeLevel()) * 1440.0 / 63.0));
		pIgen->AddGeneratorUnsigned(SFGenerator::sampleModes, GetLoopEnabled() ? 1 : 0);

		// sampleID must be the last generator added for a zone
		pIgen->AddGeneratorUnsigned(SFGenerator::sampleID, sampleIndex);

		// Add an instrument zone for this sample.
		pIbag->AddInstrumentZone(iGenIndex, 0);
	}

	// Add terminators
	pShdr->AddTerminator();
	uint16_t igenTermIndex = pIgen->AddTerminator();
	uint16_t imodTermIndex = pImod->AddTerminator();	// imod doesn't need any entries, just a terminator
	uint16_t instZoneTermIndex = pIbag->AddInstrumentZone(igenTermIndex, imodTermIndex);	// terminating zone points to terminating entries in imod and igen


	pInst->AddInstrument(GetName() + "-Inst", 0);
	pInst->AddTerminator(instZoneTermIndex);		// points to terminator in ibag



	Chunk_phdrUPtr pPhdr = std::make_unique<Chunk_phdr>();
	Chunk_pbagUPtr pPbag = std::make_unique<Chunk_pbag>();
	Chunk_pgenUPtr pPgen = std::make_unique<Chunk_pgen>();
	Chunk_pmodUPtr pPmod = std::make_unique<Chunk_pmod>();

	pPmod->AddTerminator();		// imod doesn't need any entries, just a terminator
	uint16_t pgenIndex = pPgen->AddGeneratorUnsigned(SFGenerator::instrument, 0);	// the one and only instrument in this soundfont
	uint16_t presetZoneIndex = pPbag->AddPresetZone(pgenIndex, 0);

	uint16_t pgenTermIndex = pPgen->AddTerminator();
	uint16_t presetZoneTermIndex = pPbag->AddPresetZone(pgenTermIndex, 0);	// arr terminating zone

	pPhdr->AddPreset(GetName(), 0, 0, presetZoneIndex);
	pPhdr->AddTerminator(presetZoneTermIndex);


	Chunk_ifilUPtr pIfil = std::make_unique<Chunk_ifil>(2, 1);	// version 2.01 of the SF spec
	ChunkStringUPtr pINAM = std::make_unique<ChunkString>("INAM", GetName());
	ChunkStringUPtr pIsng = std::make_unique<ChunkString>("isng", "EMU8000");
	ChunkStringUPtr pIENG = std::make_unique<ChunkString>("IENG", "Jim Hewes");
	ChunkStringUPtr pICOP = std::make_unique<ChunkString>("ICOP", "Copyright (c) 2014 Jim Hewes.");
	ChunkStringUPtr pISFT = std::make_unique<ChunkString>("ISFT", "DSSend");

	char* months[] = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
	ptime nowUtc(day_clock::universal_day(), second_clock::universal_time().time_of_day());
	std::ostringstream date;
	date << string(months[nowUtc.date().month()]);	// Jan = 1, Feb = 2, etc.
	date << " "<< nowUtc.date().day() << ", " << nowUtc.date().year();

	ChunkStringUPtr pICRD = std::make_unique<ChunkString>("ICRD", date.str());
	ChunkStringUPtr pICMT = std::make_unique<ChunkString>("ICMT", "This soundfont is based on a multisound from the Korg DSS-1");

	// Now put all the chunks into the lists
	ChunkSFInfoUPtr pInfo = std::make_unique<ChunkSFInfo>();
	pInfo->AddChunk(std::move(pIfil));
	pInfo->AddChunk(std::move(pINAM));
	pInfo->AddChunk(std::move(pIsng));
	pInfo->AddChunk(std::move(pIENG));
	pInfo->AddChunk(std::move(pICOP));
	pInfo->AddChunk(std::move(pISFT));
	pInfo->AddChunk(std::move(pICRD));
	pInfo->AddChunk(std::move(pICMT));

	ChunkSFsdtaUPtr pSdta = std::make_unique<ChunkSFsdta>();
	pSdta->AddChunk(std::move(pSmpl));

	ChunkSFpdtaUPtr pPdta = std::make_unique<ChunkSFpdta>();
	pPdta->AddChunk(std::move(pPhdr));
	pPdta->AddChunk(std::move(pPbag));
	pPdta->AddChunk(std::move(pPgen));
	pPdta->AddChunk(std::move(pPmod));
	pPdta->AddChunk(std::move(pInst));
	pPdta->AddChunk(std::move(pIbag));
	pPdta->AddChunk(std::move(pIgen));
	pPdta->AddChunk(std::move(pImod));
	pPdta->AddChunk(std::move(pShdr));

	// Now add everything into the root
	ChunkSFRoot root;
	root.AddChunk(std::move(pInfo));
	root.AddChunk(std::move(pSdta));
	root.AddChunk(std::move(pPdta));

	root.ToBinary(os);
}
