/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "ProgramParams.h"
#include <string>
#include <vector>
#include <cstdint>
#include "bson.h"
#if defined(_DEBUG)
#include "../JuceLibraryCode/JuceHeader.h"
#else
#define DBG(x)
#endif

using std::string;
using std::vector;

uint8_t defaultParams[80] = {
	100, 0, 0, 0, 1, 1, 127, 0, 0, 0,
	44, 0, 0, 0, 63, 63, 63, 63, 0, 0,
	50, 0, 63, 63, 63, 63, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	2, 0, 4, 4, 20, 20, 72, 1, 0, 0,
	0, 0, 0, 72, 1, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 4, 1, 1, 0, 0, 3,
	18, 0, 0, 3, 1, 0, 7, 0, 1, 3 };

ProgramParams::ProgramParams(const std::string& name)
	:m_programData(80)
{
	m_name = name;
	memcpy(&m_programData[0], defaultParams, 80);
}

ProgramParams::ProgramParams(vector<uint8_t>& data, const string& name)
	:m_programData(80)
{
	m_name = name;
	memcpy(&m_programData[0], &data[0], 80);
}

ProgramParams::ProgramParams(vector<uint8_t>&& data, const string& name)
{
	m_name = name;
	m_programData = std::move(data);
}

ProgramParams::ProgramParams(const ProgramParams& other)
{
	if (this != &other)
	{
		m_name = other.m_name;
		m_programData = other.m_programData;
	}
}

ProgramParams::ProgramParams(const ProgramParams&& other)
{
	if (this != &other)
	{
		m_name = other.m_name;
		m_programData = std::move(other.m_programData);
	}
}

ProgramParams::ProgramParams(DSS1* pDSS1, uint8_t programIndex, const std::string& name)
{
	FromDevice(pDSS1, programIndex, name);
}

ProgramParams::ProgramParams(uint8_t programIndex,int systemIndex,std::vector<uint8_t>& buf)
{
	FromNative(programIndex, systemIndex, buf);
}


ProgramParams& ProgramParams::operator=(const ProgramParams& other)
{
	if (this != &other)
	{
		m_name = other.m_name;
		m_programData = other.m_programData;
	}
	return *this;
}

ProgramParams& ProgramParams::operator=(const ProgramParams&& other)
{
	if (this != &other)
	{
		m_name = other.m_name;
		m_programData = std::move(other.m_programData);
	}
	return *this;
}


void ProgramParams::FromDevice(DSS1* pDSS1, uint8_t programIndex, const std::string& name)
{
	assert(pDSS1);
	assert(programIndex < 32);
	assert(name.length() == 8);

	m_name = name;
	m_programData = pDSS1->GetProgramParamData(programIndex);
}

void ProgramParams::ToDevice(DSS1* pDSS1, uint8_t programIndex, ProgressCallback callback)
{
	assert(pDSS1);
	assert(programIndex < 32);
	assert(m_name.length() == 8);

	pDSS1->SendProgramParamData(programIndex, m_name, m_programData);

}


BSONDocument ProgramParams::ToBSON()
{
	BSONDocument doc;

	doc.AddElementString("Name", m_name);
	doc.AddElementBinary("Params", m_programData);

	return doc;
}

void ProgramParams::FromBSON(const BSONDocument& doc)
{
	BSONElementString* pNameElement = (BSONElementString*)doc.GetElement("Name");
	memcpy(&m_name[0], pNameElement->GetValue().c_str(), 8);

	BSONElementBinary* pParamsElement = (BSONElementBinary*)doc.GetElement("Params");
	m_programData = pParamsElement->GetData();
}

void ProgramParams::FromNative(uint8_t programIndex,int systemIndex,vector<uint8_t>& buf)
{
	assert(programIndex >= 0 && programIndex <= 31);
	assert(systemIndex >= 0 && systemIndex <= 3);

	int nameOffset = systemIndex * 0x100 + programIndex * 8;
	m_name.resize(8);
	memcpy(&m_name[0], &buf[nameOffset], 8);

	int paramOffset = 1729 + (systemIndex * 32 * 80) + (programIndex * 80);
	m_programData.resize(80);
	memcpy(&m_programData[0], &buf[paramOffset], 80);

	DBG("OSC1: " << (int)m_programData[60] << ", OSC2: " << (int)m_programData[61]);
}

bool ProgramParamsList::FromDevice(DSS1* pDSS1, ProgressCallback callback)
{
	assert(pDSS1);
	assert(size() == 32);

	uint32_t totalDataSize = size() * ProgramParams::DataSizeInBytes() + (32 * 8);	// 32 * 8 is for the program name list;
	uint32_t bytesSoFar = 0;
	if (callback && !callback("Program name list", bytesSoFar, totalDataSize, false))
	{
		return false;
	}
	vector<string> progNameList = pDSS1->GetProgramNameList();
	bytesSoFar += 32 * 8;

	for (size_t i = 0; i < progNameList.size(); ++i)
	{
		string txt = "Program: ";
		txt += progNameList[i];
		bytesSoFar += ProgramParams::DataSizeInBytes();
		if (callback && !callback(txt, bytesSoFar, totalDataSize, false))
		{
			return false;
		}

		at(i) = ProgramParams(pDSS1, static_cast<uint8_t>(i), progNameList[i]);
	}
	return true;
}

bool ProgramParamsList::ToDevice(DSS1* pDSS1, ProgressCallback callback)
{
	assert(pDSS1);
	assert(size() == 32);
	
	uint32_t totalDataSize = size() * ProgramParams::DataSizeInBytes() + (32 * 8);	// 32 * 8 is for the program name list;
	uint32_t bytesSoFar = 0;

	for (size_t i = 0; i < size(); ++i)
	{
		string txt = "Program: ";
		txt += at(i).GetName();

		if (callback && !callback(txt, bytesSoFar, totalDataSize, false))
		{
			return false;
		}
		at(i).ToDevice(pDSS1, (uint8_t)i, nullptr);

		bytesSoFar += ProgramParams::DataSizeInBytes();

	}
	return true;
}


BSONDocument ProgramParamsList::ToBSON()
{
	BSONDocument doc;

	for (size_t i = 0; i < size(); ++i)
	{
		// We're adding ProgramParams as array elements. So the name of each element is a text representation of the array index.
		doc.AddElementDocument(i, at(i).ToBSON());
	}
	return doc;
}

void ProgramParamsList::FromBSON(const BSONDocument& doc)
{
	for (size_t i = 0; i < size(); ++i)
	{
		BSONElementDocument* pProgramParamsDoc = (BSONElementDocument*)doc.GetElement(i);
		at(i).FromBSON(pProgramParamsDoc->GetDoc());
	}

}

void ProgramParamsList::FromNative(int systemIndex, std::vector<uint8_t>& buf)
{
	for (size_t programIndex = 0; programIndex < size(); ++programIndex)
	{
		at(programIndex).FromNative((uint8_t)programIndex,systemIndex,buf);
	}
}