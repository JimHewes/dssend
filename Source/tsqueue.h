/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef TSQUEUE_H_INCLUDED
#define TSQUEUE_H_INCLUDED

#include <queue>
#include <deque>
#include <boost/thread.hpp>

template <class _Ty>
class tsqueue
{
private:
	std::deque<_Ty> c;
	boost::mutex m_mutex;

public:
	typedef tsqueue<_Ty> _Myt;
	typedef typename std::deque<_Ty>::value_type value_type;
	typedef typename std::deque<_Ty>::size_type size_type;
	typedef typename std::deque<_Ty>::reference reference;
	typedef typename std::deque<_Ty>::const_reference const_reference;

	tsqueue()
		: c()
	{	// construct with empty container
	}

	tsqueue(const _Myt& _Right)
		: c(_Right.c)
	{	// construct by copying _Right container
	}

	tsqueue(_Myt&& _Right)
		: c(std::move(_Right.c))
	{	// construct by moving _Right
	}
	explicit tsqueue(std::deque<_Ty>&& _Cont)
		: c(_std::move(_Cont))
	{	// construct by moving specified container
	}
	_Myt& operator=(const _Myt& _Right)
	{	// assign by copying _Right
		boost::mutex::scoped_lock lock(m_mutex);
		c = _Right.c;
		return (*this);
	}
	_Myt& operator=(_Myt&& _Right)
	{	// assign by moving _Right
		boost::mutex::scoped_lock lock(m_mutex);
		c = std::move(_Right.c);
		return (*this);
	}


	void push(const _Ty& elem)
	{
		boost::mutex::scoped_lock lock(m_mutex);
		c.push_back(elem);
	}
	void push(const _Ty&& elem)
	{
		boost::mutex::scoped_lock lock(m_mutex);
		c.push_back(std::move(elem));
	}

	reference front()
	{	// return first element of mutable queue
		boost::mutex::scoped_lock lock(m_mutex);
		return (c.front());
	}

	const_reference front() const
	{	// return first element of nonmutable queue
		boost::mutex::scoped_lock lock(m_mutex);
		return (c.front());
	}
	void pop()
	{	// erase element at end
		boost::mutex::scoped_lock lock(m_mutex);
		c.pop_front();
	}
	bool empty()
	{	
		boost::mutex::scoped_lock lock(m_mutex);
		// test if queue is empty
		return (c.empty());
	}

	size_type size()
	{	// return length of queue
		boost::mutex::scoped_lock lock(m_mutex);
		return (c.size());
	}



};


#endif  // TSQUEUE_H_INCLUDED
