/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef PROGRAM_H_INCLUDED
#define PROGRAM_H_INCLUDED
#include <cassert>
#include <string>
#include <vector>
#include <functional>
#include <cstdint>
#include "MidiDevice.h"
#include "DSS1.h"
#include "Multisound.h"
#include "bson.h"
#include "ProgramParams.h"

class System;

//typedef std::function<bool (int)> ProgramTransferProgressCallback;


/*

class ProgramParams
{
string GetName();
SetMultisoundIndex();
GetMultisoundIndex();
void SendToDevice(DSS1* pDSS1, uint8_t programIndex);
void FromDevice(DSS1* pDSS1, uint8_t programIndex, const std::string& name, ProgressCallback callback);
bool IsUsed()
void SetParameter(ProgramParameter param, uint8_t value)

private:
char m_name[8];

std::vector<uint8_t> m_programData;
}

class Program
{
public:
Program(m_params, Multisound, Multisound);
Program(m_params, Multisound&&, Multisound&&);

ProgramParams GetParams();


private:
	ProgramParams	m_params;
	MultisoundList m_multisoundList;

}



*/

/** The Program class represents a program in the DSS-1.

	A program will contain a full Multisound when it is passed around outside of a system. But when it is
	contained in a system it only has a reference to a Multisound in the system. More than one Program can
	refer to the same multisound in a system, which saves space.

	@nosubgrouping 
*/
class Program
{
public:
	/** @name Constructors
	*/
	//@{
	Program(const std::string& name = DEFAULT_NAME);
	Program(std::vector<uint8_t>& data, const std::string& name = DEFAULT_NAME);	// Creates a program with no multisounds
	Program(std::vector<uint8_t>&& data, const std::string& name = DEFAULT_NAME);	// Creates a program with no multisounds
	Program(const Program& other);	// copy
	Program(const Program&& other);	// move
	Program(const ProgramParams& params, MultisoundUPtr& multisound);
	Program(const ProgramParams& params, MultisoundUPtr& multisound1, MultisoundUPtr& multisound2);

	Program(DSS1* pDSS1, uint8_t programIndex, const std::string& name);	// construct a Program by streaming from the DSS1 device

	//@}
	Program& operator=(const Program& other);
	Program& operator=(const Program&& other);


	void SetParameter(ProgramParameter param, uint8_t value)
	{
		m_programParams.SetParameter(param,value);
	}
	uint8_t GetParameter(ProgramParameter param)
	{
		return m_programParams.GetParameter(param);
	}
	void SetName(const std::string& name)
	{
		m_programParams.SetName(name);
	}
	std::string GetName()
	{
		return m_programParams.GetName();
	}

	ProgramParams GetParams()
	{
		return m_programParams;
	}

	bool Compare(uint8_t multisoundIndex, Multisound* pMultisound)
	{
		assert(multisoundIndex < m_multisoundList.size());
		return m_multisoundList[multisoundIndex]->Equals(pMultisound);
	}

	Multisound* GetMultisoundPtr(uint8_t multisoundIndex)
	{
		assert(multisoundIndex < m_multisoundList.size());
		return m_multisoundList[multisoundIndex].get();
	}

	// The program copies its multisounds to the specified system, adjusts it's multisound indexes, and then 
	// copies it's parameters to the system. The function fails if there is not enought room in the system for the new multisounds.
	//bool TransferToSystem(System* pSystem);

	/**	Returns a copy of the multisound. The caller becomes the owner of the new copy.

	While it might be more efficient to return the Multisound itself, by making a copy we leave the
	Program in a usable state.

	@param multisoundIndex	Must be either 0 or 1.
	*/
	MultisoundUPtr GetMultisoundCopy(int multisoundIndex);
	Multisound* GetMultisoundRef(int multisoundIndex);
	uint32_t GetUsedPCMSpaceInSamples();

private:
	ProgramParams	m_programParams;

	/**	If the Program is outside of a System, it will own it's own multisounds and there will be either
	one or two in this list. This makes it easier to pass Programs around from System to System.
	But if the Program is contained in a System, the list will not have any Multisounds and they will
	insteam reside in the enclosing System (or Disk, if the System is contained in a Disk).

	Even though we will only have two multisounds, it makes more sense to use a vector here instead of
	two separate member variables because both oscillators may refer to the same Multisound.
	If the oscillators refer to the same Multisound there will only be one in the list.
	*/
	MultisoundList m_multisoundList;

};

#endif  // PROGRAM_H_INCLUDED
