/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

#include <string>
#include <vector>
#include <cstdint>
#include "Program.h"
#include "Multisound.h"
#include "MultisoundNameList.h"
#include "SystemParams.h"

/** @brief The System class represents a DSS-1 system. It contains all the Programs, Multisounds, and sound data for a system.

	The System class is responsible for holding all Programs and Multisounds in a system and keeping them in sync. 
	Programs refer to Multisounds by index. So the order must be in sync. Also, Multisound refer to sounds by 
	absolute address in a data dump within a system. 

	vector<uint8_t>& GetProgramParameterData(int programNumber);		// gets a reference to the raw data without removing or copying. The size of the vector is the exact size of the data.
																		// The data has references already set up to be consistent within the system.
																		// @internal I could create a funtion to get a reference to the program itself, but it doesn't make things any better.
	vector<uint8_t>& GetMultisoundParameterData(int multisoundNumber);	// gets a reference to the raw data without removing or copying. The size of the vector is the exact size of the data.

	vector<uint8_t>& GetPCMData(int multisoundNumber);					// gets a reference to the raw data without removing or copying.


	SetProgram(int programIndex, ProgramUPtr pProgram);	// A system always has 32 programs. But some of them may contain default values.
	ClearProgram(int programNumber);

	Set program by:
	SetProgram(0, Program(vector<uint8_t> buf, "name");	// creates program and then invokes SetProgram move.

*/
class System
{
public:
	System::System(const System& other);
	System(const std::string& name = "New System");
	System(const SystemParams& systemParams, MultisoundList&& multisoundList);
	//System(SystemParams systemParams, MultisoundList& multisoundList);
	System(DSS1* pDSS1, ProgressCallback callback = nullptr) ;

	System& operator=(const System& other);
	System& operator=(System&& other);

	void SetName(const std::string& name);
	std::string  GetName();

	bool FromDevice(DSS1* pDSS1, ProgressCallback callback = nullptr);

	bool ToDevice(DSS1* pDSS1, ProgressCallback callback = nullptr);


	/**	@brief Copies a program into the system at the specified index. */
	void SetProgramParams(uint8_t index, ProgramParams& params)
	{
		m_systemParams.SetProgramParams(index, params);
	}
	
	/**	@brief Moves a program into the system at the specified index. */
	void SetProgramParams(uint8_t index, ProgramParams&& params)
	{
		m_systemParams.SetProgramParams(index, std::move(params));
	}

	BSONDocument ToBSON();

	/** @brief Adds the Multisound to the System if it doesn't already exist in the System.
	
		Ownership of the Multisound is transferred to the system. If the multisound doesn't already 
		exist in the System, is is added and the index is returned. If the Multisound does already 
		exist in the System, the Multisound is not added and the index to the existing Multisound 
		is returned.\n
		This function is mean to to be called by Programs that are being added to the System.
	*/
	//int AddMultisound(MultisoundUPtr && pMultisound);

	void AddProgram(uint8_t index, Program& program);

	uint32_t GetFreePCMSpaceInSamples();
	uint32_t GetUsedPCMSpaceInSamples();

	/**	Looks for Multisounds in the System that have the same name as those in the Program but are otherwise not the same; Returns the first name found.

	This is used to avoid conflicts when adding programs to the System. If two Multisounds have the same name and also the exact same data, this is not a conflict.
	*/
	std::string GetConflictingMultisoundName(Program& program);
	bool ProgramCanFit(Program& program);

	bool HasMultisound(Multisound* pMultisound);	///< return true if this system contains the multisound specified by the pointer.

	MultisoundNameList GetMultisoundNameList();

	SystemParams GetSystemParams();

	uint8_t GetNumberOfMultisounds()
	{
		return (uint8_t)m_multisoundList.size();
	}
	Multisound* GetMultisoundPtr(uint8_t multisoundIndex)
	{
		assert(multisoundIndex < m_multisoundList.size());
		return m_multisoundList[multisoundIndex].get();
	}
	MultisoundUPtr GetMultisoundCopy(uint8_t multisoundIndex)
	{
		assert(multisoundIndex < m_multisoundList.size());
		return std::make_unique<Multisound>(*m_multisoundList[multisoundIndex]);
	}
private:
	using ProgramParamsList = std::vector<ProgramParams>;

	//std::string m_name;

	/** The program list hold 32 programs. This vector is always 32 slots in size because the DSS-1 always has 32 programs even 
		if they hold initialized data. The way to tell if a program is unused is if it has the name "!NO-NAME".
	*/
	//ProgramParamsList m_programParamsList;

	//MultisoundNameList m_multisoundNameList;	// Must be in the order that programs expect them because Programs refer to multisounds by index.
												// In a previous design I questioned whether it makes sense to keep this name list because it could be generated 
												// from the multisound list. But if multisounds are to be kept in an enclosing Disk, this list is needed.

	SystemParams m_systemParams;

	MultisoundList m_multisoundList;			// May be empty if this System is contained in a Disk. Multisounds can be in any order and are identified by name.
												// So the lowest slots are always used. If a MS is deleted, it is swapped with the MS in the highest valid slot.
												// If a MS is added, it is just added to 

};

using SystemUPtr = std::unique_ptr<System>;


#endif  // SYSTEM_H_INCLUDED
