/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef SYSTEMPARAMS_H_INCLUDED
#define SYSTEMPARAMS_H_INCLUDED
#include <string>
#include <vector>
#include <cstdint>
#include "ProgramParams.h"
#include "MultisoundNameList.h"

class BSONDocument;
class DSS1;

/** @brief The SystemParams class represents a System without Multisounds that is contained inside a Disk

	A SystemParams object contains everything in a System except the multisounds.

*/
class SystemParams
{
public:
	SystemParams();
	SystemParams(const SystemParams& params);
	SystemParams(std::string systemName, ProgramParamsList& programList, MultisoundNameList& multisoundNameList);
	SystemParams(const BSONDocument& doc) { FromBSON(doc); }
	SystemParams(int index, std::vector<uint8_t>& buf) { FromNative(index, buf); }

	SystemParams& operator=(const SystemParams& other);

	ProgramParams GetProgramParams(uint8_t programIndex) const
	{
		assert(programIndex < 32);
		return m_programParamsList[programIndex];
	}
	void SetProgramParams(uint8_t programIndex, const ProgramParams& params)
	{
		assert(programIndex < 32);
		m_programParamsList[programIndex] = params;
	}
	void SetProgramParams(uint8_t programIndex, ProgramParams&& params)
	{
		assert(programIndex < 32);
		m_programParamsList[programIndex] = std::move(params);
	}
	void SetProgramParameter(uint8_t programIndex, ProgramParameter parameter, uint8_t value)
	{
		m_programParamsList[programIndex].SetParameter(parameter, value);
	}
	uint8_t GetProgramParameter(uint8_t programIndex, ProgramParameter parameter)
	{
		return m_programParamsList[programIndex].GetParameter(parameter);
	}

	void AppendMultisoundNameListItem(MultisoundNameListItem item)
	{
		assert(m_multisoundNameList.size() < 16);
		m_multisoundNameList.push_back(item);
	}
	MultisoundNameListItem GetMultisoundNameListItem(uint8_t itemIndex)
	{
		return m_multisoundNameList[itemIndex];
	}
	uint8_t GetNumberOfMultisoundNames() const
	{
		return (uint8_t)m_multisoundNameList.size();
	}
	uint32_t GetPCMDataSizeInSamples()
	{
		return m_multisoundNameList.GetPCMDataSizeInSamples();
	}
	void SetMultisoundLengthInSamples(std::string name, uint32_t length);
	uint32_t GetMultisoundLengthInSamples(std::string name);

	/** Determines if the SystemParams refer to a Multisound with a given name.

		Note that the Multisounds are not guaranteed to have the same data, only the same name.
	
		@param[in] multisoundName	The name of the Multisound to look for.
		@returns	true if the SystemParams refer to a Multisound that has the given name.
	*/
	bool UsesMultisound(std::string multisoundName) const;

	std::vector<std::string> GetUsedMultisoundNames() const;
	std::string GetSystemName() const
	{
		return m_systemName;
	}
	void SetSystemName(const std::string& name)
	{
		m_systemName = name;
	}
	bool FromDevice(DSS1* pDSS1, ProgressCallback callback);
	bool ToDevice(DSS1* pDSS1, ProgressCallback callback);

	uint32_t GetTotalDataSize()
	{
		return	(m_programParamsList.size() * ProgramParams::DataSizeInBytes()) + 
				32 + 8 +	// Program name list size
				(m_multisoundNameList.GetListDataSize());
	}

	BSONDocument ToBSON();
	void FromBSON(const BSONDocument& doc);
	void FromNative(int index, std::vector<uint8_t>& buf);

	static bool HasSystem(int index, std::vector<uint8_t>& buf);

private:

	std::string m_systemName;

	/** The program list holds 32 programs. This vector is always 32 slots in size because the DSS-1 always has 32 programs even
		if they hold initialized data. The way to tell if a program is unused is if it has the name "!NO-NAME".
	*/
	ProgramParamsList m_programParamsList;

	MultisoundNameList m_multisoundNameList;	// Must be in the order that programs expect them because Programs refer to multisounds by index.
												// In a previous design I questioned whether it makes sense to keep this name list because it could be generated 
												// from the multisound list. But if multisounds are to be kept in an enclosing Disk, this list is needed.
};


#endif  // SYSTEMPARAMS_H_INCLUDED
