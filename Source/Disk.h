/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef DISK_H_INCLUDED
#define DISK_H_INCLUDED

#include <vector>
#include "System.h"
#include "SystemParams.h"
#include "Multisound.h"

using SystemParamList = std::vector<SystemParams>;

/** The Disk class simulates the DSS-1 physical floppy disk in that it can contain Systems and Multisounds.

	However, a Disk may hold any number of Systems and is not limited to four as a physical floppy disk is.
	When a System is added to a Disk, the System's Multisounds are moved from the System to the Disk, with duplicates removed.
	When a System is copied from a Disk for passing around, the Multisounds used by the System are copied into the System.
*/
class Disk
{
public:
	Disk(const std::string& name = "New Disk") : m_name(name), m_isModified(false) {}

	/**	Looks for Multisounds in the System that have the same name as those in the Program but are otherwise not the same; Returns the first name found.

	This is used to avoid conflicts when adding programs to the System. If two Multisounds have the same name and also the exact same data, this is not a conflict.
	*/
	std::string GetConflictingMultisoundName(System& system);

	/** Adds a copy of the System passed in.

	If the new system owns any Multisounds that are already in the Disk, those Multisounds are not copied.
	The Disk maintains only one copy of each Multisound. Since Multisounds are identified by name, Multisounds in the
	new System that are otherwise different than Multisounds in the Disk cannot have the same name, To determine if
	the new System has any Multisound names that would conflict with existing names, use the GetConflictingMultisoundName()
	function.

	@param[in] system	A reference to a System to add to the Disk.

	@throws std::runtime_error	If at least one Multisound in the System has the same name as a Multisound already in the Disk
	but the Multisound's data is different.
	*/
	void AddSystem(System& system);

	/** Removes (deletes) a System from the Disk.

	Any Multisounds that this System refers to are deleted only if they are not referred to by another System in the Disk.

	@param[in] systemNameToRemove		The name of the System to remove.

	*/
	void RemoveSystem(std::string systemName);

	/** Gets a copy of a system from the disk.

	Nothing is deleted from the Disk but rather everything, including Multisounds, is copied.
	*/
	System GetSystem(std::string name);

	bool HasSystem(const std::string& name) const;
	void SetName(const std::string& name);
	std::string GetName() const
	{
		return m_name;
	}
	BSONDocument ToBSON();
	void FromBSON(const BSONDocument& doc);
	void FromNative(std::vector<uint8_t>& buf, std::string folderpath, const std::string& systemName = "");

	void SaveToFile(const std::string& filepath);
	void LoadFromFile(const std::string& filepath);

	std::vector<std::string> GetSystemNameList();
	std::string GetSystemName(uint32_t index);
	void SetSystemName(uint32_t index, const std::string& name);
	uint32_t GetNumberOfSystems();

	bool IsModified()
	{
		return m_isModified;
	}

	void ClearModified()
	{
		m_isModified = false;
	}

	//	std::vector<std::string> GetSystemsThatOwnMultisound();

	void SwapSystemOrder(uint32_t systemIndex1, uint32_t systemIndex2);

#if defined(_DEBUG)
	void Validate();
#endif

private:
	std::string		m_name;
	SystemParamList	m_systemParamList;
	MultisoundList	m_multisoundList;			// May be empty if this System is contained in a Disk. Multisounds can be in any order and are identified by name.
	bool			m_isModified;
};

using DiskUPtr = std::unique_ptr<Disk>;

#endif  // DISK_H_INCLUDED
