/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <string>
#include <vector>
#include <memory>
#include <algorithm>
#include "SystemParams.h"
#include "bson.h"

#if defined(_DEBUG)
#include "../JuceLibraryCode/JuceHeader.h"	// include just for DBG()
#else
#define DBG(x)
#endif

using std::string;
using std::vector;


SystemParams::SystemParams() 
{}

SystemParams::SystemParams(const SystemParams& other)
{
	m_systemName = other.m_systemName;
	m_programParamsList = other.m_programParamsList;
	m_multisoundNameList = other.m_multisoundNameList;
}

SystemParams::SystemParams(string systemName, ProgramParamsList& programList, MultisoundNameList& multisoundNameList)
{
	m_systemName = systemName;
	m_programParamsList = programList;
	m_multisoundNameList = multisoundNameList;
}

SystemParams& SystemParams::operator=(const SystemParams& other)
{
	m_systemName = other.m_systemName;
	m_programParamsList = other.m_programParamsList;
	m_multisoundNameList = other.m_multisoundNameList;
	return *this;
}


void SystemParams::SetMultisoundLengthInSamples(std::string name, uint32_t length)
{
	auto pos = std::find_if(m_multisoundNameList.begin(), m_multisoundNameList.end(), [&name](auto& item) 
	{ return name == item.name; });

	if (pos != m_multisoundNameList.end())
	{
		pos->length = length;
	}
}

uint32_t SystemParams::GetMultisoundLengthInSamples(std::string multisoundName)
{
	auto pos = std::find_if(m_multisoundNameList.begin(), m_multisoundNameList.end(), [&multisoundName](const MultisoundNameListItem& item)
	{ return multisoundName == item.name; });

	return (pos != m_multisoundNameList.end()) ? pos->length : 0;
}

bool SystemParams::UsesMultisound(std::string multisoundName) const
{
	auto iter = std::find_if(m_multisoundNameList.begin(), m_multisoundNameList.end(), [&multisoundName](const MultisoundNameListItem& item)
	{ 
		return multisoundName == item.name; 
	});
	return iter != m_multisoundNameList.end();
}

std::vector<std::string> SystemParams::GetUsedMultisoundNames() const
{
	std::vector<std::string> list;
	for (auto& ms : m_multisoundNameList)
	{
		list.push_back(ms.name);
	}
	return list;
}


bool SystemParams::FromDevice(DSS1* pDSS1, ProgressCallback callback)
{
	assert(pDSS1);
	assert(m_programParamsList.size() == 32);

	// Note: don't clear m_programParamsList because it is always size 32. The programs will just get overwritten. (Maybe m_programParamsList should set all unloaded slots to the default.)
	m_multisoundNameList.clear();

	// We don't' know what the transfer size is until we get the multisound list. But let's just assume the worst case estimate. 
	// It doesn't matter that much since the list is relatively small.
	uint32_t estimatedTotalDataSize = (m_programParamsList.size() * ProgramParams::DataSizeInBytes()) +
		32 + 8 +	// Program name list size
		(m_multisoundNameList.GetMaxListDataSize());	// Assume maximum size until we after we get the multisound list.

	if (callback && !callback("Multisound list", 0, estimatedTotalDataSize, false))
	{
		return false;
	}
	m_multisoundNameList.FromDevice(pDSS1);

	// now we know the real size.
	uint32_t totalDataSize = GetTotalDataSize();
	uint32_t multisoundNameListDataSize = m_multisoundNameList.GetListDataSize();

	// We provide our own callback for the programParams transfer so that we report its transfer as a part of the larger SystemParams transfer.
	auto systemParamsCallback = [&](std::string objectBeingRead, uint32_t bytesSoFar, uint32_t /*totalBytes*/, bool /*isDone*/) -> bool
	{
		return callback(objectBeingRead, multisoundNameListDataSize + bytesSoFar, totalDataSize, false);
	};
	return m_programParamsList.FromDevice(pDSS1, systemParamsCallback);

}

bool SystemParams::ToDevice(DSS1* pDSS1, ProgressCallback callback)
{
	assert(pDSS1);
	assert(m_programParamsList.size() == 32);

	uint32_t totalDataSize = GetTotalDataSize();
	uint32_t multisoundNameListDataSize = m_multisoundNameList.GetListDataSize();

	if (callback && !callback("Multisound list", 0, totalDataSize, false))
	{
		return false;
	}
	m_multisoundNameList.ToDevice(pDSS1);

	// We provide our own callback for the programParams transfer so that we report its transfer as a part of the larger SystemParams transfer.
	auto systemParamsCallback = [&](std::string objectBeingRead, uint32_t bytesSoFar, uint32_t /*totalBytes*/, bool /*isDone*/) -> bool
	{
		return callback(objectBeingRead, multisoundNameListDataSize + bytesSoFar, totalDataSize, false);
	};
	m_programParamsList.ToDevice(pDSS1, systemParamsCallback);

	return true;
}

BSONDocument SystemParams::ToBSON()
{
	BSONDocument doc;

	doc.AddElementString("Name", m_systemName);
	doc.AddElementArray("ProgramParams", m_programParamsList.ToBSON());
	doc.AddElementArray("MultisoundList", m_multisoundNameList.ToBSON());
	return doc;
}

void SystemParams::FromBSON(const BSONDocument& doc)
{
	// Note: don't clear m_programParamsList because it is always size 32. The programs will just get overwritten. (Maybe m_programParamsList should set all unloaded slots to the default.)
	m_multisoundNameList.clear();

	BSONElementString* pNameElement = dynamic_cast<BSONElementString*>(doc.GetElement("Name"));
	if (pNameElement)
	{
		m_systemName = pNameElement->GetValue();
	}
	DBG("Loading system: " << m_systemName);

	BSONElementArray* pProgramParamsListDoc = dynamic_cast<BSONElementArray*>(doc.GetElement("ProgramParams"));
	if (pProgramParamsListDoc)
	{
		m_programParamsList.FromBSON(pProgramParamsListDoc->GetDoc());
	}

	BSONElementArray* pMultisoundListDoc = dynamic_cast<BSONElementArray*>(doc.GetElement("MultisoundList"));
	if (pMultisoundListDoc)
	{
		m_multisoundNameList.FromBSON(pMultisoundListDoc->GetDoc());
	}
}


std::vector<uint8_t> GetUsedMultisoundIndices(ProgramParamsList& programParamsList)
{
	std::vector<uint8_t> list;

	for (auto& p : programParamsList)
	{
		list.push_back(p.GetParameter(OSC1MULTISOUNDNUMBER));
		list.push_back(p.GetParameter(OSC2MULTISOUNDNUMBER));
	}
	// sort before removing duplicates otherwise unique() will not work right.
	std::sort(list.begin(),list.end());

	// remove duplicates
	list.erase(std::unique(list.begin(),list.end()),list.end());

	return list;
}

/**	@brief Updates the multisound index of programs based on the system-only multisound list.

	The programs in the specified ProgramParamsList are modified to point to a new list of multisounds 
	that is a subset of the disk's master list. The subset list contains only the multisounds used by 
	the one system. The list of indices passed in the list parameter contains the indices to the master 
	list. This is used to modify the programs to refer to the subset list indices instead of the 
	master list indices. The order of multisounds (created elsewhere) is assumed to be the same as 
	the list. That is, if the third slot in the list hold a value 17, it means that multisound 17 from 
	the master list is now multisound 3 in the system's subset list, and OSCs in programs that referred 
	to multisound 17 need to be changed to refer to multisound 3.

	@param[in] programParamsList	A reference to the ProgramParamsList that has the programs to update.
	@param[in] list		A sorted list of multisound indices used by one of the systems on a disk.
						The indices point to to multisounds in the master list which includes 
						all multisounds on the disk. All values must be unique in this list.
*/
void ResetProgramMultisoundIndices(ProgramParamsList& programParamsList,std::vector<uint8_t> list)
{
	assert(list.size() > 0);

	// create a reverse lookup list so we don't have to do a lot of searches.
	auto pos = std::max_element(list.begin(),list.end());
	std::vector<uint8_t> revlist(*pos + 1);	// add one to get the size because these values are zero-based indexes
	DBG("Converting multisound indices:");
	for (size_t i = 0; i < list.size(); ++i)
	{
		revlist[list[i]] = uint8_t(i);
		DBG((int)list[i] << " -> " << (int)i);
	}
	DBG(" ");

	for (auto& p : programParamsList)
	{
		p.SetParameter(OSC1MULTISOUNDNUMBER,revlist[p.GetParameter(OSC1MULTISOUNDNUMBER)]);
		p.SetParameter(OSC2MULTISOUNDNUMBER,revlist[p.GetParameter(OSC2MULTISOUNDNUMBER)]);
	}
}


void SystemParams::FromNative(int systemIndex,vector<uint8_t>& buf)
{
	assert(systemIndex >= 0 && systemIndex <= 3);

	m_systemName = "System ";
	m_systemName += char(0x41 + systemIndex);	// System A, System B, etc
	DBG("Loading system: " << m_systemName);

	m_programParamsList.FromNative(systemIndex,buf);

	// Get the master list of ALL multisound names from the disk.
	MultisoundNameList msNamesDisk;
	msNamesDisk.FromNative(buf);

	// In some native files I've found that sometimes a Program will contain an index 
	// to a multisound greater than the number of multisounds. I'm not sure how it gets that 
	// way, but I assume it's an unused program, probably where the multisound was later deleted.
	// So we need to fix that up here rather than just abort.
	uint8_t maxMultisoundIndex = uint8_t(msNamesDisk.size() - 1);
	for (auto& p : m_programParamsList)
	{
		if (p.GetParameter(OSC1MULTISOUNDNUMBER) > maxMultisoundIndex)
		{
			p.SetParameter(OSC1MULTISOUNDNUMBER, 0);
		}
		if (p.GetParameter(OSC2MULTISOUNDNUMBER) > maxMultisoundIndex)
		{
			p.SetParameter(OSC2MULTISOUNDNUMBER, 0);
		}
	}

	// Get a list of the multisound indices used for this system
	std::vector<uint8_t> list = GetUsedMultisoundIndices(m_programParamsList);
	assert(list.size() > 0);

	// Based on the list of used indices, build a sublist of multisound names containing only 
	// those used by the system. This is the multisound list just for this system.
	for (size_t i = 0; i < list.size(); ++i)
	{
		m_multisoundNameList.push_back(msNamesDisk[list[i]]);
	}

#if defined(_DEBUG)
	for (auto& m : m_multisoundNameList)
	{
		DBG(m.name);
	}
#endif

	// Now adjust the program parameters so that the two oscillators refer to the multisounds 
	// by index in this smaller sublist (rather than the master list from the disk).
	ResetProgramMultisoundIndices(m_programParamsList,list);

}


/** @brief Determines if a native (DS1) format file contains a system.

	@param[in] index	An index to the system to check for. This must be one of the numbers 0, 1, 2, or 3.
	@param[in] buf		A reference to a buffer that contains the contents of the DS1 file.
						The buffer size must be exactly 11,979 bytes in size.
	@returns			true if the file contains the system. false, otherwise.
*/
bool SystemParams::HasSystem(int index,vector<uint8_t>& buf)
{
	assert(buf.size() == 11979);
	assert(index >= 0 && index <= 3);

	int offset = index * 0x100;
	int end = offset + 0x100;
	for (; offset < end; offset++)
	{
		if (buf[offset] != 0)
		{
			return true;
		}
	}
	return false;
}
