/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <boost/locale.hpp>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
#include <boost/lockfree/queue.hpp>
#include "MainComponent.h"
#include "MidiDevice.h"
#include "DSS1.h"
#include "Disk.h"
#include "System.h"
#include "Sound.h"
#include "ZipLib.h"

namespace bfs = boost::filesystem;
using std::string;
using std::vector;
using std::make_unique;
using std::for_each;

StringArray MainMenuBarModel::getMenuBarNames()
{
	const char* const names[] = { "MIDI", "Export", "Help", nullptr };

	return StringArray(names);
}

PopupMenu MainMenuBarModel::getMenuForIndex(int menuIndex, const String& /*menuName*/)
{

	PopupMenu menu;

	if (menuIndex == 0)
	{
		menu.addCommandItem(m_pAppCommandManager, MainContentComponent::MIDISetup);
		//menu.addSeparator();
		//menu.addCommandItem(m_pAppCommandManager, StandardApplicationCommandIDs::quit);
	}
	if (menuIndex == 1)
	{
		menu.addCommandItem(m_pAppCommandManager, MainContentComponent::ExportWave);
		menu.addCommandItem(m_pAppCommandManager, MainContentComponent::ExportSoundfont);
	}
	if (menuIndex == 2)
	{
		menu.addCommandItem(m_pAppCommandManager, MainContentComponent::HelpAbout);
	}

	return menu;
}

void MainMenuBarModel::menuItemSelected(int menuItemID, int /*topLevelMenuIndex*/)
{
	int nothing;
	nothing = menuItemID;
}


void AdjustLookAndFeelColors(LookAndFeel& lookAndFeel);

//==============================================================================
MainContentComponent::MainContentComponent(ApplicationCommandManager* pAppCommandManager)
	:m_upButton("UpButton", 0.75, Colour::fromRGB(0, 0, 0))
	, m_downButton("DownButton", 0.25, Colour::fromRGB(0, 0, 0))
{
    setSize (540, 280);

	// Create and install global locale
	std::locale::global(boost::locale::generator().generate(""));
	// Make boost.filesystem use it
	boost::filesystem::path::imbue(std::locale());

	AdjustLookAndFeelColors(lookAndFeel);
	LookAndFeel::setDefaultLookAndFeel(&lookAndFeel);

	m_propertiesOptions.applicationName = "DSSend";
	m_propertiesOptions.folderName = "DSSend";
	m_propertiesOptions.filenameSuffix = "config";
	m_propertiesOptions.storageFormat = PropertiesFile::storeAsXML;

	m_pPropertiesFile = make_unique<PropertiesFile>(m_propertiesOptions);
	DBG("Configuration file:");
	DBG(m_pPropertiesFile->getFile().getFullPathName());
	m_defaultProperties.setValue(String("midiindevice"), var(""));
	m_defaultProperties.setValue(String("midioutdevice"), var(""));
	m_defaultProperties.setValue(String("midichannel"), var("1"));
	m_defaultProperties.setValue(String("lastdirectory"), File::getCurrentWorkingDirectory().getFullPathName());
	m_pPropertiesFile->setFallbackPropertySet(&m_defaultProperties);

	m_pMainMenuModel = std::make_unique<MainMenuBarModel>(pAppCommandManager);
	m_pMainMenuComponent = std::make_unique<MenuBarComponent>(m_pMainMenuModel.get());

	Rectangle<int> area(getLocalBounds());
	m_pMainMenuComponent->setBounds(area.removeFromTop(LookAndFeel::getDefaultLookAndFeel().getDefaultMenuBarHeight()));
	addAndMakeVisible(m_pMainMenuComponent.get());
	//const Colour colour(m_pMainMenuComponent->findColour(PopupMenu::backgroundColourId));

	m_diskNameLabel.setBounds(25, 40, 400, 24);
	m_diskNameLabel.setText("Start by creating a New disk or Opening an existing one.", NotificationType::dontSendNotification);
	m_diskNameLabel.setJustificationType(Justification::topLeft);
	m_diskNameLabel.setEditable(false);
	addAndMakeVisible(m_diskNameLabel);

	m_newButton.setButtonText("New");
	m_openButton.setButtonText("Open");
	m_saveButton.setButtonText("Save");
	m_saveAsButton.setButtonText("Save As");
	m_sendButton.setButtonText("Send");
	m_receiveButton.setButtonText("Receive");
	m_renameButton.setButtonText("Rename");
	m_removeButton.setButtonText("Remove");

	m_newButton.setTooltip("Create an empty Disk that you can add Systems to using the Receive button.");
	m_openButton.setTooltip("Open a Disk file.");
	m_saveButton.setTooltip("Save the current Disk to a file.");
	m_saveAsButton.setTooltip("Save the current Disk to a specific file.");
	m_sendButton.setTooltip("Send the currently selected System to the DSS-1 (overwriting its current contents).");
	m_receiveButton.setTooltip("Retrieve the System currently in the DSS-1.");
	m_renameButton.setTooltip("Rename the currently selected System.");
	m_removeButton.setTooltip("Remove (delete) the currently selected System.");
	m_upButton.setTooltip("Move the selected System up in the list.");
	m_downButton.setTooltip("Move the selected System down in the list.");

	m_newButton.setBounds(25, 80, 90, 24);
	m_openButton.setBounds(25, 120, 90, 24);
	m_saveButton.setBounds(25, 160, 90, 24);
	m_saveAsButton.setBounds(25, 200, 90, 24);
	m_sendButton.setBounds(425, 80, 90, 24);
	m_receiveButton.setBounds(425, 120, 90, 24);
	m_renameButton.setBounds(425, 160, 90, 24);
	m_removeButton.setBounds(425, 200, 90, 24);
	m_upButton.setBounds(380, 248, 20, 16);
	m_downButton.setBounds(352, 248, 20, 16);

	//const Colour colour1(m_newButton.findColour(TextButton::ColourIds::buttonColourId));

	m_newButton.addListener(this);
	m_openButton.addListener(this);
	m_saveButton.addListener(this);
	m_saveAsButton.addListener(this);
	m_sendButton.addListener(this);
	m_receiveButton.addListener(this);
	m_renameButton.addListener(this);
	m_removeButton.addListener(this);
	m_upButton.addListener(this);
	m_downButton.addListener(this);

	addAndMakeVisible(m_newButton);
	addAndMakeVisible(m_openButton);
	addAndMakeVisible(m_saveButton);
	addAndMakeVisible(m_saveAsButton);
	addAndMakeVisible(m_sendButton);
	addAndMakeVisible(m_receiveButton);
	addAndMakeVisible(m_renameButton);
	addAndMakeVisible(m_removeButton);
	addAndMakeVisible(m_upButton);
	addAndMakeVisible(m_downButton);

	UpdateButtonStates();

	m_testButton.setButtonText("Test");
	//area.removeFromTop(20);	// move down 20
	//area = area.removeFromTop(33);	// make area 33 pixels high
	//m_testButton.setBounds(area.removeFromLeft(100).reduced(6));		// reduce will trim from all sides
	m_testButton.setBounds(6, 50, 88, 21);		// reduce will trim from all sides
	m_testButton.setColour(TextButton::ColourIds::buttonColourId, Colours::lightskyblue);
	m_testButton.addListener(this);
	//addAndMakeVisible(m_testButton);

	m_systemListboxModel.setSelectionChangedCallback([this](int /*lastRowSelected*/){ this->UpdateButtonStates(); });

	m_systemListbox.setModel(&m_systemListboxModel);
	m_systemListbox.setMultipleSelectionEnabled(false);
	m_systemListbox.setBounds(140, 80, 260, 160);
	m_systemListbox.setWantsKeyboardFocus(false);
	m_systemListbox.setOutlineThickness(2);
	m_systemListbox.getViewport()-> setScrollBarsShown(true, true);	// Note that Horiz scroll bar won't be shown unless we change the minContentWidth of the listbox, which is done as needed.
	addAndMakeVisible(m_systemListbox);
}

MainContentComponent::~MainContentComponent()
{
	PopupMenu::dismissAllActiveMenus();

	// MainMenuComponent has to be deleted before MainMenuModel otherwise a crash will occur when the model tries to 
	// remove a listener (the MenuComponent) and it no longer exists.
	m_pMainMenuComponent.reset();
}

void MainContentComponent::paint (Graphics& /* g  */)
{
	static bool doOnce = false;
	if (!doOnce)
	{
		m_systemListbox.setWantsKeyboardFocus(false);
		m_newButton.grabKeyboardFocus();
		doOnce = true;
	}

   // g.fillAll (Colour (0xffeeddff));

   // g.setFont (Font (16.0f));
   // g.setColour (Colours::black);
   // g.drawText ("Hello World!", getLocalBounds(), Justification::centred, true);
}

void MainContentComponent::resized()
{
    // This is called when the MainContentComponent is resized.
    // If you add any child components, this is where you should
    // update their positions.
}


//==============================================================================
// The following methods implement the ApplicationCommandTarget interface, allowing
// this window to publish a set of actions it can perform, and which can be mapped
// onto menus, keypresses, etc.

ApplicationCommandTarget* MainContentComponent::getNextCommandTarget()
{
	// this will return the next parent component that is an ApplicationCommandTarget (in this
	// case, there probably isn't one, but it's best to use this method in your own apps).
	return findFirstTargetParentComponent();
}

void MainContentComponent::getAllCommands(Array<CommandID>& commands)
{
	// this returns the set of all commands that this target can perform..
	const CommandID ids[] = { 
		MIDISetup,
		ExportWave,
		ExportSoundfont,
		HelpAbout
	};

	commands.addArray(ids, numElementsInArray(ids));

}
void MainContentComponent::getCommandInfo(CommandID commandID, ApplicationCommandInfo& result)
{
	const String midiCategory("MIDI");

	switch (commandID)
	{
	case MIDISetup:
		result.setInfo("Setup", "Allows you to set the input and output MIDI devices.", midiCategory, 0);
		result.addDefaultKeypress('m', ModifierKeys::commandModifier);
		break;
	case ExportWave:
		result.setInfo("Wave", "Exports all sounds in a system as a set of Wave files packaged in a single zip file.", "Export", 0);
		result.addDefaultKeypress('w', ModifierKeys::commandModifier);

		// Note that there is no need to inform JUCE that the status of the menu has changed because this getCommandInfo() function gets 
		// called every time the menu is clicked on and displayed. (So the main menu Export is not disabled but the child popup may be.)
		result.setActive((m_currentDisk != nullptr) && (m_systemListbox.getNumSelectedRows() > 0) && (m_currentDisk->GetNumberOfSystems() > 0));
		break;
	case ExportSoundfont:
		result.setInfo("Soundfont", "Exports all multisounds in a system as a set of Soundfont 2 files packaged in a single zip file.", "Export", 0);
		result.addDefaultKeypress('s', ModifierKeys::commandModifier);

		// Note that there is no need to inform JUCE that the status of the menu has changed because this getCommandInfo() function gets 
		// called every time the menu is clicked on and displayed. (So the main menu Export is not disabled but the child popup may be.)
		result.setActive((m_currentDisk != nullptr) && (m_systemListbox.getNumSelectedRows() > 0) && (m_currentDisk->GetNumberOfSystems() > 0));
		break;
	case HelpAbout:
		result.setInfo("About", "About DSSend.", "Help", 0);
		result.addDefaultKeypress('a', ModifierKeys::commandModifier);
		break;
	default:
		break;
	};
}

bool MainContentComponent::perform(const InvocationInfo& info)
{
	switch (info.commandID)
	{
	case MIDISetup:
		DoMidiSetup();
		break;
	case ExportWave:
		DoExportWave();
		break;
	case ExportSoundfont:
		DoExportSoundfont();
		break;
	case HelpAbout:
		DoHelpAbout();
		break;
	default:
		return false;
	};

	return true;

}

void MainContentComponent::DoMidiSetup()
{
	AlertWindow w("MIDI Setup",
		"",
		AlertWindow::NoIcon);

	String midiInCurrentName = m_pPropertiesFile->getValue("midiindevice");
	String midiOutCurrentName = m_pPropertiesFile->getValue("midioutdevice");
	int  midiCurrentChannel = m_pPropertiesFile->getValue("midichannel").getIntValue();

	int midiInCurrentIndex = 0;
	int midiOutCurrentIndex = 0;
	String midiInComboName = "MidiIn";
	String midiOutComboName = "MidiOut";
	String midiChannelName = "MIDIChannel";
	String noDevicesFound = "No devices found";

	MidiInDeviceUPtr pMidiIn = make_unique<MidiInDevice>();
	MidiOutDeviceUPtr pMidiOut = make_unique<MidiOutDevice>();

	vector<string> midiInList = pMidiIn->GetDeviceList();
	vector<string> midiOutList = pMidiOut->GetDeviceList();
	
	// Copy names to StringArray because that's what addComboBox requires.
	StringArray midiInNames;
	StringArray midiOutNames;
	if (!midiInList.empty())
	{
		for (auto s : midiInList)
		{
			midiInNames.add(String(s));
			if (s == midiInCurrentName)
			{
				midiInCurrentIndex = midiInNames.size() - 1;
			}
		}
	}
	else
	{
		midiInNames.add(noDevicesFound);
	}
	w.addComboBox(midiInComboName, midiInNames, "Midi In");
	ComboBox* pMidiInCombo = w.getComboBoxComponent(midiInComboName);
	assert(pMidiInCombo);
	pMidiInCombo->setSelectedItemIndex(midiInCurrentIndex);
	pMidiInCombo->setEnabled(!midiInList.empty());

	if (!midiOutList.empty())
	{
		for (auto s : midiOutList)
		{
			midiOutNames.add(String(s));
			if (s == midiOutCurrentName)
			{
				midiOutCurrentIndex = midiOutNames.size() - 1;
			}
		}
	}
	else
	{
		midiOutNames.add(noDevicesFound);
	}
	w.addComboBox(midiOutComboName, midiOutNames, "Midi Out");
	ComboBox* pMidiOutCombo = w.getComboBoxComponent(midiOutComboName);
	assert(pMidiOutCombo);
	pMidiOutCombo->setSelectedItemIndex(midiOutCurrentIndex);
	pMidiOutCombo->setEnabled(!midiOutList.empty());

	w.addComboBox(midiChannelName, StringArray::fromTokens("1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16", false) , "MIDI Channel");
	ComboBox* pMidiChannelCombo = w.getComboBoxComponent(midiChannelName);
	pMidiChannelCombo->setSelectedItemIndex(midiCurrentChannel - 1);

	w.addButton("Ok", 1, KeyPress(KeyPress::returnKey, 0, 0));
	w.addButton("Cancel", 0, KeyPress(KeyPress::escapeKey, 0, 0));
	
	if (w.runModalLoop() != 0) // if 'Ok' button clicked
	{
		if (pMidiInCombo->isEnabled())
		{
			midiInCurrentName = pMidiInCombo->getText();
			m_pPropertiesFile->setValue(String("midiindevice"), var(midiInCurrentName));
		}
		if (pMidiOutCombo->isEnabled())
		{
			midiOutCurrentName = pMidiOutCombo->getText();
			m_pPropertiesFile->setValue(String("midioutdevice"), var(midiOutCurrentName));
		}

		String midiChannelString = pMidiChannelCombo->getText();
		m_pPropertiesFile->setValue(String("midichannel"), var(midiChannelString));

		m_pPropertiesFile->save();
	}
}

void MainContentComponent::DoHelpAbout()
{
	AlertWindow::showMessageBoxAsync(AlertWindow::InfoIcon,
		"About DSSend",
		"DSSend version 1.3 by Jim Hewes. Copyright 2015.",
		"OK");
}


void MainContentComponent::UpdateButtonStates()
{
	m_saveButton.setEnabled(m_currentDisk != nullptr && !m_currentFilePath.empty());
	m_saveAsButton.setEnabled(m_currentDisk != nullptr);
	m_sendButton.setEnabled(m_currentDisk != nullptr && (m_systemListbox.getNumSelectedRows() > 0) && (m_currentDisk->GetNumberOfSystems() > 0));
	m_receiveButton.setEnabled(m_currentDisk != nullptr);
	m_renameButton.setEnabled(m_currentDisk != nullptr && m_systemListbox.getNumSelectedRows() > 0);
	m_removeButton.setEnabled(m_currentDisk != nullptr && (m_systemListbox.getNumSelectedRows() > 0) && (m_currentDisk->GetNumberOfSystems() > 0));
	m_upButton.setEnabled(m_currentDisk != nullptr && (m_systemListboxModel.getNumRows() > 1) && (m_systemListbox.getNumSelectedRows() > 0));
	m_downButton.setEnabled(m_currentDisk != nullptr && (m_systemListboxModel.getNumRows() > 1) && (m_systemListbox.getNumSelectedRows() > 0));

}

void MainContentComponent::editorShown(Label* /* labelThatHasChanged */, TextEditor& textEditorShown)
{
	textEditorShown.setInputRestrictions(64);
}

void  MainContentComponent::labelTextChanged(Label* labelThatHasChanged)
{
	if (labelThatHasChanged == &m_diskNameEdit)
	{
		string newDiskName = labelThatHasChanged->getText().toStdString();
		if (newDiskName != m_currentDisk->GetName())
		{
			m_currentDisk->SetName(newDiskName);
		}
	}
}


/**	A one-time function that enables the Disk name label/edit control once a Disk is created or opened.

	Before a Disk is created or opened, there is one label control that gives the user a hint on 
	what to do next. Once the user creates or opens a Disk, this function is called (only once ever) 
	and it reduces the size of the label and changes it to "Disk name:". Also, the disk name label/edit 
	control is enabled and it has the name of the disk. That control is a label but it turns into a 
	text edit control when the user clicks on it or it gets focus.

	@param[in] diskName		The initial name of the disk to put in the label/edit control that will get enabled.
*/
void MainContentComponent::SetupForDiskName(const std::string& diskName)
{
	// previous bounds:    m_diskNameLabel.setBounds(30, 40, 320, 24);
	m_diskNameLabel.setBounds(25, 40, 90, 24);
	m_diskNameLabel.setText("Disk name:", NotificationType::dontSendNotification);
	m_diskNameLabel.setJustificationType(Justification::topLeft);
	m_diskNameLabel.setEditable(false);

	m_diskNameEdit.setBounds(115, 40, 400, 24);
	m_diskNameEdit.setText(diskName, NotificationType::dontSendNotification);
	m_diskNameEdit.setJustificationType(Justification::topLeft);
	m_diskNameEdit.setEditable(true);
	m_diskNameEdit.setTooltip("Click the Disk name to edit it.");
	m_diskNameEdit.addListener(this);
	addAndMakeVisible(m_diskNameEdit);
}

void MainContentComponent::ButtonClickedNew()
{
	// Old comment that doesn't apply here anymore but is still useful:
	//
	// Note that in order to use a lambda as a function pointer in this cast, we need to explicitly declare the type of the lambda.
	// A normal lambda defined with auto has its own unique type. The function forComponent() is really a template and in order for 
	// the compiler to match the template correctly it needs to match the type of the function pointer. But it can't do that if 
	// the lambda has its own unique type. So we need to define its type here. (We could use auto and then cast as we're passing 
	// to forComponent(), but this is just as easy.)
	// Further, we cannot define it with std:function<> because forComponent() was not defined to take a std::function<> but rather a 
	// native function pointer. Also, our lambda cannot have any capture context otherwise it cannot be converted into a function pointer.

	if (1 == AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
		"New Disk",
		"This will create a new, empty Disk and will clear the current Disk. Are you sure you want to do this?",
		String::empty,
		String::empty,
		this,
		nullptr))
	{
		m_currentDisk = std::make_unique<Disk>();
		m_systemListboxModel.SetDisk(m_currentDisk.get());
		m_systemListbox.setMinimumContentWidth(m_systemListboxModel.GetWidthForHorizontalScroll());
		m_systemListbox.updateContent();
		SetupForDiskName("New Disk");
		m_currentFilePath.clear();
		m_systemListbox.setWantsKeyboardFocus(false);
		UpdateButtonStates();

	}
}

void MainContentComponent::ButtonClickedOpen()
{

	bool okToOpen = true;
	if (m_currentDisk && m_currentDisk->IsModified())
	{
		okToOpen = AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Changes not saved!",
			"Your current Disk has not been saved since it was last modified. Are you sure you want to open the new file and lose your changes?",
			String::empty,
			String::empty,
			this,
			nullptr);

	}

	if (okToOpen)
	{
		File lastDirectory(m_pPropertiesFile->getValue("lastdirectory"));

			FileChooser fc("Choose a file to open...",
				lastDirectory,
				"*.dssdsk;*.ds1",
				true);

		if (fc.browseForFileToOpen())
			{
				File fileChosen = fc.getResult();
				String filepathChosen = fileChosen.getFullPathName();

					if (filepathChosen.endsWith(fileChosen.getFileName()))
					{
						String lastDirStr = filepathChosen.dropLastCharacters(fileChosen.getFileName().length());
						m_pPropertiesFile->setValue("lastdirectory", var(lastDirStr));
					}

					m_currentDisk = std::make_unique<Disk>();
					m_systemListboxModel.SetDisk(m_currentDisk.get());
					m_systemListbox.updateContent();	// we need to update here to force the list box to really update. Just having the one below dowsn't do it for some reason.
					try
					{
						m_currentDisk->LoadFromFile(filepathChosen.toStdString());
					}
					catch (std::runtime_error&)
					{
						throw std::runtime_error("Unable to load the file. It may be corrupted or it may not be a DSSend file.");
					}

					m_systemListboxModel.SetDisk(m_currentDisk.get());
					SetupForDiskName(m_currentDisk->GetName());
					m_systemListbox.setMinimumContentWidth(m_systemListboxModel.GetWidthForHorizontalScroll());
					m_systemListbox.updateContent();
					m_currentFilePath = filepathChosen.toStdString();

					UpdateButtonStates();
					//m_pMainMenuModel->menuItemsChanged();

					if (m_systemListboxModel.getNumRows() > 0)
					{
						m_systemListbox.setWantsKeyboardFocus(true);

						m_systemListbox.selectRow(0);
			}
		}
	}
}

void MainContentComponent::ButtonClickedSave()
{
	if (m_currentDisk && !m_currentFilePath.empty())
	{
		string msg = "Save Disk to file  ";
		msg += m_currentFilePath;
		msg += " ?";

		if (AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Save Disk",
			msg,
			String::empty,
			String::empty,
			this,
			nullptr))
		{
			m_currentDisk->SaveToFile(m_currentFilePath);
			UpdateButtonStates();
		}
	}
}

void MainContentComponent::ButtonClickedSaveAs()
{

	if (m_currentDisk)
	{
		String locationToSaveTo = m_currentFilePath.empty() ? m_pPropertiesFile->getValue("lastdirectory") : m_currentFilePath;
		File initialBrowseFile(locationToSaveTo);

		if (initialBrowseFile.isDirectory())
		{
			initialBrowseFile = File(locationToSaveTo + m_currentDisk->GetName() + ".dssdsk");
		}
		else
		{	// replace .DS1 extension with .dssdsk. We only save to .dssdsk.
			if (initialBrowseFile.getFileExtension() == ".DS1")
			{
				initialBrowseFile = initialBrowseFile.withFileExtension(".dssdsk");
			}
		}

		FileChooser fc("Choose a file to save...",
			initialBrowseFile,
			"*.dssdsk",
			true);

		if (fc.browseForFileToSave(true))
		{
			File fileChosen = fc.getResult();
			String filepathChosen = fileChosen.getFullPathName();

			// save the last directory browsed
			if (filepathChosen.endsWith(fileChosen.getFileName()))
			{
				String lastDirStr = filepathChosen.dropLastCharacters(fileChosen.getFileName().length());
				m_pPropertiesFile->setValue("lastdirectory", var(lastDirStr));
			}

			// Ensure the filename always ends with the right extension.
			if (!filepathChosen.endsWith(".dssdsk"))
			{
				filepathChosen += ".dssdsk";
			}

			m_currentFilePath = filepathChosen.toStdString();
			m_currentDisk->SaveToFile(m_currentFilePath);
			UpdateButtonStates();
		}
	}
}

void SendTask::run()
{
	try
	{
		auto transferCallback = [this](std::string objectBeingRead,uint32_t bytesSoFar,uint32_t totalBytes,bool isDone) -> bool
		{
			// this will update the progress bar on the dialog box
			setProgress((double)bytesSoFar / (double)totalBytes);
			assert(((double)bytesSoFar / (double)totalBytes) <= 1.0);

			setStatusMessage(isDone ? "Done" : objectBeingRead);	// causes alert dialog to get repainted.

			// must check this as often as possible, because this is
			// how we know if the user's pressed 'cancel'
			return !threadShouldExit();
		};

		const string midiInCurrentName = m_pPropertiesFile->getValue("midiindevice").toStdString();
		const string midiOutCurrentName = m_pPropertiesFile->getValue("midioutdevice").toStdString();
		const uint8_t midiCurrentChannel = (uint8_t)(m_pPropertiesFile->getValue("midichannel").getIntValue());

		MidiIoDevice midiDevice;
		midiDevice.Open(midiInCurrentName,midiOutCurrentName);	// will throw if the MIDI IN and OUT devices cannot be opened.

		DSS1 dss1(&midiDevice,midiCurrentChannel - 1);	// the value stored in the app settings ranges from 1 - 16 so we need to adjust to an index.

		// Make sure we can talk to the DSS-1 before we start the receive process. This avoids showing the progress dialog until we're sure we're ready to go.
		if (!dss1.IsConnected())
		{
			throw std::runtime_error("Could not communicate with the DSS-1. Please check the MIDI connection. Make sure the DSS-1 is powered on.");
		}

		m_completedTransfer = m_systemToSend.ToDevice(&dss1,transferCallback);

	}
	catch (std::exception e)
	{
		m_exceptionMsg = e.what();
	}

}

void MainContentComponent::OnSendDone(bool userPressedCancel)
{
	userPressedCancel;
	// Check if there was any exception in the thread.
	string exceptionMsg = m_pSendTask->GetExceptionMsg();
	if (exceptionMsg.empty())
	{
		// thread finished normally..
		m_systemListboxModel.SetDisk(m_currentDisk.get());
		m_systemListbox.updateContent();

		UpdateButtonStates();
	}
	else
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error:",
			exceptionMsg,
			"OK");
	}
	m_pSendTask.reset();

}

void MainContentComponent::ButtonClickedSend()
{
	assert(m_currentDisk);
	assert(m_currentDisk->GetNumberOfSystems() > 0);


	if(!AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
			"Send System to DSS-1",
			"This will overwrite the System currently in your DSS-1. Are you sure you want to Send?",
			String::empty,
			String::empty,
			this,
			nullptr))
	{
		return;
	}

	uint32_t selectedSystemIndex = m_systemListbox.getSelectedRow();
	assert(selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	string systemName = m_currentDisk->GetSystemName(selectedSystemIndex);

	// Yes, for now the System must be copied out of the Disk because we need to get the Multisounds that are only for this System.
	// True, I could figure out some way to send a system while avoiding the copy, but for now this is how it is. 
	// (Anyway, performance seems OK and this is simpler.)
	System systemToSend = m_currentDisk->GetSystem(systemName);

	//TransferTask rt(true, dss1, systemToSend);
	m_pSendTask = std::make_unique<SendTask>(systemToSend,m_pPropertiesFile.get(),
							[this](bool userClickedCancel){OnSendDone(userClickedCancel); });
	m_pSendTask->launchThread();

}


void ReceiveTask::run()
{
	try
	{
		auto transferCallback = [this](std::string objectBeingRead,uint32_t bytesSoFar,uint32_t totalBytes,bool isDone) -> bool
		{
			// this will update the progress bar on the dialog box
			setProgress((double)bytesSoFar / (double)totalBytes);
			assert(((double)bytesSoFar / (double)totalBytes) <= 1.0);

			setStatusMessage(isDone ? "Done" : objectBeingRead);	// causes alert dialog to get repainted.

			// must check this as often as possible, because this is
			// how we know if the user's pressed 'cancel'
			return !threadShouldExit();
		};

		const string midiInCurrentName = m_pPropertiesFile->getValue("midiindevice").toStdString();
		const string midiOutCurrentName = m_pPropertiesFile->getValue("midioutdevice").toStdString();
		String midiCurrentStr = m_pPropertiesFile->getValue("midichannel");
		uint8_t midiCurrentChannel = (uint8_t)(midiCurrentStr.getIntValue());

		MidiIoDevice midiDevice;
		midiDevice.Open(midiInCurrentName,midiOutCurrentName);	// will throw if the MIDI IN and OUT devices cannot be opened.

		DSS1 dss1(&midiDevice,midiCurrentChannel - 1);	// the value stored in the app settings ranges from 1 - 16 so we need to adjust to an index.

		// Make sure we can talk to the DSS-1 before we start the receive process. This avoids showing the progress dialog until we're sure we're ready to go.
		if (!dss1.IsConnected())
		{
			throw std::runtime_error("Could not communicate with the DSS-1. Please check the MIDI connection. Make sure the DSS-1 is powered on");
		}

		m_completedTransfer = m_system.FromDevice(&dss1,transferCallback);

	}
	catch (std::exception e)
	{
		m_exceptionMsg = e.what();
	}

}

void MainContentComponent::ButtonClickedReceive()
{
	assert(m_currentDisk);

	//TransferTask rt(false, dss1, system);
	m_pReceiveTask = std::make_unique<ReceiveTask>(m_pPropertiesFile.get(), [this](bool userClickedCancel){OnReceiveDone(userClickedCancel); });

	m_pReceiveTask->launchThread();
}

void MainContentComponent::OnReceiveDone(bool userPressedCancel)
{
	userPressedCancel;

	// Check if there was any exception in the thread.
	string exceptionMsg = m_pReceiveTask->GetExceptionMsg();

	if (exceptionMsg.empty())
	{
		// thread finished normally..
		if (!m_pReceiveTask->WasCanceled())
		{
			// Create a Unique name for the systems
			vector<string> list = m_currentDisk->GetSystemNameList();
			const string newSys = "New System ";
			std::ostringstream os;
			for (int i = 1; i < 1000; ++i)
			{
				os.str("");
				os << newSys << i;
				auto found = find_if(list.begin(),list.end(),[&os](string& s){ return s == os.str();  });
				if (found == list.end())
				{
					i = 1000;	// done
				}
			}

			System& system = m_pReceiveTask->GetSystemReceived();
			system.SetName(os.str());

			m_currentDisk->AddSystem(system);

			m_systemListboxModel.SetDisk(m_currentDisk.get());
			m_systemListbox.setMinimumContentWidth(m_systemListboxModel.GetWidthForHorizontalScroll());
			m_systemListbox.updateContent();

			// It's convenient that we select a row if there's only one so that the buttons get enabled.
			if (m_systemListboxModel.getNumRows() == 1)
			{
				m_systemListbox.setWantsKeyboardFocus(true);
				m_systemListbox.selectRow(0);
			}
		}
		UpdateButtonStates();

	}
	else
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error:",
			exceptionMsg,
			"OK");
	}
	m_pReceiveTask.reset();
}



void MainContentComponent::ButtonClickedRename()
{
	AlertWindow w("Rename System", "", AlertWindow::NoIcon);

	assert(m_systemListbox.getNumSelectedRows() == 1);
	assert(m_currentDisk);


	uint32_t selectedSystemIndex = m_systemListbox.getSelectedRow();
	assert(selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	string oldName = m_currentDisk->GetSystemName(selectedSystemIndex);
	string textboxEditorName = "SystemNameTextbox";

	w.addTextEditor(textboxEditorName, oldName, "System Name:");
	w.getTextEditor(textboxEditorName)->setInputRestrictions(64);
	w.addButton("Ok", 1, KeyPress(KeyPress::returnKey, 0, 0));
	w.addButton("Cancel", 0, KeyPress(KeyPress::escapeKey, 0, 0));

	if (w.runModalLoop() != 0) // if 'Ok' button was clicked
	{
		string newName = w.getTextEditorContents(textboxEditorName).toStdString();
		if (0 != newName.compare(oldName))
		{
			m_currentDisk->SetSystemName(selectedSystemIndex, newName);
			m_systemListbox.setMinimumContentWidth(m_systemListboxModel.GetWidthForHorizontalScroll());
			m_systemListbox.repaintRow(selectedSystemIndex);
		}
	}
}
void MainContentComponent::ButtonClickedRemove()
{
	assert(m_currentDisk);
	assert(m_currentDisk->GetNumberOfSystems() > 0);
	assert(m_systemListbox.getNumSelectedRows() == 1);

	int selectedSystemIndex = m_systemListbox.getSelectedRow();
	if (selectedSystemIndex < 0)	// no rows selected.
	{	
		return;
	}
	assert((uint32_t)selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	string systemName = m_currentDisk->GetSystemName(selectedSystemIndex);
	string msg = "Are you sure you want to remove the system named ";
	msg += systemName;
	msg += " from the Disk?";
	AlertWindow w("Remove System", msg, AlertWindow::NoIcon);
	w.addButton("Ok", 1, KeyPress(KeyPress::returnKey, 0, 0));
	w.addButton("Cancel", 0, KeyPress(KeyPress::escapeKey, 0, 0));

	if (w.runModalLoop() != 0) // if 'Ok' button was clicked
	{
		m_currentDisk->RemoveSystem(systemName);
		m_systemListbox.setMinimumContentWidth(m_systemListboxModel.GetWidthForHorizontalScroll());
		m_systemListbox.updateContent();
		UpdateButtonStates();
	}

}

void MainContentComponent::ButtonClickedUp()
{
	uint32_t selectedSystemIndex = m_systemListbox.getSelectedRow();
	assert(selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	if (m_currentDisk->GetNumberOfSystems() > 1 && selectedSystemIndex > 0)
	{
		m_currentDisk->SwapSystemOrder(selectedSystemIndex, selectedSystemIndex - 1);
		m_systemListbox.updateContent();
		m_systemListbox.selectRow(selectedSystemIndex - 1);
	}

}
void MainContentComponent::ButtonClickedDown()
{
	uint32_t selectedSystemIndex = m_systemListbox.getSelectedRow();
	assert(selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	if ((selectedSystemIndex + 1) < m_currentDisk->GetNumberOfSystems())
	{
		m_currentDisk->SwapSystemOrder(selectedSystemIndex, selectedSystemIndex + 1);
		m_systemListbox.updateContent();
		m_systemListbox.selectRow(selectedSystemIndex + 1);
	}

}

/** @brief Creates a unique and empty directory within the user's temporary directory; Returns the full path to it.

	@returns The full path to the directory created.
*/
bfs::path CreateTempSubDirectory()
{
	boost::system::error_code ec;
	bfs::path tempPath(bfs::unique_path( bfs::temp_directory_path(ec) / "DSS-%%%%-%%%%-%%%%"));

	if (!bfs::create_directories(tempPath, ec) || ec)
	{
		std::ostringstream os;
		os << "Could not create temporary directory: " << tempPath.string() << " : " << ec.message();
		throw std::runtime_error(os.str());
	}

	return tempPath;
}

void MainContentComponent::DoExportWave()
{
	assert(m_currentDisk);
	assert(m_currentDisk->GetNumberOfSystems() > 0);
	assert(m_systemListbox.getNumSelectedRows() == 1);
	
	int selectedSystemIndex = m_systemListbox.getSelectedRow();
	if (selectedSystemIndex < 0)	// no rows selected.
	{
		return;
	}
	assert((uint32_t)selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	if (m_currentDisk)
	{
		String locationToSaveTo = m_pPropertiesFile->getValue("lastdirectory");
		File initialBrowseFile(locationToSaveTo + m_currentDisk->GetName() + ".zip");

		FileChooser fc("Choose a ZIP file to save Sounds as Wave files...",
			initialBrowseFile,
			"*.zip",
			false);

		if (fc.browseForFileToSave(true))
		{
			File fileChosen = fc.getResult();
			String filepathChosen = fileChosen.getFullPathName();

			// Ensure the filename always ends with the right extension.
			if (!filepathChosen.endsWith(".zip"))
			{
				filepathChosen += ".zip";
			}

			bfs::path tempDir = CreateTempSubDirectory();

			System sys = m_currentDisk->GetSystem(m_currentDisk->GetSystemName(selectedSystemIndex));

			bfs::path loopInfoFileName = tempDir / "LoopInfo.txt";
			bfs::ofstream loopInfoFile;
			loopInfoFile.open(loopInfoFileName);

			loopInfoFile << "Loop Information\n\n";


			vector<string> zipFiles;
			uint8_t numMultisounds = sys.GetNumberOfMultisounds();
			for (uint8_t msIndex = 0; msIndex < numMultisounds; ++msIndex)
			{
				Multisound* pMs = sys.GetMultisoundPtr(msIndex);
				uint8_t numSounds = pMs->GetNumberOfSounds();

				bfs::ofstream of;
				for (uint8_t sndIndex = 0; sndIndex < numSounds; ++sndIndex)
				{
					Sound snd = pMs->GetSound(sndIndex);
					string sndFilename = snd.GetName() + ".wav";
					bfs::path sndFilepath = tempDir / sndFilename;
					of.open(sndFilename.c_str(), std::ios::out | std::ios::binary);
					snd.ToWave(of);
					of.close();
					zipFiles.push_back(sndFilename);

					loopInfoFile << snd.GetName() << std::endl;
					loopInfoFile << "    start:  " << snd.GetLoopStart() << std::endl;
					loopInfoFile << "    length: " << snd.GetLoopLength() << std::endl << std::endl;
				}
			}
			loopInfoFile.close();

			// Now zip up all the files
			ZipLib zipLib;
			zipLib.AddFile(loopInfoFileName.string());
			for (auto& filename : zipFiles)
			{
				zipLib.AddFile(filename);
			}

			zipLib.SetStoreWithPath(false);		// do not store the files in the archive with their path. Just the filenames.

			zipLib.CreateLibrary(filepathChosen.toStdString());

			for (auto& filename : zipFiles)
			{
				bfs::remove(filename);
			}
			bfs::remove(loopInfoFileName);

			bfs::remove(tempDir);
		}
	}
}

void MainContentComponent::DoExportSoundfont()
{
	assert(m_currentDisk);
	assert(m_currentDisk->GetNumberOfSystems() > 0);
	assert(m_systemListbox.getNumSelectedRows() == 1);

	int selectedSystemIndex = m_systemListbox.getSelectedRow();
	if (selectedSystemIndex < 0)	// no rows selected.
	{
		return;
	}
	assert((uint32_t)selectedSystemIndex < m_currentDisk->GetNumberOfSystems());

	if (m_currentDisk)
	{
		String locationToSaveTo = m_pPropertiesFile->getValue("lastdirectory");
		File initialBrowseFile(locationToSaveTo + m_currentDisk->GetName() + ".zip");

		FileChooser fc("Choose a ZIP file to save Multisounds as Soundfonts...",
			initialBrowseFile,
			"*.zip",
			false);

		if (fc.browseForFileToSave(true))
		{
			File fileChosen = fc.getResult();
			String filepathChosen = fileChosen.getFullPathName();

			// Ensure the filename always ends with the right extension.
			if (!filepathChosen.endsWith(".zip"))
			{
				filepathChosen += ".zip";
			}

			bfs::path tempDir = CreateTempSubDirectory();	// includes trailing path separator.

			System sys = m_currentDisk->GetSystem(m_currentDisk->GetSystemName(selectedSystemIndex));

			vector<string> zipFiles;
			bfs::ofstream of;
			uint8_t numMultisounds = sys.GetNumberOfMultisounds();
			for (uint8_t msIndex = 0; msIndex < numMultisounds; ++msIndex)
			{
				Multisound* pMs = sys.GetMultisoundPtr(msIndex);
				string msName = pMs->GetName();
				std::replace(msName.begin(), msName.end(), '.', '_'); // replace all '.' with '_'. At least the Kronos doesn't like periods in filenames
				msName += ".sf2";
				bfs::path sfFilename = tempDir / msName;
				of.open(sfFilename.c_str(), std::ios::out | std::ios::binary);
				pMs->ToSoundfont(of);
				of.close();
				zipFiles.push_back(sfFilename.string());
			}

			// Now zip up all the files
			ZipLib zipLib;
			for (auto& filename : zipFiles)
			{
				zipLib.AddFile(filename);
			}

			zipLib.SetStoreWithPath(false);		// do not store the files in the archive with their path. Just the filenames.

			zipLib.CreateLibrary(filepathChosen.toStdString());

			for (auto& filename : zipFiles)
			{
				bfs::remove(filename);
			}

			bfs::remove(tempDir);
		}
	}

}



void MainContentComponent::buttonClicked(Button* button)
{
	try
	{
		if (button == &m_newButton)
		{
			ButtonClickedNew();
		}
		if (button == &m_openButton)
		{
			ButtonClickedOpen();
		}
		if (button == &m_saveButton)
		{
			ButtonClickedSave();
		}
		if (button == &m_saveAsButton)
		{
			ButtonClickedSaveAs();
		}
		if (button == &m_sendButton)
		{
			ButtonClickedSend();
		}
		if (button == &m_receiveButton)
		{
			ButtonClickedReceive();
		}
		if (button == &m_renameButton)
		{
			ButtonClickedRename();
		}
		if (button == &m_removeButton)
		{
			ButtonClickedRemove();
		}
		if (button == &m_upButton)
		{
			ButtonClickedUp();
		}
		if (button == &m_downButton)
		{
			ButtonClickedDown();
		}
		if (button == &m_testButton)
		{

// Old code
#if 0
			string midiInCurrentName = m_pPropertiesFile->getValue("midiindevice").toStdString();
			string midiOutCurrentName = m_pPropertiesFile->getValue("midioutdevice").toStdString();


			m_currentDisk = std::make_unique<Disk>();
			m_currentDisk->LoadFromFile("testdiskfile.dsk");
			m_systemListboxModel.SetDisk(m_currentDisk.get());
			m_systemListbox.updateContent();

			//System sys5 = disk1.GetSystem("System C");

			MidiIoDevice midiDevice;

			midiDevice.Open(midiInCurrentName, midiOutCurrentName);
			assert(midiDevice.IsOpen());

			//int mode;
			DSS1 dss1(&midiDevice, 0);
			int mode = dss1.GetMode();
			mode;	// avoid compiler warning.

			dss1.SetPlayMode();
			sys5.ToDevice(&dss1);
			// For testing:  Retrieve four system from the DSS1 and add them all to a Disk. Save the Disk to a file.
			// You need to break at each FromDevice() call to load in a new system from the disk.
			System sys1("System A");
			System sys2("System B");
			System sys3("System C");
			System sys4("System D");
			sys1.FromDevice(&dss1);
			sys2.FromDevice(&dss1);
			sys3.FromDevice(&dss1);
			sys4.FromDevice(&dss1);
			Disk disk;
			disk.AddSystem(sys1);
			disk.AddSystem(sys2);
			disk.AddSystem(sys3);
			disk.AddSystem(sys4);
			// For testing:   Get a System from the Disk and send it back to the DSS-1
			sys5 = disk.GetSystem("System C");	// reassignment to system causing problem. unique_ptr trying to copydue to default assignment operator.
			sys5.ToDevice(&dss1);

			disk.SaveToFile();
#endif
			//midiDevice.Close();
		}
	}
	catch (std::exception& e)
	{
		AlertWindow::showMessageBoxAsync(AlertWindow::WarningIcon,
			"Error:",
			e.what(),
			"OK");

	}


}


void AdjustLookAndFeelColors(LookAndFeel& lookAndFeel)
{
	// initialise the standard set of colours..
	//const uint32 textButtonColour = 0xffbbbbff;
	const uint32 textButtonColour = 0xff87cefa;	// lightskyblue
	//const uint32 textHighlightColour = 0x401111ee;
	const uint32 textHighlightColour = 0xff87cefa;
	//const uint32 standardOutlineColour = 0xb2808080;
	const uint32 standardOutlineColour = 0xff87cefa;

	static const uint32 standardColours[] =
	{
		TextButton::buttonColourId, textButtonColour,
		TextButton::buttonOnColourId, 0xff4444ff,
		TextButton::textColourOnId, 0xff000000,
		TextButton::textColourOffId, 0xff000000,

		ToggleButton::textColourId, 0xff000000,

		TextEditor::backgroundColourId, 0xffffffff,
		TextEditor::textColourId, 0xff000000,
		TextEditor::highlightColourId, textHighlightColour,
		TextEditor::highlightedTextColourId, 0xff000000,
		TextEditor::outlineColourId, 0x00000000,
		TextEditor::focusedOutlineColourId, textButtonColour,
		TextEditor::shadowColourId, 0x38000000,

		CaretComponent::caretColourId, 0xff000000,

		Label::backgroundColourId, 0x00000000,
		Label::textColourId, 0xff000000,
		Label::outlineColourId, 0x00000000,

		ScrollBar::backgroundColourId, 0x00000000,
		ScrollBar::thumbColourId, 0xffffffff,

		TreeView::linesColourId, 0x4c000000,
		TreeView::backgroundColourId, 0x00000000,
		TreeView::dragAndDropIndicatorColourId, 0x80ff0000,
		TreeView::selectedItemBackgroundColourId, 0x00000000,

		PopupMenu::backgroundColourId, 0xffffffff,
		PopupMenu::textColourId, 0xff000000,
		PopupMenu::headerTextColourId, 0xff000000,
		PopupMenu::highlightedTextColourId, 0xffffffff,
		//PopupMenu::highlightedBackgroundColourId, 0x991111aa,
		PopupMenu::highlightedBackgroundColourId, 0xff87cefa,
		
		//ComboBox::buttonColourId, 0xffbbbbff,
		ComboBox::buttonColourId, 0xff87cefa,
		ComboBox::outlineColourId, standardOutlineColour,
		ComboBox::textColourId, 0xff000000,
		ComboBox::backgroundColourId, 0xffffffff,
		ComboBox::arrowColourId, 0x99000000,

		PropertyComponent::backgroundColourId, 0x66ffffff,
		PropertyComponent::labelTextColourId, 0xff000000,

		TextPropertyComponent::backgroundColourId, 0xffffffff,
		TextPropertyComponent::textColourId, 0xff000000,
		TextPropertyComponent::outlineColourId, standardOutlineColour,

		BooleanPropertyComponent::backgroundColourId, 0xffffffff,
		BooleanPropertyComponent::outlineColourId, standardOutlineColour,

		ListBox::backgroundColourId, 0xffffffff,
		ListBox::outlineColourId, standardOutlineColour,
		ListBox::textColourId, 0xff000000,

		Slider::backgroundColourId, 0x00000000,
		Slider::thumbColourId, textButtonColour,
		Slider::trackColourId, 0x7fffffff,
		Slider::rotarySliderFillColourId, 0x7f0000ff,
		Slider::rotarySliderOutlineColourId, 0x66000000,
		Slider::textBoxTextColourId, 0xff000000,
		Slider::textBoxBackgroundColourId, 0xffffffff,
		Slider::textBoxHighlightColourId, textHighlightColour,
		Slider::textBoxOutlineColourId, standardOutlineColour,

		ResizableWindow::backgroundColourId, 0xff777777,
		//DocumentWindow::textColourId,               0xff000000, // (this is deliberately not set)

		AlertWindow::backgroundColourId, 0xffededed,
		AlertWindow::textColourId, 0xff000000,
		AlertWindow::outlineColourId, 0xff666666,

		ProgressBar::backgroundColourId, 0xffeeeeee,
		//ProgressBar::foregroundColourId, 0xffaaaaee,
		ProgressBar::foregroundColourId, textButtonColour,

		TooltipWindow::backgroundColourId, 0xffeeeebb,
		TooltipWindow::textColourId, 0xff000000,
		TooltipWindow::outlineColourId, 0x4c000000,

		TabbedComponent::backgroundColourId, 0x00000000,
		TabbedComponent::outlineColourId, 0xff777777,
		TabbedButtonBar::tabOutlineColourId, 0x80000000,
		TabbedButtonBar::frontOutlineColourId, 0x90000000,

		Toolbar::backgroundColourId, 0xfff6f8f9,
		Toolbar::separatorColourId, 0x4c000000,
		Toolbar::buttonMouseOverBackgroundColourId, 0x4c0000ff,
		Toolbar::buttonMouseDownBackgroundColourId, 0x800000ff,
		Toolbar::labelTextColourId, 0xff000000,
		Toolbar::editingModeOutlineColourId, 0xffff0000,

		DrawableButton::textColourId, 0xff000000,
		DrawableButton::textColourOnId, 0xff000000,
		DrawableButton::backgroundColourId, 0x00000000,
		DrawableButton::backgroundOnColourId, 0xaabbbbff,

		HyperlinkButton::textColourId, 0xcc1111ee,

		GroupComponent::outlineColourId, 0x66000000,
		GroupComponent::textColourId, 0xff000000,

		BubbleComponent::backgroundColourId, 0xeeeeeebb,
		BubbleComponent::outlineColourId, 0x77000000,

		DirectoryContentsDisplayComponent::highlightColourId, textHighlightColour,
		DirectoryContentsDisplayComponent::textColourId, 0xff000000,

		0x1000440, /*LassoComponent::lassoFillColourId*/        0x66dddddd,
		0x1000441, /*LassoComponent::lassoOutlineColourId*/     0x99111111,

		0x1005000, /*MidiKeyboardComponent::whiteNoteColourId*/               0xffffffff,
		0x1005001, /*MidiKeyboardComponent::blackNoteColourId*/               0xff000000,
		0x1005002, /*MidiKeyboardComponent::keySeparatorLineColourId*/        0x66000000,
		0x1005003, /*MidiKeyboardComponent::mouseOverKeyOverlayColourId*/     0x80ffff00,
		0x1005004, /*MidiKeyboardComponent::keyDownOverlayColourId*/          0xffb6b600,
		0x1005005, /*MidiKeyboardComponent::textLabelColourId*/               0xff000000,
		0x1005006, /*MidiKeyboardComponent::upDownButtonBackgroundColourId*/  0xffd3d3d3,
		0x1005007, /*MidiKeyboardComponent::upDownButtonArrowColourId*/       0xff000000,
		0x1005008, /*MidiKeyboardComponent::shadowColourId*/                  0x4c000000,

		0x1004500, /*CodeEditorComponent::backgroundColourId*/                0xffffffff,
		0x1004502, /*CodeEditorComponent::highlightColourId*/                 textHighlightColour,
		0x1004503, /*CodeEditorComponent::defaultTextColourId*/               0xff000000,
		0x1004504, /*CodeEditorComponent::lineNumberBackgroundId*/            0x44999999,
		0x1004505, /*CodeEditorComponent::lineNumberTextId*/                  0x44000000,

		0x1007000, /*ColourSelector::backgroundColourId*/                     0xffe5e5e5,
		0x1007001, /*ColourSelector::labelTextColourId*/                      0xff000000,

		0x100ad00, /*KeyMappingEditorComponent::backgroundColourId*/          0x00000000,
		0x100ad01, /*KeyMappingEditorComponent::textColourId*/                0xff000000,

		FileSearchPathListComponent::backgroundColourId, 0xffffffff,

		FileChooserDialogBox::titleTextColourId, 0xff000000,
	};

	for (int i = 0; i < numElementsInArray(standardColours); i += 2)
		lookAndFeel.setColour((int)standardColours[i], Colour((uint32)standardColours[i + 1]));

}