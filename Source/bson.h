/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#if !defined(BSON_H)
#define BSON_H
#include <vector>
#include <string>
#include <memory>
#include <sstream>
#include <cstdint>

// Element types
enum ElementType : char
{
	/// Indicates the type of a BSONElement.
	ET_DOUBLE = 0x01,
	ET_UTF8STRING = 0x02,
	ET_DOCUMENT = 0x03,
	ET_ARRAY = 0x04,
	ET_BINARY = 0x05,
	ET_BOOLEAN = 0x08,
	ET_UTCDATETIME = 0x09,
	ET_INT32 = 0x10,
	ET_INT64 = 0x12,

};


class BSONElement
{

public:
	BSONElement() :m_pNext(nullptr){};
	virtual ~BSONElement() {};
	virtual std::unique_ptr<BSONElement> Clone() const = 0;
	virtual void ToBinary(std::ostringstream& os) const = 0;
	virtual std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) = 0;
	virtual std::string GetName() const = 0;

	void SetNextSibling(BSONElement* pNext)
	{
		m_pNext = pNext;
	}
	BSONElement* GetNextSibling()
	{
		return m_pNext;
	}
private:
	BSONElement* m_pNext;
};
using BSONElementPtr = std::unique_ptr<BSONElement>;



class BSONElementInt32: public BSONElement
{
private:
	std::string		m_name;
	std::int32_t	m_value;
	
public:

	BSONElementInt32() {}
	virtual ~BSONElementInt32() {}
	BSONElementInt32(const BSONElementInt32& other)
	{
		m_name = other.m_name;
		m_value = other.m_value;
	}

	BSONElementInt32(std::string name, std::int32_t val = 0)
	{
		m_name = name;
		m_value = val;
	}

	virtual std::unique_ptr<BSONElement> Clone() const override
	{
		return std::unique_ptr<BSONElementInt32>(new BSONElementInt32(*this));
	}

	std::string GetName() const override
	{
		return m_name;
	}
	void SetName(const std::string& name)
	{
		m_name = name;
	}
	std::int32_t GetValue() const
	{
		return m_value;
	}
	void SetValue(std::int32_t val)
	{
		m_value = val;
	}
	virtual void ToBinary(std::ostringstream& os) const override;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) override;

};

class BSONElementString : public BSONElement
{
private:
	std::string		m_name;
	std::string		m_value;

public:
	BSONElementString() { }
	virtual ~BSONElementString() { }
	BSONElementString(const BSONElementString& other)
	{
		m_name = other.m_name;
		m_value = other.m_value;
	}
	BSONElementString(const std::string& name, const std::string& val = "")
	{
		m_name = name;
		m_value = val;
	}
	/** Useful constructor for array elements. The name parameter is converted to text and used as the name.
	*/
	BSONElementString(uint32_t name, const std::string& val);

	virtual std::unique_ptr<BSONElement> Clone() const override
	{
		return std::unique_ptr<BSONElementString>(new BSONElementString(*this));
	}

	void SetValue(std::string val)
	{
		m_value = val;
	}
	std::string GetValue() const
	{
		return m_value;
	}
	std::string GetName() const
	{
		return m_name;
	}
	virtual void ToBinary(std::ostringstream& os) const override;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) override;

};

class BSONElementBinary : public BSONElement
{
private:
	std::string				m_name;
	std::vector<uint8_t>	m_value;

public:
	BSONElementBinary() { }
	virtual ~BSONElementBinary() { }

	BSONElementBinary(const BSONElementBinary& other)
	{
		m_name = other.m_name;
		m_value = other.m_value;
	}
	BSONElementBinary(const BSONElementBinary&& other)
	{
		m_name = std::move(other.m_name);
		m_value = std::move(other.m_value);
	}
	BSONElementBinary(const std::string& name, const std::vector<uint8_t>& val)
	{
		m_name = name;
		m_value = val;
	}
	virtual std::unique_ptr<BSONElement> Clone() const override
	{
		return std::unique_ptr<BSONElementBinary>(new BSONElementBinary(*this));
	}
	std::string GetName() const
	{
		return m_name;
	}

	std::vector<uint8_t> GetData() const
	{
		return m_value;
	}
	void SetValue(std::vector<uint8_t> val)
	{
		m_value = val;
	}

	virtual void ToBinary(std::ostringstream& os) const override;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) override;

};

class BSONDocument
{
private:
	std::vector<BSONElementPtr>	elemList;
	std::int32_t m_length;

public:
	BSONDocument() {}
	virtual ~BSONDocument() {}
	BSONDocument(const BSONDocument& other)
	{
		if (this != &other)
		{
			for (auto& e : other.elemList)
			{
				elemList.push_back(e->Clone());
			}
			m_length = other.m_length;
		}
	}

	/** @brief Assignment operator.

	This is required because a document contains a vector of unique_ptrs.
	unique_ptrs cannot be copied (and we wouldn't want to just copy the pointers anyway).
	So we need to explicitly make copies of all the elements in the vector.

	@param other	A reference to the document object on right side of the equals sign.
	@returns		A reference to 'this', the newly assigned document.
	*/
	BSONDocument& operator=(const BSONDocument& other)
	{
		if (this != &other)		// self-assignment check 
		{
			elemList.clear();
			for (auto& e : other.elemList)
			{
				elemList.push_back(e->Clone());		// make a copy of the element, whatever type that element is.
			}
			m_length = other.m_length;
		}
		return *this;
	}

	void ToBinary(std::ostringstream& os) const;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0);

	void AddElement(BSONElementPtr&& pElement)
	{
		elemList.push_back(std::move(pElement));

		// Setup the "next" link
		if (elemList.size() > 1)
		{
			elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
		}
	}

	void AddElementString(const std::string& name, const std::string& value)
	{
		elemList.push_back(std::make_unique<BSONElementString>(name, value));
		
		// Setup the "next" link
		if (elemList.size() > 1)
		{
			elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
		}
	}
	void AddElementBinary(const std::string& name, const std::vector<uint8_t>& value)
	{
		elemList.push_back(std::make_unique<BSONElementBinary>(name, value));
		
		// Setup the "next" link
		if (elemList.size() > 1)
		{
			elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
		}
	}

	void AddElementDocument(const std::string& name, const BSONDocument& value);
	void AddElementDocument(uint32_t nameAsInt, const BSONDocument& value);

	void AddElementArray(const std::string& name, const BSONDocument& value);
	void AddElementInt32(const std::string& name, int32_t value);

	int GetNumElements() const
	{
		return elemList.size();
	}

	BSONElement* GetElement(const std::string& name) const;
	BSONElement* GetElement(uint32_t nameFromInt) const;
	BSONElement* GetFirstElement() const;

};


class BSONElementDocument : public BSONElement
{
private:
	std::string		m_name;
	BSONDocument	m_value;

public:
	BSONElementDocument() {}
	virtual ~BSONElementDocument() {}
	BSONElementDocument(const BSONElementDocument& other)
	{
		m_name = other.m_name;
		m_value = other.m_value;
	}
	BSONElementDocument(const std::string& name, const BSONDocument& val)
	{
		m_name = name;
		m_value = val;
	}
	/** Useful constructor for array elements. The name parameter is converted to text and used as the name.
	*/
	BSONElementDocument(uint32_t name, const BSONDocument& val);

	virtual std::unique_ptr<BSONElement> Clone() const override
	{
		return std::unique_ptr<BSONElementDocument>(new BSONElementDocument(*this));
	}
	void SetValue(BSONDocument val)
	{
		m_value = val;
	}
	BSONDocument& GetDoc()
	{
		return m_value;
	}
	std::string GetName() const
	{
		return m_name;
	}
	virtual void ToBinary(std::ostringstream& os) const override;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) override;

};

class BSONElementArray : public BSONElement
{
private:
	std::string		m_name;
	BSONDocument	m_value;

public:
	BSONElementArray() {}
	virtual ~BSONElementArray() {}
	BSONElementArray(const BSONElementArray& other)
	{
		m_name = other.m_name;
		m_value = other.m_value;
	}
	BSONElementArray(const std::string& name, const BSONDocument& val)
	{
		m_name = name;
		m_value = val;
	}
	virtual std::unique_ptr<BSONElement> Clone() const override
	{
		return std::unique_ptr<BSONElementArray>(new BSONElementArray(*this));
	}
	void SetValue(BSONDocument val)
	{
		m_value = val;
	}
	BSONDocument& GetDoc()
	{
		return m_value;
	}
	std::string GetName() const
	{
		return m_name;
	}
	virtual void ToBinary(std::ostringstream& os) const override;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) override;

};


class BSONElementStringArray : public BSONElement
{
private:
	std::string		m_name;
	BSONDocument	m_value;
public:
	BSONElementStringArray() {}
	virtual ~BSONElementStringArray() {}
	BSONElementStringArray(const BSONElementStringArray& other)
	{
		m_name = other.m_name;
		m_value = other.m_value;
	}
	virtual std::unique_ptr<BSONElement> Clone() const override
	{
		return std::unique_ptr<BSONElementStringArray>(new BSONElementStringArray(*this));
	}

	BSONDocument& GetArrayDoc()
	{
		return m_value;
	}
	std::string GetName() const override
	{
		return m_name;
	}

	int GetNumElements()
	{
		return m_value.GetNumElements();
	}
	virtual void ToBinary(std::ostringstream& os) const override;
	std::int32_t Parse(const std::vector<uint8_t>& buf, std::size_t startIndex = 0) override;
};


#endif

