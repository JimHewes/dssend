/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#pragma once
#include <cassert>
#include <memory>
#include <vector>
#include <string>
#include <queue>
#include <cstdint>
#include <functional>
#include "tsqueue.h"
#include <boost/thread.hpp>
#include <boost/interprocess/sync/interprocess_semaphore.hpp>
#include <boost/atomic.hpp>

#ifdef noexcept
#undef noexcept
#endif
#define noexcept  throw()

class MidiInDeviceImpl;
class MidiOutDeviceImpl;

class MidiInDevice
{
public:
	MidiInDevice();
	~MidiInDevice();

	std::vector<std::string> GetDeviceList();

	void Open(const std::string deviceName);
	void Open(uint32_t deviceIndex);
	void Close();
	bool IsOpen() const noexcept;
	void RecordingOn(int bufferLength);
	void RecordingOff() noexcept;
	bool IsRecordingOn();
	bool ReceiveData(std::vector<unsigned char>& buffer, uint32_t startIndex, uint32_t& bytesRecorded);

private:	// member variables
	std::unique_ptr<MidiInDeviceImpl> m_pImpl;
};


class MidiOutDevice
{
public:
	MidiOutDevice();
	~MidiOutDevice();

	std::vector<std::string> GetDeviceList();
	void Open(const std::string deviceName);
	void Open(uint32_t deviceIndex);
	void Close();

	bool SendSysEx(std::vector<unsigned char>& msg, uint32_t lengthToSend);

	bool IsOpen() const noexcept;

private:
	std::unique_ptr<MidiOutDeviceImpl> m_pImpl;
};

using MidiInDeviceUPtr = std::unique_ptr<MidiInDevice>;
using MidiOutDeviceUPtr = std::unique_ptr<MidiOutDevice>;



class TransferRequest
{
public:
	TransferRequest()
		:NumberOfBytesRead(0)
		, ReceiveStartIndex(0)
		, userData(0)
		, cancelRequest(false)
		, Success(false)
		, Callback(nullptr) 
	{}
	int						NumberOfBytesRead;
	std::vector<uint8_t>	ReceiveBuffer;
	std::vector<uint8_t>	SendBuffer;
	size_t					ReceiveStartIndex;
	int						ReceiveLength;	// Not really needed? I think it was originally just used by the caller to confirm the correct number of bytes were received.
	bool					Success;		///< true for success, false for error. default value is false.
	std::function<void(std::shared_ptr<TransferRequest> req)> Callback;
	uint32_t				userData;
	bool					cancelRequest;						///< If true, all requests in the queue before this one are cancelled.

	// void ReportBack()
	// {
	// 	if (Callback != nullptr)
	// 	{
	// 		Callback(*this);
	// 	}
	// }
};
using TransferRequestSPtr = std::shared_ptr<TransferRequest>;


#if 0
class ThreadClass
{
public:
	ThreadClass(MidiInDevice& midiInDevice, MidiOutDevice midiOutDevice)
		:m_midiInDevice(midiInDevice)
		, m_midiOutDevice(midiOutDevice)
	{}
	void operator()()
	{

	}

private:
	MidiInDevice& m_midiInDevice;
	MidiOutDevice& m_midiOutDevice;
};
#endif // 0

const int MAX_QUEUE_SIZE = 50;

class MidiIoDevice
{
public:
	MidiIoDevice() : m_numCancelRequests(0), m_quit(false) {}
	virtual ~MidiIoDevice();
	void Open(std::string inputDeviceName, std::string outputDevicename);
	bool IsOpen() const noexcept;
	void Close();


	/** @brief  Submit a MIDI request to the device.

		We take a performance hit using the shared pointer instead of unique pointer or native pointer. But using a native pointer would put a burden 
		on the caller to maintain the existence of the request object. I would have preferred to use a unique pointer, passing the ownership in to the 
		MidiIoDevice and back out on the callback. But the Boost lockfree queue doesn't appear to support move semantics. I could use std::queue but then 
		I need to manage my own mutex to guard it and it wouldn't be lockfree. That may be OK, and I'd probably want to build my own wrapper class then, 
		but it would take more time and might not even be better performance. The shared pointer seemed to solve the problem the easiest for now.
		(When building a generalized message queue for use between threads, I'd want to suport move semantics. I wish Boost would add that.)
		
		@param pTransReq	A shared pointer the the transfer request. 
	*/
	void SubmitRequest(TransferRequestSPtr pTransReq);

	int Read(TransferRequest& transReq);
	void Write(TransferRequest& transReq);
private:
	void MidiIoDevice::TransferThreadFunction();
	void ProcessRequest(TransferRequest& transReq);



	MidiInDevice m_midiInDevice;
	MidiOutDevice m_midiOutDevice;

	tsqueue<TransferRequestSPtr>		m_queue;
	std::unique_ptr<boost::thread>		m_pWorkerThread;
	std::unique_ptr<boost::interprocess::interprocess_semaphore>	m_eventSignal;	///< A semaphore is used to keep count of how many transfer requests are currently in the queue.
	boost::atomic_int m_numCancelRequests;
	boost::atomic_bool m_quit;	///< when set to true, the thread should exit

	boost::mutex m_mutex;

};

