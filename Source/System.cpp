/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "System.h"
#include "Program.h"


using std::string;
using std::vector;
static const uint32_t totalPCMSpaceInSamples = 262144;


System::System(const string& systemName)
{ 
	m_systemParams.SetSystemName(systemName);
}

System::System(DSS1* pDSS1, ProgressCallback callback )
{
	FromDevice(pDSS1, callback);
}

System::System(const SystemParams& systemParams, MultisoundList&& multisoundList)
{
	m_systemParams = systemParams;
	m_multisoundList = std::move(multisoundList);
}

System::System(const System& other)
{
	*this = other;
}

System& System::operator=(const System& other)
{
	m_systemParams = other.m_systemParams;

	m_multisoundList.clear();
	for (auto& ms : other.m_multisoundList)
	{
		m_multisoundList.push_back(std::make_unique<Multisound>(*ms));
	}
	return *this;
}

System& System::operator=(System&& other)
{
	m_systemParams = other.m_systemParams;

	m_multisoundList.clear();
	m_multisoundList = std::move(other.m_multisoundList);

	return *this;
}

void System::SetName(const std::string& systemName)
{
	m_systemParams.SetSystemName(systemName);
}

std::string System::GetName()
{
	return m_systemParams.GetSystemName();
}

/**	This constructor is here for completeness but is generally not preferred due to performance.
*/
// System::System(SystemParams systemParams, MultisoundList& multisoundList)
// {
// 	m_systemParams = systemParams;
// 	for (auto& ms : multisoundList)
// 	{
// 		m_multisoundList.push_back(std::make_unique<Multisound>(*ms));
// 	}
// }


/**	Retrieves the current System from the DSS-1.

	@param[in] pDSS1	A native pointer to the DSS1 object from which the System is obtained. This must have already been opened with a MidiDevice.
	@param[in] callback	An optional callback function that will be called to report progress. This can be set to nullptr for no progress reporting.

	@retval true	If the process completed.
	@retval false	If it was canceled by the callback function.
*/
bool System::FromDevice(DSS1* pDSS1, ProgressCallback callback)
{
	pDSS1->SetPlayMode();

	// We provide our own callback for the systemParams transfer so that we report its transfer as a part of the larger System transfer.
	// We don't know the real size at first because we don't have the multisound list yet. So we use a worst case estimate and update as we go.
	// The SystemParams will tell us its best estimate of the systemParams size in the totalBytes parameter, which we just add to pscDataSize
	auto systemParamsCallback = [&](std::string objectBeingRead, uint32_t bytesSoFar, uint32_t totalBytes, bool /*isDone*/) -> bool
	{
		uint32_t estimatedDataSize = m_systemParams.GetPCMDataSizeInSamples();
		if (estimatedDataSize == 0)
		{
			estimatedDataSize = 512 * 1024;
		}
		estimatedDataSize += totalBytes;
		return callback(objectBeingRead, bytesSoFar, estimatedDataSize, false);
	};

	if (!m_systemParams.FromDevice(pDSS1, callback ? systemParamsCallback : callback))
	{
		return false;
	}

	// Now we should know the real size.
	uint32_t systemParamsSize = m_systemParams.GetTotalDataSize();
	uint32_t totalDataSize = systemParamsSize + m_systemParams.GetPCMDataSizeInSamples() * 2;
	uint8_t	numMultisoundsNames = m_systemParams.GetNumberOfMultisoundNames();
	uint32_t pcmDataStartAddress = 0;
	uint32_t totalMultisoundBytesSoFar = 0;

	// We provide our own callback for the Multisound transfer so that we report its transfer as a part of the larger System transfer.
	auto multisoundCallback = [&](std::string objectBeingRead, uint32_t bytesSoFar, uint32_t totalBytes, bool isDone) -> bool
	{
		bool continueRec = callback(objectBeingRead, systemParamsSize + totalMultisoundBytesSoFar + bytesSoFar, totalDataSize, false);
		if (isDone)	// This isDone refers to the Multisound saying that it is done that one Multisound. So we can then update totalMultisoundBytesSoFar.
		{
			totalMultisoundBytesSoFar += totalBytes;
		}
		return continueRec;
	};

	bool continueReceive = true;
	for (size_t i = 0; i < numMultisoundsNames; ++i)
	{
		MultisoundUPtr pMultisound = std::make_unique<Multisound>();
		continueReceive = pMultisound->FromDevice(pDSS1, static_cast<uint8_t>(i), pcmDataStartAddress, callback ? multisoundCallback : callback);
		if (!continueReceive)	// cancelled
		{
			break;
		}
		m_multisoundList.push_back(std::move(pMultisound));

		pcmDataStartAddress += m_multisoundList[m_multisoundList.size()-1]->GetPCMDataLengthInSamples();
	}

	// Signal done (only if not canceled)
	if (callback && continueReceive)
	{
		callback("Done", totalDataSize, totalDataSize, true);
	}
	return continueReceive;
}

/** Sends the System to the specified DSS1 device over MIDI.

	@param[in] pDSS1	A native pointer to the DSS1 device to send the System to.
						This must have already been opened with a MidiDevice.
	@param[in] callback	An optionsl callback function that will be called regularly to report 
						progress. This can be set to nullptr for no progress reporting.
						Also, the callback function can cencel the transfer.

	@retval true	If the transfer was completed.
	@retval false	If the transfer was canceled by the callback function before finishing.
	*/
bool System::ToDevice(DSS1* pDSS1, ProgressCallback callback)
{
	pDSS1->SetPlayMode();

	bool continueSend = true;

	uint32_t systemParamsSize = m_systemParams.GetTotalDataSize();
	uint32_t totalDataSize = systemParamsSize + m_systemParams.GetPCMDataSizeInSamples() * 2;
	uint32_t totalMultisoundBytesSoFar = 0;

	// We provide our own callback for the Multisound transfer so that we report its transfer as a part of the larger System transfer.
	auto multisoundCallback = [&](std::string objectBeingRead, uint32_t bytesSoFar, uint32_t totalBytes, bool isDone) -> bool
	{
		bool continueRec = callback(objectBeingRead, systemParamsSize + totalMultisoundBytesSoFar + bytesSoFar, totalDataSize, false);
		if (isDone)	// This isDone refers to the Multisound saying that it is done that one Multisound. So we can then update totalMultisoundBytesSoFar.
		{
			totalMultisoundBytesSoFar += totalBytes;
		}
		return continueRec;
	};

	uint8_t	numMultisoundsNames = m_systemParams.GetNumberOfMultisoundNames();
	assert(numMultisoundsNames == m_multisoundList.size());
	uint32_t pcmDataStartAddress = 0;

	// Check that all the data can fit. If you've loaded native files for the DSM-1 or modified DSS-1, 
	// it may be more than can fit into a regular DSS-1.
	if (GetUsedPCMSpaceInSamples() > totalPCMSpaceInSamples)
	{
		throw std::runtime_error("The System's data is too large to fit into a standard DSS-1.");
	}


	for (size_t i = 0; i < numMultisoundsNames; ++i)
	{
		continueSend = m_multisoundList[i]->ToDevice(pDSS1, static_cast<uint8_t>(i), pcmDataStartAddress, multisoundCallback);
		if (!continueSend)
		{
			return false;
		}

		pcmDataStartAddress += m_multisoundList[i]->GetPCMDataLengthInSamples();
	}

	// We provide our own callback for the Multisound transfer so that we report its transfer as a part of the larger System transfer.
	auto systemParamsCallback = [&](std::string objectBeingRead, uint32_t bytesSoFar, uint32_t /* totalBytes*/, bool /*isDone*/) -> bool
	{
		return callback(objectBeingRead, totalMultisoundBytesSoFar + bytesSoFar, totalDataSize, false);
	};

	m_systemParams.ToDevice(pDSS1, systemParamsCallback);

	// Set to program 1
	pDSS1->ProgramChange(0);

	// Signal done (only if not canceled)
	if (callback && continueSend)
	{
		callback("Done", totalDataSize, totalDataSize, true);
	}
	return continueSend;

}

BSONDocument System::ToBSON()
{
	BSONDocument systemDoc;

	systemDoc.AddElementDocument("SystemParams", m_systemParams.ToBSON());

	BSONDocument multicoundsDoc;
	for (auto &ms : m_multisoundList)
	{
		multicoundsDoc.AddElementDocument(ms->GetName(), ms->ToBSON());
	}
	systemDoc.AddElementArray("Multisounds", multicoundsDoc);

	return systemDoc;
}

string System::GetConflictingMultisoundName(Program& program)
{
	string result;
	bool done = false;
	for (uint8_t osc = 0; osc < 2 && !done; ++osc)
	{
		uint8_t oscMultisoundIndex = program.GetParameter(osc == 0 ? OSC1MULTISOUNDNUMBER : OSC2MULTISOUNDNUMBER);
		Multisound* pMultisound = program.GetMultisoundPtr(oscMultisoundIndex);
		if (pMultisound)
		{
			string prgMultisoundName = pMultisound->GetName();
			for (size_t i = 0; i < m_multisoundList.size() && !done; ++i)
			{
				if (prgMultisoundName == m_multisoundList[i]->GetName())
				{
					if (!m_multisoundList[i]->Equals(pMultisound))
					{
						result = prgMultisoundName;
						done = true;
					}
				}
			}
		}
	}
	return result;
}

/**	Determines whether the Multisounds in the Program can fit into the System.

	A Program may not fit because the total number of Multisounds would be greater than 16. Or, a Program may not fit because the PCM data will not fit.\n

	This function can be used to avoid getting an exception when calling AddProgram(). Or, if you just want to know ahead of time that a Program will fit without 
	actually adding it. This does not tell you if a Program can be successfully added to a System because it may be that the Multisounds fit but there is a name conflict. 
	(Two different Multisounds with the same name.) 
*/
bool System::ProgramCanFit(Program& program)
{
	uint32_t PCMSpaceNeededInSamples = 0;
	uint8_t multisoundsToAdd = 0;

	// If both oscillators use the same multisound, then we only check the one multisound.
	uint8_t numOscsToCheck = program.GetParameter(OSC1MULTISOUNDNUMBER) == program.GetParameter(OSC2MULTISOUNDNUMBER) ? 1 : 2;
	for (uint8_t osc = 0; osc < numOscsToCheck; ++osc)
	{
		uint8_t oscMultisoundIndex = program.GetParameter(osc == 0 ? OSC1MULTISOUNDNUMBER : OSC2MULTISOUNDNUMBER);
		Multisound* pMultisound = program.GetMultisoundPtr(oscMultisoundIndex);

		if (pMultisound)
		{
			bool found = false;
			for (size_t i = 0; i < m_multisoundList.size() && !found; ++i)
			{
				if (m_multisoundList[i]->Equals(pMultisound))
				{
					found = true;
				}
			}
			if (!found)
			{
				PCMSpaceNeededInSamples += pMultisound->GetPCMDataLengthInSamples();
				++multisoundsToAdd;
			}
		}
	}

	return (PCMSpaceNeededInSamples <= GetFreePCMSpaceInSamples()) && (m_multisoundList.size() + multisoundsToAdd <= 16);
}

/**
	@param[in]	index	The index where to store the Program. Must be between 0 and 31 inclusive.
	@param[in]	program	A reference to a Program to add to the System. The Multisounds in the Program must fit into the System 
						and must not have any name conflicts.

	@throws	std::runtime_error	if the program cannot be added because a Multisound name conflicts with one in the System 
								or if the Multisounds cannot fit. Use GetConflictingMultisoundName() and ProgramCanFit() 
								to avoid these exceptions.
*/

void System::AddProgram(uint8_t programIndex, Program& program)
{
	assert(programIndex < 32);
	string conflictingName = GetConflictingMultisoundName(program);
	if (!conflictingName.empty())
	{
		std::ostringstream os;
		os << "Name conflict when adding program to System. The multisound name " << conflictingName << " is already in the system.";
		throw std::runtime_error(os.str().c_str());
	}
	if (!ProgramCanFit(program))
	{
		throw std::runtime_error("Not enough sample space to add program to System.");
	}

	// At this point it is known that the Program can safely be added to the System.

	m_systemParams.SetProgramParams(programIndex, program.GetParams());

	// If both oscillators use the same multisound, then we only check the one multisound.
	uint8_t numOscsToCheck = program.GetParameter(OSC1MULTISOUNDNUMBER) == program.GetParameter(OSC2MULTISOUNDNUMBER) ? 1 : 2;
	for (uint8_t osc = 0; osc < numOscsToCheck; ++osc)
	{
		uint8_t oscMultisoundIndex = m_systemParams.GetProgramParameter(programIndex, (osc == 0 ? OSC1MULTISOUNDNUMBER : OSC2MULTISOUNDNUMBER));
		Multisound* pMultisound = program.GetMultisoundPtr(oscMultisoundIndex);
		assert(pMultisound);
		if (pMultisound)
		{
#if 0
			bool found = false;
			for (uint8_t i = 0; i < m_multisoundNameList.size() && !found; ++i)
			{
				if (pMultisound->GetName() == m_multisoundNameList[i].name)
				{
					found = true;
				}
			}
			if (!found)
#endif
			// Note that there is no need to check whether the multisounds are really equal or not because that was already done above.
			// If the names are equal then the params and PCM data are known to be equal as well.
			auto found = std::find_if(m_multisoundList.begin(), m_multisoundList.end(), [pMultisound](MultisoundUPtr& i){ return pMultisound->GetName() == i->GetName(); });
			if (found != m_multisoundList.end())
			{
				// Add multisound name to the name list and multisound to the multisound list.
				MultisoundUPtr upMultisound = program.GetMultisoundCopy(oscMultisoundIndex);
				//m_multisoundNameList.push_back(MultisoundNameListItem(upMultisound->GetName().c_str(), upMultisound->GetPCMDataLengthInSamples()));
				m_systemParams.AppendMultisoundNameListItem(MultisoundNameListItem(upMultisound->GetName().c_str(), upMultisound->GetPCMDataLengthInSamples()));
				m_multisoundList.push_back(std::move(upMultisound));	// transfer ownership to the list

				assert(m_systemParams.GetNumberOfMultisoundNames() == m_multisoundList.size());

				// fixup the oscillator indexes.
				if (osc == 0)
				{
					m_systemParams.SetProgramParameter(programIndex, OSC1MULTISOUNDNUMBER, uint8_t(m_multisoundList.size() - 1));
				}
				if (osc == 1 || numOscsToCheck == 1)	// handle the case when there is only one multisound in the program.
				{
					m_systemParams.SetProgramParameter(programIndex, OSC2MULTISOUNDNUMBER, uint8_t(m_multisoundList.size() - 1));

					//m_programList[programIndex].SetParameter(OSC2MULTISOUNDNUMBER, uint8_t(m_multisoundNameList.size() - 1));
				}
			}
		}
	}
}

uint32_t System::GetUsedPCMSpaceInSamples()
{
	uint32_t total = 0;
	for (auto &i : m_multisoundList)
	{
		total += i->GetPCMDataLengthInSamples();
	}
	return total;

}

uint32_t System::GetFreePCMSpaceInSamples()
{
	uint32_t usedSpace = GetUsedPCMSpaceInSamples();
	return (usedSpace < totalPCMSpaceInSamples) ? totalPCMSpaceInSamples - usedSpace : 0;
}

bool System::HasMultisound(Multisound* pMultisound)
{
	bool found = false;
	for (auto &ms : m_multisoundList)
	{
		if (ms->Equals(pMultisound))
		{
			found = true;
			break;
		}
	}
	return found;

}

/** Generates a MultisoundNameList from the System's list of Multisounds and returns it.

*/
MultisoundNameList System::GetMultisoundNameList()
{
	MultisoundNameList mnl;

	for (auto& ms : m_multisoundList)
	{
		mnl.push_back(MultisoundNameListItem(ms->GetName().c_str(), ms->GetPCMDataLengthInSamples()));
	}
	return mnl;
}

SystemParams System::GetSystemParams()
{
	//return SystemParams(m_name, m_programList, m_multisoundNameList);
	return m_systemParams;
}

