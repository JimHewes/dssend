/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "Soundfont.h"

using std::vector;
using std::string;

/** Add sample data to the soundfont

	It is assumed that the start of the sample data is 0 and the end is the full length of the data. So these values 
	are obtained based on the size of the "data" vector parameter.\n
	\n
	The soundfont spec say that there should be at least 4 samples before the loop start and 4 after the loop end. 
	Ideally those four samples should be identical. This presents a couple problems which the spec does not explain. 
	For single cycle waves that have loop start at the first sample and loop end after the last sample, from where 
	are the four samples taken? The spec does say that the data after the loop end should be the same as the first 
	samples of the loop start. That makes sense. But if the four bytes before the loop start are to be identical to those,
	then why should they also be the same four bytes as the loop start? Shouldn't those four bytes be the same as the last 
	four bytes in the loop?\n
	And, if the sample is to continue after the loop when the key is released, then why should the four bytes after the 
	loop be the same as the four before the loop start? Rather those samples should be a continuation of where the loop 
	left off. (Though this case never happens with the DSS-1 because it never continues past a loop.)\n
	There are a lot of possibilities depending on where the loop points are and it seems there are different cases for each.\n
	\n
	The spec seems to be saying that the soundfont creator needs to cater to the interpolation algorithms by providing them 
	extra samples. But I think it should be the interpolation algorithm's job to handle all the possibilities. It should 
	acceopt if there are no samples after the loop and deal with it. After all, the algorithm is in the best position to 
	make the decision of what data to use based on how its algorithm works.


*/
SampleInfo Chunk_smpl::AddSampleData(vector<int16_t> data, bool loopEnabled, uint32_t loopStart, uint32_t loopEnd)
{
	SampleInfo retInfo;
	retInfo.loopEnabled = loopEnabled;
	retInfo.start = m_data.size();
	retInfo.end = data.size() + m_data.size();
	retInfo.loopStart = loopStart + m_data.size();
	retInfo.loopEnd = loopEnd + m_data.size();

	if (data.size() > 0)
	{
		if (loopEnabled)
		{
			assert(loopStart < data.size());
			assert(loopEnd <= data.size());

			if (loopStart < 4)
			{
				size_t numSamplesToAdd = 4 - loopStart;
				uint32_t indexToSource = loopEnd - 1;
				vector<uint16_t> preface;
				for (size_t i = 0; i < numSamplesToAdd; ++i)
				{
					preface.insert(preface.begin(), data[indexToSource]);
					if (indexToSource <= loopStart)
					{
						indexToSource = loopEnd - 1;
					}
					else
					{
						--indexToSource;
					}
				}
				m_data.insert(m_data.end(), preface.begin(), preface.end());

				retInfo.loopStart += numSamplesToAdd;
				retInfo.loopEnd += numSamplesToAdd;

				// In this case, the start of the sound is the same as the start of the loop.
				// The first 4 samples are never played but are just for the benefit of the interpolation.
				retInfo.start += numSamplesToAdd;
				retInfo.end += numSamplesToAdd;
			}

			m_data.insert(m_data.end(), data.begin(), data.end());

			size_t numSamplesAfterLoop = data.size() - loopEnd;
			if (numSamplesAfterLoop < 4)
			{
				size_t numSamplesToAdd = 4 - numSamplesAfterLoop;
				// I've seen loops as small as one sample. So we need to account for that. Only insert as many samples as you have.
				uint32_t indexToSource = loopStart;
				for (size_t i = 0; i < numSamplesToAdd; ++i)
				{
					m_data.push_back(data[indexToSource]);
					++indexToSource;
					if (indexToSource >= loopEnd)
					{
						indexToSource = loopStart;
					}
				}
				retInfo.end += numSamplesToAdd;
			}
		}
		else
		{
			m_data.insert(m_data.end(), data.begin(), data.end());
			m_data.insert(m_data.end(), 46, 0);	// as required by Soundfont spec.
		}
	}

	return retInfo;
}