/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "bson.h"
#include <sstream>
#include <algorithm>
#include <cstdint>
#include <cassert>

using std::int32_t;
using std::size_t;

void BSONDocument::ToBinary(std::ostringstream& os) const
{
	std::ostringstream outStr;
	for (std::size_t i = 0; i < elemList.size(); ++i)
	{
		elemList[i]->ToBinary(outStr);
	}

	int32_t len = outStr.str().length() + 5;	// +4 for length, +1 for 0 terminator
	os.write((char*)&len, sizeof(int32_t));
	os << outStr.str();
	os << '\0';

}

int32_t BSONDocument::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{

	m_length = *((int32_t *)&buf[startIndex]);
	startIndex += 4;

	BSONElementPtr pElem;
	//elemList.clear();

	while (buf[startIndex] != 0)
	{
		switch (buf[startIndex])
		{
			case ET_INT32:
				pElem = std::make_unique<BSONElementInt32>();
				break;
			case ET_INT64:
				//pElem = std::make_shared<BSONElementInt64>();
				break;
			case ET_DOCUMENT:
				pElem = std::make_unique<BSONElementDocument>();
				break;
			case ET_ARRAY:
				pElem = std::make_unique<BSONElementArray>();
				break;
			case ET_UTF8STRING:
				pElem = std::make_unique<BSONElementString>();
				break;
			case ET_BINARY:
				pElem = std::make_unique<BSONElementBinary>();
				break;
			default:
				throw std::runtime_error("Bad BSON data.");
		};
		startIndex = pElem->Parse(buf, startIndex);
		elemList.push_back(std::move(pElem));

		// Setup the "next" link
		if (elemList.size() > 1)
		{
			elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
		}
	}
	++startIndex;	// skip over the document's terminating zero that ended the while loop

	return startIndex;
}

/** Gets a pointer to the first element found in the document with the given name.

	The comparison is case-sensitive. It is up to the caller to downcast correctly to the derived element type.

	@param[in] name		The name of the element to get.
	@returns			A native pointer to the element, or nullptr if no element with that name was found.
*/
BSONElement* BSONDocument::GetElement(const std::string& name) const
{
	auto iter = std::find_if(elemList.begin(), elemList.end(), [&name](const BSONElementPtr& pElem) { return pElem->GetName() == name; });
	return (iter != elemList.end()) ? (*iter).get() : nullptr;
}

/** Gets a pointer to the first element in the document that has the name that is a text form of an integer.

	This is useful for getting elements from an Array type of document. It is up to the caller to downcast correctly 
	to the derived element type.

	@param[in] nameFromInt	An integer that will be converted to text to form the name of the element requested.
	@returns				A native pointer to the element, or nullptr if no element with that name was found.
	*/
BSONElement* BSONDocument::GetElement(uint32_t nameFromInt) const
{
	std::ostringstream os;
	os << nameFromInt;
	std::string name = os.str();
	auto iter = std::find_if(elemList.begin(), elemList.end(), [&name](const BSONElementPtr& pElem) { return pElem->GetName() == name; });
	return (iter != elemList.end()) ? (*iter).get() : nullptr;
}

BSONElement* BSONDocument::GetFirstElement() const
{
	BSONElement* pElem = nullptr;
	if (elemList.size() > 0)
	{
		pElem = elemList[0].get();
	}
	return pElem;
}


void BSONDocument::AddElementDocument(const std::string& name, const BSONDocument& value)
{
	elemList.push_back(std::make_unique<BSONElementDocument>(name, value));

	// Setup the "next" link
	if (elemList.size() > 1)
	{
		elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
	}
}

/** Converts the integer name parameter to text. Useful for adding Documents as array elements.
*/
void BSONDocument::AddElementDocument(uint32_t nameAsInt, const BSONDocument& value)
{
	std::ostringstream os;
	os << nameAsInt;
	elemList.push_back(std::make_unique<BSONElementDocument>(os.str(), value));

	// Setup the "next" link
	if (elemList.size() > 1)
	{
		elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
	}
}


void BSONDocument::AddElementArray(const std::string& name, const BSONDocument& value)
{
	elemList.push_back(std::make_unique<BSONElementArray>(name, value));

	// Setup the "next" link
	if (elemList.size() > 1)
	{
		elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
	}
}

void BSONDocument::AddElementInt32(const std::string& name, int32_t value)
{
	elemList.push_back(std::make_unique<BSONElementInt32>(name, value));

	// Setup the "next" link
	if (elemList.size() > 1)
	{
		elemList[elemList.size() - 2]->SetNextSibling(elemList[elemList.size() - 1].get());
	}
}

int32_t BSONElementInt32::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{
	assert(startIndex < buf.size());

	unsigned char jtype = buf[startIndex++];
	jtype;	// avoid warning
	assert(jtype == ET_INT32);

	m_name = (char*)&buf[startIndex];
	startIndex += m_name.length() + 1; 	// +1 to skip over zero terminator.

	m_value = *((int32_t*)&buf[startIndex]);
	startIndex += sizeof(int32_t);

	return startIndex;
}

void BSONElementInt32::ToBinary(std::ostringstream& os) const
{
	os << (unsigned char)ET_INT32;
	os << m_name << '\0';
	os.write((char*)&m_value, sizeof(std::int32_t));
}

BSONElementString::BSONElementString(uint32_t name, const std::string& val)
{
	std::ostringstream os;
	os << name;
	m_name = os.str();
	m_value = val;
}

int32_t BSONElementString::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{
	assert(startIndex < buf.size());

	unsigned char jtype = buf[startIndex++];
	jtype;	// avoid warning
	assert(jtype == ET_UTF8STRING);

	m_name = (char*)&buf[startIndex];
	startIndex += m_name.length() + 1; 	// +1 to skip over zero terminator.

	int32_t lengthOfString = *(int32_t*)&buf[startIndex];
	lengthOfString;	// avoid warning
	startIndex += sizeof(int32_t);

	m_value = (char*)&buf[startIndex];
	startIndex += m_value.length() + 1; 	// +1 to skip over zero terminator.
	assert((size_t)lengthOfString == m_value.length() + 1);

	return startIndex;
}

void BSONElementString::ToBinary(std::ostringstream& os) const
{
	os << (unsigned char)ET_UTF8STRING;
	os << m_name << '\0';
	std::int32_t len = m_value.size() + 1;
	os.write((char*)&len, sizeof(std::int32_t));
	os << m_value << '\0';
}

void BSONElementBinary::ToBinary(std::ostringstream& os) const
{
	os << (unsigned char)ET_BINARY;
	os << m_name << '\0';
	std::int32_t len = m_value.size();
	os.write((char*)&len, sizeof(std::int32_t));
	os << '\x80';	// subtype user defined
	os.write((char*)&m_value[0], len);
}

int32_t BSONElementBinary::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{
	assert(startIndex < buf.size());

	unsigned char jtype = buf[startIndex++];
	jtype;	// avoid warning
	assert(jtype == ET_BINARY);

	m_name = (char*)&buf[startIndex];
	startIndex += m_name.length() + 1; 	// +1 to skip over zero terminator.

	int32_t lengthOfBinaryData = *(int32_t*)&buf[startIndex];
	startIndex += sizeof(int32_t);

	uint8_t subtype = buf[startIndex];
	subtype;	// avoid warning
	++startIndex;
	assert(subtype == 0x80);

	m_value.resize(lengthOfBinaryData);
	memcpy(&m_value[0], &buf[startIndex], lengthOfBinaryData);
	startIndex += lengthOfBinaryData;

	return startIndex;
}

BSONElementDocument::BSONElementDocument(uint32_t name, const BSONDocument& val)
{
	std::ostringstream os;
	os << name;
	m_name = os.str();
	m_value = val;
}

int32_t BSONElementDocument::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{
	assert(startIndex < buf.size());

	unsigned char jtype = buf[startIndex++];
	jtype;	// avoid warning
	assert(jtype == ET_DOCUMENT);

	m_name = (char*)&buf[startIndex];
	startIndex += m_name.length() + 1; 	// +1 to skip over zero terminator.

	startIndex = m_value.Parse(buf, startIndex);

	return startIndex;
}

void BSONElementDocument::ToBinary(std::ostringstream& os) const
{
	os << (unsigned char)ET_DOCUMENT;
	os << m_name << '\0';
	m_value.ToBinary(os);
}


int32_t BSONElementArray::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{
	assert(startIndex < buf.size());

	unsigned char jtype = buf[startIndex++];
	jtype;	// avoid warning
	assert(jtype == ET_ARRAY);

	m_name = (char*)&buf[startIndex];
	startIndex += m_name.length() + 1; 	// +1 to skip over zero terminator.

	startIndex = m_value.Parse(buf, startIndex);

	return startIndex;
}

void BSONElementArray::ToBinary(std::ostringstream& os) const
{
	os << (unsigned char)ET_ARRAY;
	os << m_name << '\0';
	m_value.ToBinary(os);
}

int32_t BSONElementStringArray::Parse(const std::vector<uint8_t>& buf, size_t startIndex)
{
	assert(startIndex < buf.size());

	unsigned char jtype = buf[startIndex++];
	jtype;	// avoid warning
	assert(jtype == ET_ARRAY);

	m_name = (char*)&buf[startIndex];
	startIndex += m_name.length() + 1; 	// +1 to skip over zero terminator.

	startIndex = m_value.Parse(buf, startIndex);

	return startIndex;
}

void BSONElementStringArray::ToBinary(std::ostringstream& os) const
{
	os << (unsigned char)ET_ARRAY;
	os << m_name << '\0';
	m_value.ToBinary(os);
}