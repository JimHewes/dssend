/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include <memory>
#include <cassert>
#include "Disk.h"

struct SystemListboxModel : public ListBoxModel
{
public:
	SystemListboxModel() : m_pDisk(nullptr){}

	void SetDisk(Disk* pDisk)
	{
		m_pDisk = pDisk;

	}

	/** Returns the width in pixels the listbox will need to be in order to hold all system names in the current disk without truncating them.
	
		This is kind of a workaround to deal with the problem that JUCE listboxes don't have an easy way to support horizontal 
		scroll bars.\n
		\n
		Scrollbars are managed by viewports. If you look in the JUCE Listbox::ListViewport::updateVisibleArea() function you 
		can see that the viewport always resizes the width of it's content (the Listbox) to be the width of the visible area. 
		So no horizontal scrollbar ever appears. The exception to that is when you define the minimumRowWidth in the Listbox. 
		Then the Listbox can be wider than the visible area and a scrollbar appears. But you don't want it to be the same width 
		all the time, otherwise you'll get a scrollbar when you don't need one. So what I do here is to have the SystemListboxModel 
		figure out how wide the content needs to be based on all the system names and then report that. This depends on the font. 
		The font is normally provided by the Graphics object and that is only available in the paint() function. So we assume a 
		standard (sans serif) font and that the Listbox is going to set a height of 22 for listbox rows. (If that's ever not the 
		case you need to change this function.)\n
		\n
		The calling application should make use of this function by getting the width and then calling Listbox::setMinimumContentWidth() 
		whenever any of the system names are changed.

		@returns The width in pixels needed to contain all system names without truncating them.
	*/
	int GetWidthForHorizontalScroll() const
	{
		Font fnt(22 * 0.7f);
		int maxWidth = 0;
		if (m_pDisk)
		{
			for (uint32_t s = 0; s < m_pDisk->GetNumberOfSystems(); ++s)
			{
				maxWidth = std::max(maxWidth, fnt.getStringWidth(m_pDisk->GetSystemName(s)));
			}
		}

		// add 10 because the paint() function will indent the text from the left by 5 and we want the same on the right.
		return maxWidth + 10;
	}

	virtual int getNumRows() override
	{
		int numRows = 0;
		if (m_pDisk)
		{
			numRows = m_pDisk->GetNumberOfSystems();
		}
		return numRows;
	}

	virtual void paintListBoxItem(int rowNumber,
		Graphics& g,
		int width, int height,
		bool rowIsSelected) override
	{
		// The documentation for ListBoxModel::paintListBoxItem() says that the row number might be 
		// greater than the number of rows we report in getNumRows(). (Not sure why) So don't assume 
		// that it's always less.
		if (rowNumber < getNumRows())
		{
			if (rowIsSelected)
				g.fillAll(Colours::lightskyblue);

			g.setColour(Colours::black);
			g.setFont(height * 0.7f);

			if (m_pDisk)
			{
				// The first version of DSSend did not restrict system name lengths. So just in case there are any old DISK files created with names
				// longer than 64, we'll restrict it here.
				g.drawText(m_pDisk->GetSystemName(rowNumber).substr(0,64),
					5,0,width,height,
					Justification::centredLeft,true);
			}
		}
	}

	virtual void selectedRowsChanged(int lastRowSelected)
	{
		if (m_rowSelectedCallback)
		{
			m_rowSelectedCallback(lastRowSelected);
		}
	}
	void setSelectionChangedCallback(std::function<void(int lastRowSelected)> callback)
	{
		m_rowSelectedCallback = callback;
	}
private:
	Disk* m_pDisk;
	std::function<void(int lastRowSelected)> m_rowSelectedCallback;
};



//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/

class MainMenuBarModel : public MenuBarModel
{
public:
	MainMenuBarModel(ApplicationCommandManager* pAppCommandManager)
		: m_pAppCommandManager(pAppCommandManager)
	{	assert(pAppCommandManager);	}

	StringArray getMenuBarNames() override;
	PopupMenu getMenuForIndex(int menuIndex, const String& menuName) override;
	void menuItemSelected(int menuItemID, int topLevelMenuIndex) override;
private:
	ApplicationCommandManager* m_pAppCommandManager;
	//PropertiesFile* m_pPropertiesFile;
};



class SendTask : public ThreadWithProgressWindow
{
public:
	SendTask(System& systemToSend,PropertiesFile* pPropertiesFile, std::function<void(bool)> threadDoneFunction)
		:ThreadWithProgressWindow("Sending System",true,true)
		,m_systemToSend(systemToSend)
		,m_threadDoneFunction(threadDoneFunction)
		,m_completedTransfer(false)
		,m_pPropertiesFile(pPropertiesFile)
	{
	}

	bool WasCanceled() const noexcept
	{
		return !m_completedTransfer;
	}
	void run() override;

	//==============================================================================
	/** This method is called (on the message thread) when the operation has finished.
	You may choose to use this callback to delete the ThreadWithProgressWindow object.
	*/
	void threadComplete(bool userPressedCancel) override
	{
		m_threadDoneFunction(userPressedCancel);
	}

	std::string GetExceptionMsg()
	{
		return m_exceptionMsg;
	}
private:
	bool				m_completedTransfer;
	System				m_systemToSend;
	PropertiesFile*		m_pPropertiesFile;
	std::string			m_exceptionMsg;
	std::function<void(bool)> m_threadDoneFunction;
};


class ReceiveTask : public ThreadWithProgressWindow
{
public:
	ReceiveTask(PropertiesFile* pPropertiesFile, std::function<void(bool userPressedCancel)> threadDoneFunction)
		:ThreadWithProgressWindow("Receiving System",true,true)
		,m_pPropertiesFile(pPropertiesFile)
		,m_threadDoneFunction(threadDoneFunction)
		,m_completedTransfer(false)
	{
	}

	std::exception_ptr anException;
	bool WasCanceled() const noexcept
	{
		return !m_completedTransfer;
	}
	void run() override;

	//==============================================================================
	/** This method is called (on the message thread) when the operation has finished.
	You may choose to use this callback to delete the ThreadWithProgressWindow object.
	*/
	void threadComplete(bool userPressedCancel) override
	{
		m_threadDoneFunction(userPressedCancel);
	}

	std::string GetExceptionMsg()
	{
		return m_exceptionMsg;
	}
	System& GetSystemReceived()
	{
		return m_system;
	}
private:
	System				m_system;
	PropertiesFile*		m_pPropertiesFile;
	std::string			m_exceptionMsg;
	bool				m_completedTransfer;
	std::function<void(bool userPressedCancel)> m_threadDoneFunction;
};

class MainContentComponent   :	public Component,
								public ApplicationCommandTarget,
								private Button::Listener,
								private Label::Listener
{
public:

	enum CommandIDs
	{
		MIDISetup = 0x2000,
		ExportWave = 0x2001,
		ExportSoundfont = 0x2002,
		HelpAbout = 0x20032
	};

    //==============================================================================
	MainContentComponent(ApplicationCommandManager* pAppCommandManager);
    ~MainContentComponent();

    void paint (Graphics&);
    void resized();

	// The following methods support the ApplicationCommandTarget interface
	ApplicationCommandTarget* getNextCommandTarget() override;
	void getAllCommands(Array<CommandID>& commands) override;
	void getCommandInfo(CommandID commandID, ApplicationCommandInfo& result) override;
	bool perform(const InvocationInfo& info) override;

	bool IsModified()
	{
		return m_currentDisk ? m_currentDisk->IsModified() : false;
	}

	/** Called when the Disk name text has changed. */
	virtual void labelTextChanged(Label* labelThatHasChanged );

	/** Called when a Label goes into editing mode and displays a TextEditor. */
	virtual void editorShown(Label*, TextEditor&);

private:
	void DoMidiSetup();
	void DoExportWave();
	void DoExportSoundfont();
	void DoHelpAbout();
	void buttonClicked(Button* button) override;
	void UpdateButtonStates();
	void SetupForDiskName(const std::string& diskName);

	void ButtonClickedNew();
	void ButtonClickedOpen();
	void ButtonClickedSave();
	void ButtonClickedSaveAs();
	void ButtonClickedSend();
	void ButtonClickedReceive();
	void ButtonClickedRename();
	void ButtonClickedRemove();
	void ButtonClickedUp();
	void ButtonClickedDown();

	void OnSendDone(bool userPressedCancel);
	void OnReceiveDone(bool userPressedCancel);


private:
	std::unique_ptr<MenuBarComponent> m_pMainMenuComponent;
	std::unique_ptr<MenuBarModel> m_pMainMenuModel;
	LookAndFeel_V2 lookAndFeel;

	Label		m_diskNameLabel;
	Label		m_diskNameEdit;

	TextButton	m_testButton;

	TextButton	m_newButton;
	TextButton	m_openButton;
	TextButton	m_saveButton;
	TextButton	m_saveAsButton;
	TextButton	m_sendButton;
	TextButton	m_receiveButton;
	TextButton	m_renameButton;
	TextButton	m_removeButton;

	ArrowButton	m_upButton;
	ArrowButton	m_downButton;

	ListBox				m_systemListbox;
	SystemListboxModel	m_systemListboxModel;

	std::unique_ptr<PropertiesFile>	m_pPropertiesFile;
	PropertySet						m_defaultProperties;
	PropertiesFile::Options			m_propertiesOptions;

	DiskUPtr						m_currentDisk;
	std::string						m_currentFilePath;

	std::unique_ptr<SendTask>		m_pSendTask;
	std::unique_ptr<ReceiveTask>	m_pReceiveTask;


    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainContentComponent)
};


#endif  // MAINCOMPONENT_H_INCLUDED
