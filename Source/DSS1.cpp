/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <exception>
#include <vector>
#include "DSS1.h"
#include "MidiDevice.h"
#include "tsqueue.h"

using std::vector;
using std::string;



DSS1::DSS1(MidiIoDevice* pDevice, uint8_t midiChannel)
	: m_pDevice(pDevice)
	, m_midiChannel(midiChannel & 0x0F)
{
	assert(m_pDevice);
	assert(m_pDevice->IsOpen());
	assert(midiChannel < 16);
}

int DSS1::GetMode()
{

	if (!IsConnected())
	{
		throw std::runtime_error("Not connected.");
	}

	uint8_t modeReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x12,	// Function:  Mode Request
		0xF7	// End of Exclusive	
	};

	TransferRequest req;
	req.SendBuffer = std::vector<uint8_t>(std::begin(modeReq), std::end(modeReq));
	req.ReceiveBuffer.resize(20);
	m_pDevice->Read(req);

	return req.ReceiveBuffer[5];

}

void DSS1::SetPlayMode()
{
	assert(m_pDevice->IsOpen());

	uint8_t playModeReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x13,	// Function:  Play Mode Request
		0xF7	// End of Exclusive	
	};

	TransferRequest req;
	req.SendBuffer = std::vector<uint8_t>(std::begin(playModeReq), std::end(playModeReq));
	m_pDevice->Write(req);

}

/** Determines if the DSS-1 is connected (can be communicatied with) or not.

	This function assumes that the MIDI device is already open and it thows an exception if it is not.\n
	\n
	This function doesn't require any particular MIDI channel. So even if it succeeds, subsequent 
	transfers may fail if the MIDI channel is not correct.

	@returns	true if the DSS-1 is connected and has responded with it's device ID. false otherwise.
	@throws		std::runtime_error if the midi device is not open.
	@throws		std::bad_alloc if out of memory.
*/
bool DSS1::IsConnected()
{
	if (!m_pDevice->IsOpen())
	{
		throw std::runtime_error("The MIDI input or output device is not open.");
	}

	uint8_t dssReqID[] = {
		0xF0,   // Exclusive status
		0x42,   // Korg ID
		0x40,   // Format ID / channel (0)
		0xF7    // End of Exclusive	
	
	};

	TransferRequest req;
	req.SendBuffer = std::vector<uint8_t>(std::begin(dssReqID), std::end(dssReqID));
	req.ReceiveBuffer.resize(20);
	bool result = false;

	// If we're not connected we'll get a Read exception here. But since the purpose of this function is to 
	// report if we're connected, we don't want propagate the exception.  So we catch it here and return false.
	try
	{
		m_pDevice->Read(req);
		if ((req.NumberOfBytesRead == 5) &&
			(req.ReceiveBuffer[1] == 0x42) &&	// Korg ID
			(req.ReceiveBuffer[3] == 0x0B))		// DSS-1 ID
		{
			result = true;
		}
	}
	catch (std::runtime_error& )
	{
		result = false;
	}

	return result;
}

vector<string> DSS1::GetProgramNameList()
{
	assert(m_pDevice->IsOpen());

	vector<string> progNameList;

	uint8_t progNameReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x17,	// Function:  Program Name Request
		0xF7	// End of Exclusive	
	};
	progNameReq[2] |= m_midiChannel && 0x0F;

	TransferRequest req;
	req.SendBuffer.resize(sizeof(progNameReq));
	memcpy(&req.SendBuffer[0], &progNameReq[0], sizeof(progNameReq));

	req.ReceiveBuffer.resize(262);

	m_pDevice->Read(req);

	if (!req.Success || req.NumberOfBytesRead != 262)
	{
		throw std::runtime_error("Receive error.");
	}

	char* pName = (char*)&req.ReceiveBuffer[5];
	for (int i = 0; i < 32; i++)
	{
		progNameList.push_back(string(pName, 8));
		pName += 8;
	}
	return progNameList;
}


vector<uint8_t> DSS1::GetProgramParamData(int programIndex)
{
	assert(programIndex < 32);
	assert(m_pDevice->IsOpen());

	uint8_t paramReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x10,	// Function:  Program Parameter Dump Request
		0x00,	// Program index
		0xF7	// End of Exclusive	
	};
	paramReq[5] = static_cast<uint8_t>(programIndex);
	paramReq[2] |= m_midiChannel;

	TransferRequest req;
	req.SendBuffer = std::vector<uint8_t>(std::begin(paramReq), std::end(paramReq));

	// A full dump is 86 bytes.
	req.ReceiveBuffer.resize(86);	// Note that the size is only 86 because the name is not sent by the DSS-1. That is obtained by the program list request.
	req.ReceiveLength = 86;
	req.ReceiveStartIndex = 0;
	req.userData = programIndex;

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead != 86))
	{
		throw std::runtime_error("Program read failed.");
	}

	vector<uint8_t> v(80);
	memcpy(&v[0], &req.ReceiveBuffer[5], 80);
	return v;
}

void DSS1::SendProgramParamData(uint8_t programIndex, const string& name, const vector<uint8_t>& paramData)
{
	assert(programIndex < 32);
	assert(paramData.size() == 80);
	assert(name.length() >= 8);
	assert(m_pDevice->IsOpen());

	TransferRequest req;
	req.SendBuffer.resize(94);		// 5 bytes header, 80 bytes params, 8 bytes name, 1 byte EOX
	req.SendBuffer[0] = 0xF0;					// Exclusive status
	req.SendBuffer[1] = 0x42;					// Korg ID
	req.SendBuffer[2] = 0x30 | m_midiChannel;	// Format ID / channel
	req.SendBuffer[3] = 0x0B;					// DSS-1 ID
	req.SendBuffer[4] = 0x40;					// Function:  Program Parameter Dump
	memcpy(&req.SendBuffer[5], &paramData[0], 80);
	memcpy(&req.SendBuffer[85], name.c_str(), 8);
	req.SendBuffer[93] = 0xF7;					// End of Exclusive	

	req.ReceiveBuffer.resize(6);	// A response is 6 bytes no matter whether it succeeds or fails.
	req.ReceiveLength = 6;
	req.ReceiveStartIndex = 0;

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead != 6) || req.ReceiveBuffer[4] != 0x23)	// 0x23 means the DSS-1 reported that the dump completed successfully.
	{
		throw std::runtime_error("Program read failed.");
	}

	// The dump we just did only transferred the program parameters to the DSS-1 edit buffer.
	// Now write the program to a program slot.
	req.SendBuffer.resize(7);
	req.SendBuffer[0] = 0xF0;					// Exclusive status
	req.SendBuffer[1] = 0x42;					// Korg ID
	req.SendBuffer[2] = 0x30 | m_midiChannel;	// Format ID / channel
	req.SendBuffer[3] = 0x0B;					// DSS-1 ID
	req.SendBuffer[4] = 0x11;					// Function:  Write Request
	req.SendBuffer[5] = programIndex;			// Program number - 1
	req.SendBuffer[6] = 0xF7;					// End of Exclusive	

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead != 6) || req.ReceiveBuffer[4] != 0x21)	// 0x21 means the DSS-1 reported that the write completed successfully.
	{
		throw std::runtime_error("Program read failed.");
	}

}

vector<uint8_t> DSS1::GetMultisoundNameList()
{
	assert(m_pDevice->IsOpen());

	uint8_t multiListReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x16,	// Function:  Multisound List Request
		0xF7	// End of Exclusive	
	};
	multiListReq[2] |= m_midiChannel;

	TransferRequest req;
	req.SendBuffer.resize(sizeof(multiListReq));
	memcpy(&req.SendBuffer[0], &multiListReq[0], sizeof(multiListReq));

	// The amount data can be as much as 232 depending on how many multisounds there are.
	req.ReceiveBuffer.resize(232);

	m_pDevice->Read(req);

	if (!req.Success || req.NumberOfBytesRead < 8)
	{
		throw std::runtime_error("Receive error.");
	}

	int numMs = req.ReceiveBuffer[5];
	assert((numMs * 14) + 8 == req.NumberOfBytesRead);
	assert(req.ReceiveBuffer[4] == 0x45);
	int sum = 0;
	int i;
	for (i = 5; i < 6 + (numMs * 14); ++i)	// calculate checksum
	{
		sum += req.ReceiveBuffer[i];
	}
	assert((sum & 0x7F) == req.ReceiveBuffer[i]);	// confirm checksum

	vector<uint8_t> v(1 + numMs * 14);
	memcpy(&v[0], &req.ReceiveBuffer[5], v.size());
	return v;
}

void DSS1::SendMultisoundNameList(uint8_t numberOfMultisounds, std::vector<uint8_t> nameList)
{
	assert(m_pDevice->IsOpen());
	assert(nameList.size() >= size_t(numberOfMultisounds * 14 + 1));	// We include the number of multisounds as part of the data
	assert(numberOfMultisounds == nameList[0]);					// We include the number of multisounds as part of the data
	assert(numberOfMultisounds > 0);
	
	TransferRequest req;
	req.SendBuffer.resize(numberOfMultisounds * 14 + 8);		// 5 bytes header, 1 + (X * 14) bytes MS data, 1 byte checksum, 1 byte EOX
	req.SendBuffer[0] = 0xF0;					// Exclusive status
	req.SendBuffer[1] = 0x42;					// Korg ID
	req.SendBuffer[2] = 0x30 | m_midiChannel;	// Format ID / channel
	req.SendBuffer[3] = 0x0B;					// DSS-1 ID
	req.SendBuffer[4] = 0x45;					// Function:  Multisound List Header
	memcpy(&req.SendBuffer[5], &nameList[0], numberOfMultisounds * 14 + 1);
	req.SendBuffer[req.SendBuffer.size() - 1] = 0xF7;					// End of Exclusive	

	// Calculate checksum
	int sum = 0;
	for (size_t i = 5; i < req.SendBuffer.size() - 2; ++i)	// calculate checksum
	{
		sum += req.SendBuffer[i];
	}
	req.SendBuffer[req.SendBuffer.size() - 2] = (sum & 0x7F);

	req.ReceiveBuffer.resize(6);	// A response is 6 bytes no matter whether it succeeds or fails.
	req.ReceiveLength = 6;
	req.ReceiveStartIndex = 0;

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead != 6) || req.ReceiveBuffer[4] != 0x23)	// 0x23 means the DSS-1 reported that the dump completed successfully.
	{
		throw std::runtime_error("Program read failed.");
	}
}

#if 0
vector<MultisoundNameListItem> DSS1::GetMultisoundNameList()
{
	assert(m_pDevice->IsOpen());

	vector<MultisoundNameListItem> nameList;

	vector<string> progNameList;

	uint8_t multiListReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x16,	// Function:  Multisound List Request
		0xF7	// End of Exclusive	
	};
	multiListReq[2] |= m_midiChannel;

	TransferRequest req;
	req.SendBuffer.resize(sizeof(multiListReq));
	memcpy(&req.SendBuffer[0], &multiListReq[0], sizeof(multiListReq));

	// The amount data can be as much as 232 depending on how many multisounds there are.
	req.ReceiveBuffer.resize(232);

	m_pDevice->Read(req);

	if (!req.Success || req.NumberOfBytesRead < 8)
	{
		throw std::runtime_error("Receive error.");
	}

	int numMs = req.ReceiveBuffer[5];
	assert((numMs * 14) + 8 == req.NumberOfBytesRead);
	assert(req.ReceiveBuffer[4] == 0x45);
	int sum = 0;
	int i;
	for (i = 5; i < 6 + (numMs * 14); ++i)	// calculate checksum
	{
		sum += req.ReceiveBuffer[i];
	}
	assert((sum & 0x7F) == req.ReceiveBuffer[i]);	// confirm checksum

	uint8_t* pName = &req.ReceiveBuffer[6];
	for (int i = 0; i < numMs; ++i)
	{
		nameList.push_back(MultisoundNameListItem((char*)pName, ToLength(pName + 8)));
		pName += 14;
	}

	return nameList;
}

#endif // 0

std::vector<uint8_t> DSS1::GetMultisoundParamData(int multisoundIndex)
{
	assert(multisoundIndex < 16);
	assert(m_pDevice->IsOpen());

	uint8_t paramReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x15,	// Function:  Multisound Dump Request
		0x00,	// Multisound number
		0xF7	// End of Exclusive	
	};
	paramReq[2] |= m_midiChannel && 0x0F;
	paramReq[5] = static_cast<uint8_t>(multisoundIndex);

	TransferRequest req;
	req.SendBuffer.resize(sizeof(paramReq));
	memcpy(&req.SendBuffer[0], &paramReq[0], sizeof(paramReq));

	// The amount data can be as much as 600 depending on how many sounds there are.
	req.ReceiveBuffer.resize(600);

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead < 24)) // at least 24 even if there're no sounds
	{
		throw std::runtime_error("Multisound read failed.");
	}

	assert(req.ReceiveBuffer[4] == 0x44);

	int numSounds = req.ReceiveBuffer[20] & 0x3F;
	assert((numSounds * 36) + 24 == req.NumberOfBytesRead);

	int sum = 0;
	int i;
	for (i = 5; i < 22 + (numSounds * 36); ++i)	// calculate checksum
	{
		sum += req.ReceiveBuffer[i];
	}
	assert((sum & 0x7F) == req.ReceiveBuffer[i]);	// confirm checksum

	int paramSize = (numSounds * 36) + 17;
	vector<uint8_t> v(paramSize);
	memcpy(&v[0], &req.ReceiveBuffer[5], paramSize);

	// For debugging
	// int msLength = GetSize(&req.ReceiveBuffer[14]);
	// string name = string((char*)&req.ReceiveBuffer[6], 8);

	return v;
}

void DSS1::SendMultisoundParamData(const vector<uint8_t>& paramData)
{
	assert(m_pDevice->IsOpen());

	TransferRequest req;
	req.SendBuffer.resize(paramData.size() + 7);		// 5 bytes header, X bytes param data, 1 byte checksum, 1 byte EOX
	req.SendBuffer[0] = 0xF0;					// Exclusive status
	req.SendBuffer[1] = 0x42;					// Korg ID
	req.SendBuffer[2] = 0x30 | m_midiChannel;	// Format ID / channel
	req.SendBuffer[3] = 0x0B;					// DSS-1 ID
	req.SendBuffer[4] = 0x44;					// Function:  Multisound Parameter Dump
	memcpy(&req.SendBuffer[5], &paramData[0], paramData.size());
	req.SendBuffer[req.SendBuffer.size() - 1] = 0xF7;					// End of Exclusive	


	// Calculate checksum
	int sum = 0;
	for (size_t i = 5; i < req.SendBuffer.size() - 2; ++i)	// calculate checksum
	{
		sum += req.SendBuffer[i];
	}
	req.SendBuffer[req.SendBuffer.size() - 2] = (sum & 0x7F);

	req.ReceiveBuffer.resize(6);	// A response is 6 bytes no matter whether it succeeds or fails.
	req.ReceiveLength = 6;
	req.ReceiveStartIndex = 0;

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead != 6) || req.ReceiveBuffer[4] != 0x23)	// 0x23 means the DSS-1 reported that the dump completed successfully.
	{
		throw std::runtime_error("Program read failed.");
	}

}


void DSS1::GetPCMData(uint32_t startAddress, uint32_t lengthInSamples, std::vector<uint8_t>& outBuffer, uint32_t startIndex)
{
	assert(m_pDevice->IsOpen());
	//assert(startAddress < 261886);
	// assert(startAddress + lengthInSamples < 261886);
	assert((lengthInSamples <= 261886) && (lengthInSamples > 0));
	assert((uint32_t)outBuffer.size() - startIndex >= lengthInSamples * 2);

#if defined(_DEBUG)
	if (startAddress < 261886 || (startAddress + lengthInSamples < 261886))
	{
		std::ostringstream os;
		os << "Warning: Attempting to read beyond the documented end of memory.\n";
		os << "         StartAddress: " << startAddress << "\n";
		os << "         Length:       " << lengthInSamples << "\n\n";
	}
#endif

	uint8_t pcmDataReq[] = {
		0xF0,	// Exclusive status
		0x42,	// Korg ID
		0x30,	// Format ID / channel (0)
		0x0B,	// DSS-1 ID
		0x14,	// Function:  PCM Data Dump Request
		0, 0, 0, 0, 0, 0,	// StartAddress to get
		0, 0, 0, 0, 0, 0,	// Length in samples (words) to get
		0xF7	// End of Exclusive	
	};
	pcmDataReq[2] |= m_midiChannel && 0x0F;
	FromAddress(&pcmDataReq[5], startAddress);
	FromAddress(&pcmDataReq[11], startAddress + lengthInSamples);

	TransferRequest req;
	req.SendBuffer.resize(sizeof(pcmDataReq));
	memcpy(&req.SendBuffer[0], &pcmDataReq[0], sizeof(pcmDataReq));
	req.ReceiveBuffer.resize(lengthInSamples * 2 + 19);		// 5 bytes header, 12 bytes of addresses, (lengthInSamples * 2) bytes data, 1 byte checksum, 1 byte EOX

	m_pDevice->Read(req);

	uint32_t startAddressReturned = ToAddress(&req.ReceiveBuffer[5]);
	uint32_t lastAddressReturned = ToAddress(&req.ReceiveBuffer[11]);
	startAddressReturned; lastAddressReturned;	// avoid warnings
	assert(startAddressReturned == startAddress);
	assert(lastAddressReturned == startAddress + lengthInSamples);


	// check the checksum
	int calculatedChecksum = 0;
	uint8_t highBitCheck = 0;
	for (size_t i = 5; i < req.ReceiveBuffer.size() - 2; i++)
	{
		uint8_t c = req.ReceiveBuffer[i];
		calculatedChecksum += c;
		highBitCheck |= c;
	}
	calculatedChecksum &= 0x7F;
	int receivedChecksum = req.ReceiveBuffer[req.ReceiveBuffer.size() - 2];
	receivedChecksum;	// avoid warning

	// confirm checksum
	if (calculatedChecksum != receivedChecksum)
	{
		throw std::runtime_error("Received data checksum failed.");
	}
	if (highBitCheck & 0x80)
	{
		throw std::runtime_error("Invalid data received.");
	}

	memcpy(&outBuffer[startIndex], &req.ReceiveBuffer[17], lengthInSamples * 2);
}

void DSS1::SendPCMData(uint32_t startAddress, uint32_t lengthInSamples, vector<uint8_t>& inBuffer, uint32_t startIndex)
{
	assert(startAddress < 256 * 1024);	// Not sure exactly how much of the 512K bytes are usable (the documentation is wrong) but at least catch the crazy values.
	assert((startIndex + lengthInSamples * 2) <= inBuffer.size());
	assert(m_pDevice->IsOpen());

	TransferRequest req;
	req.SendBuffer.resize(lengthInSamples * 2 + 19);		// 5 bytes header, 12 bytes of addresses, (lengthInSamples * 2) bytes data, 1 byte checksum, 1 byte EOX
	req.SendBuffer[0] = 0xF0;						// Exclusive status
	req.SendBuffer[1] = 0x42;						// Korg ID
	req.SendBuffer[2] = 0x30 | m_midiChannel;		// Format ID / channel
	req.SendBuffer[3] = 0x0B;						// DSS-1 ID
	req.SendBuffer[4] = 0x43;						// Function:  PCM Data Dump
	req.SendBuffer[req.SendBuffer.size()-1] = 0xF7;	// End of Exclusive
	FromAddress(&req.SendBuffer[5], startAddress);
	FromAddress(&req.SendBuffer[11], startAddress + lengthInSamples);
	
	memcpy(&req.SendBuffer[17], &inBuffer[startIndex], lengthInSamples * 2);

	// Calculate checksum
	int sum = 0;
	for (size_t i = 5; i < req.SendBuffer.size() - 2; ++i)	// calculate checksum
	{
		sum += req.SendBuffer[i];
	}
	req.SendBuffer[req.SendBuffer.size() - 2] = (sum & 0x7F);

	req.ReceiveBuffer.resize(6);	// A response is 6 bytes no matter whether it succeeds or fails.
	req.ReceiveLength = 6;
	req.ReceiveStartIndex = 0;

	m_pDevice->Read(req);

	if (!req.Success || (req.NumberOfBytesRead != 6) || req.ReceiveBuffer[4] != 0x23)	// 0x23 means the DSS-1 reported that the dump completed successfully.
	{
		throw std::runtime_error("PCM data send failed.");
	}
}

void DSS1::ProgramChange(uint8_t programNumber)
{
	assert(m_pDevice->IsOpen());
	assert(programNumber < 32);

	TransferRequest req;
	req.SendBuffer.resize(2);
	req.SendBuffer[0] = 0xC0 | m_midiChannel;
	req.SendBuffer[1] = programNumber & 0x7F;
	m_pDevice->Write(req);
}
