/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#if !defined(ZIPLIB_H)
#define ZIPLIB_H

#include <vector>

class ZipLib
{
public:
	ZipLib(void);

	void AddFile(std::string filename);
	bool CreateLibrary(std::string libraryName);

	bool SetCompressionLevel(int level);
	bool SetDirectory(std::string directory);
	void SetStoreWithPath(bool storeWithPath);

private:
	using FileListType = std::vector<std::string>;
	
	FileListType m_fileList;	///< This is the list of files that will be included in the library.
	int m_compressionLevel;		///< The compression level. Valid values are 0 to 9.
	bool m_storeWithPath;		///< if true, files included in the library are in their original folder. If false, files do not have a path.

	std::string m_directory;
};

#endif
