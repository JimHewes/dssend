/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>
#include <string>
#include <vector>
#include <algorithm>
#include "Disk.h"

using boost::filesystem::path;
using boost::filesystem::ifstream;
using boost::filesystem::ofstream;
using std::string;
using std::vector;

#if defined(_DEBUG)
#define VALIDATE Validate
#include "../JuceLibraryCode/JuceHeader.h"	// include just for DBG()
#else
#define VALIDATE()
#endif

string Disk::GetConflictingMultisoundName(System& system)
{
	string result;
	bool done = false;

	int numberOfMultisounds = system.GetNumberOfMultisounds();

	for (uint8_t i = 0; i < numberOfMultisounds && !done; ++i)
	{
		Multisound* pMultisound = system.GetMultisoundPtr(i);
		assert(pMultisound);
		if (pMultisound)
		{
			string sysMultisoundName = pMultisound->GetName();
			for (size_t j = 0; j < m_multisoundList.size() && !done; ++j)
			{
				if (sysMultisoundName == m_multisoundList[j]->GetName())
				{
					if (!m_multisoundList[j]->Equals(pMultisound))
					{
						result = sysMultisoundName;
						done = true;
					}
				}
			}
		}
	}

	return result;
}


void Disk::AddSystem(System& system)
{
	string conflictingName = GetConflictingMultisoundName(system);// Gets the first multisound name in the system that conflicts with a multisound already in the disk.
	if (!conflictingName.empty())
	{
		std::ostringstream os;
		os << "Name conflict when adding System to Disk. A different multisound named " << conflictingName << " is already in the Disk.";
		throw std::runtime_error(os.str().c_str());
	}

	m_systemParamList.push_back(system.GetSystemParams());

	int numberOfMultisounds = system.GetNumberOfMultisounds();
	for (uint8_t i = 0; i < numberOfMultisounds; ++i)
	{
		Multisound* pMultisound = system.GetMultisoundPtr(i);	// Don't copy the Multisound until we determine we don't already have the same one in the disk.

		// Since we called GetConflictingMultisoundName() above, we know that any Multisound with the same name also has the same data, so we don't need to copy it into the disk.
		auto found = std::find_if(m_multisoundList.begin(), m_multisoundList.end(), [pMultisound](MultisoundUPtr& i){ return pMultisound->GetName() == i->GetName(); });
		if (found == m_multisoundList.end())
		{
			m_multisoundList.push_back(system.GetMultisoundCopy(i));
		}
	}
	m_isModified = true;

}

void Disk::RemoveSystem(std::string systemNameToRemove)
{
	VALIDATE();

	auto sysParamsIter = std::find_if(m_systemParamList.begin(), m_systemParamList.end(), [&systemNameToRemove](const SystemParams& params) { return systemNameToRemove == params.GetSystemName(); });

	if (sysParamsIter == m_systemParamList.end())	// not found
	{
		assert(false);
		return;
	}

	// This uses a parallel array to mark multisounds for removal. Then they actually get removed below.
	// (I could have also removed multisounds one at a time as it was discovered they weren't used. I just decided to do it this way 
	// to see what all was going to get removed before I removed it. It might perform slightly better as well since remove_if is called aminimla number of times.)
	auto systemMultisoundNames = sysParamsIter->GetUsedMultisoundNames();
	std::vector<bool> okToRemoveMultisound(systemMultisoundNames.size(), true);	// parallel array that tells if a multisound can be removed. Starts out as all can be removed.
	for (uint8_t i = 0; i < systemMultisoundNames.size(); ++i)
	{
		string multisoundNameToRemove = sysParamsIter->GetMultisoundNameListItem(i).name;
		for (const auto &sp : m_systemParamList)
		{
			if (sp.GetSystemName() != systemNameToRemove)	// avoid comparing with the System we want to remove
			{
				if (sp.UsesMultisound(multisoundNameToRemove))
				{
					okToRemoveMultisound[i] = false;
					break;
				}
			}
		}
	}

	auto newEnd = m_multisoundList.end();
	// now all multisound names have been marked for removal. Remove them.
	for (uint8_t i = 0; i < systemMultisoundNames.size(); ++i)
	{
		if (okToRemoveMultisound[i])
		{
			newEnd = std::remove_if(m_multisoundList.begin(), newEnd, [&](const MultisoundUPtr& pMultisound)
			{
				return (pMultisound->GetName() == systemMultisoundNames[i]);
			});
		}
	}
	// In a vector, it's not enough to remove elements, you need to erase also.
	m_multisoundList.erase(newEnd, m_multisoundList.end());
	m_systemParamList.erase(sysParamsIter);
	m_isModified = true;

	VALIDATE();
}


bool Disk::HasSystem(const std::string& name) const
{
	auto sysParamsFound = std::find_if(m_systemParamList.begin(), m_systemParamList.end(), [name](const SystemParams& sp){ return sp.GetSystemName() == name; });
	return sysParamsFound != m_systemParamList.end();
}

void Disk::SetName(const std::string& name)
{
	if (m_name != name)
	{
		m_name = name;
		m_isModified = true;
	}
}

System Disk::GetSystem(std::string name)
{
	VALIDATE();

	// Find SystemParams with the given name
	auto systemParamsIter = std::find_if(m_systemParamList.begin(), m_systemParamList.end(), [name](SystemParams& sp){ return sp.GetSystemName() == name; });
	if (systemParamsIter == m_systemParamList.end())
	{
		std::ostringstream os;
		os << "The System named " << name << " does not exist on the Disk named " << m_name << ".";
		throw std::runtime_error(os.str().c_str());
	}

	MultisoundList newMultisoundList;

	// loop through Multisound name list to find multisounds we need.
	uint8_t numMultisounds = systemParamsIter->GetNumberOfMultisoundNames();
	for (uint8_t i = 0; i < numMultisounds; ++i)
	{
		string msName = systemParamsIter->GetMultisoundNameListItem(i).name;

		// Find the multisound on the Disk. We need and make a copy of it.
		auto multisoundIter = std::find_if(m_multisoundList.begin(), m_multisoundList.end(), [msName](MultisoundUPtr& i){ return msName == i->GetName(); });
		assert(multisoundIter != m_multisoundList.end());
		if (multisoundIter != m_multisoundList.end())
		{
			newMultisoundList.push_back(std::make_unique<Multisound>(  *(multisoundIter->get())  ));
			
			// Here we need to update the multisound number in the multisound parameters. That's the only byte in a Multisound that 
			// can change depending on which System the Multisound is in.
			// (When sending programs and Multisounds to the DSS-1, we don't send a Multisound List, so the Multisounds need to be numbered correctly 
			// so the programs can find them.)
			newMultisoundList[newMultisoundList.size() - 1]->SetIndex(i);
		}
	}
	
	VALIDATE();

	return System(*systemParamsIter, std::move(newMultisoundList));
}


BSONDocument Disk::ToBSON()
{
	BSONDocument doc;
	BSONDocument sysParamsDoc;
	BSONDocument multisoundsDoc;

	for (auto& s : m_systemParamList)
	{
		sysParamsDoc.AddElementDocument(s.GetSystemName(), s.ToBSON());
	}
	for (auto& s : m_multisoundList)
	{
		multisoundsDoc.AddElementDocument(s->GetName(), s->ToBSON());
	}

	doc.AddElementString("Type", "Disk");
	doc.AddElementString("Version", "0.1");
	doc.AddElementString("Name", m_name);
	doc.AddElementDocument("Systems", sysParamsDoc);
	doc.AddElementDocument("Multisounds", multisoundsDoc);

	return doc;
}


void Disk::SaveToFile(const string& filepath)
{
	BSONDocument doc = ToBSON();	// Get this Disk as a BSON tree representation.

	std::ostringstream outstring;
	doc.ToBinary(outstring);		// Convert BSON representation to raw bytes contained in the stream.
	
	ofstream outFile;
	outFile.open(filepath, std::ios::out | std::ios::binary);
	outFile.write(outstring.str().c_str(), outstring.str().length());
	outFile.close();
	m_isModified = false;
}


void Disk::LoadFromFile(const string& filepath)
{
	// Determine the file type, either .dssdsk or .DS1

	ifstream inFile;
	inFile.open(filepath,ifstream::binary);
	if (!inFile)
	{
		throw std::runtime_error("Unable to open the file.");
	}

	inFile.seekg(0,inFile.end);
	uint32_t realFileSize = (uint32_t)inFile.tellg();	// tellg() actually returns a file position, but as long as we open in binary mode it should be equal to the file offset.
	inFile.seekg(0,inFile.beg);

	if (realFileSize > 0)
	{
		vector<uint8_t> buf;
		buf.resize(realFileSize);
		inFile.read((char*)&buf[0],realFileSize);
		inFile.close();

		// Try to determine the file type from the first few byte of the contents.
		int index = 0;
		uint32_t documentSize = *((uint32_t *)&buf[index]);
		index += 4;
		uint8_t code = buf[index++];
		string name;
		int c = buf[index++];
		while (isalpha(c) && index < 30)
		{
			name += (char)c;
			c = buf[index++];
		}

		if (realFileSize == 11979 && 
			!(realFileSize == documentSize && (code == 2) && name == "Type" && name == "Version"))
		{
			// file is a .DS1/DSM type (native DSS-1)
			path p(filepath);
			string parentPath = p.parent_path().string();
			parentPath += '\\';
			FromNative(buf, parentPath);
			m_isModified = false;
		}
		else
		{
			// file is a .dssdsk type

			BSONDocument doc;
			doc.Parse(buf);

			FromBSON(doc);
			m_isModified = false;
		}
	}
	else
	{
		throw std::runtime_error("File size is 0");
	}
}

#if 0
void Disk::LoadFromFile(const string& filepath)
{
	BSONDocument doc;

	ifstream inFile;
	inFile.open(filepath, ifstream::binary);
	if (!inFile)
	{
		throw std::runtime_error("Unable to open the file.");
	}
	inFile.seekg(0, inFile.end);
	uint32_t fileSize = (uint32_t)inFile.tellg();	// tellg() actually returns a file position, but as long as we open in binary mode it should be equal to the file offset.
	inFile.seekg(0, inFile.beg);

	if (fileSize > 0)
	{
		vector<uint8_t> buf;
		buf.resize(fileSize);
		inFile.read((char*)&buf[0], fileSize);
		inFile.close();

		doc.Parse(buf);

		FromBSON(doc);
		m_isModified = false;
	}
	else
	{
		throw std::runtime_error("File size is 0");
	}
}
#endif

/** Populates this Disk from a BSON document.

	Note that a Disk does not have to contain any SystemParams or Multisounds. This allows a Disk to be in a 
	state of being constructed out of parts. (It remains to be seen with further development whether this 
	makes sense or not.)

	@param[in] doc		The BSON document from which to populate this Disk.
	@throws std::runtime_error	If the BSON document is not a Disk type. 
								(i.e. It doesn't have a string element in the root with name "Type" and value "Disk".
*/
void Disk::FromBSON(const BSONDocument& doc)
{

	BSONElementString* pTypeElement = dynamic_cast<BSONElementString*>(doc.GetElement("Type"));
	if (!pTypeElement || pTypeElement->GetValue() != "Disk")
	{
		throw std::runtime_error("Not a valid Disk type.");	// It is not a disk type.
	}

	BSONElementString* pNameElement = dynamic_cast<BSONElementString*>(doc.GetElement("Name"));
	// We won't penalize if there is no name element found, just set it to a default value.
	m_name = pNameElement ? pNameElement->GetValue() : "No Name Found";

	// It's OK, if there is no Systems element or if there is one and it is empty.
	// In the future a Disk may just be a container for Multisounds.
	BSONElementDocument* pSystemParamsDoc = dynamic_cast<BSONElementDocument*>(doc.GetElement("Systems"));
	if (pSystemParamsDoc)
	{
		// The SystemParams are identified by their name, but the name is included in the SystemParams data so we don't need to know it. 
		// (Should be changed then? It might be useful if you are looking for a particular System. But normally you're building a whole Disk.)
		// Since names are arbitrary we need to loop through using GetNextSibling.
		BSONElementDocument* pSystemParamsElem = dynamic_cast<BSONElementDocument*>(pSystemParamsDoc->GetDoc().GetFirstElement());

		while (pSystemParamsElem)
		{
			m_systemParamList.push_back(SystemParams(pSystemParamsElem->GetDoc()));
			pSystemParamsElem = dynamic_cast<BSONElementDocument*>(pSystemParamsElem->GetNextSibling());
		}
	}


	BSONElementDocument* pMultisoundsDoc = dynamic_cast<BSONElementDocument*>(doc.GetElement("Multisounds"));
	if (pMultisoundsDoc)
	{
		// The SystemParams are identified by their name, but the name is included in the SystemParams data so we don't need to know it. (Should be changed then?)
		// Since names are arbitrary we need to loop through using GetNextSibling.
		BSONElementDocument* pMultisoundElem = dynamic_cast<BSONElementDocument*>(pMultisoundsDoc->GetDoc().GetFirstElement());

		while (pMultisoundElem)
		{
			m_multisoundList.push_back(std::make_unique<Multisound>(pMultisoundElem->GetDoc()));
			pMultisoundElem = dynamic_cast<BSONElementDocument*>(pMultisoundElem->GetNextSibling());
		}
	}

	m_isModified = false;
}


/** @brief Populates this Disk from native DS1/DSM files.

	@param[in] buf	A reference to a buffer containing the contents of the .DS1 file.
					The size of the buffer must be exactly 11,979 bytes because all
					.DS1 files are 11,979 bytes.
	@param[in] folderpath	The absolute path to the folder that contains the DS1 and DSM files.
							This is used for finding the DSM files.
							It must include a path separator at the end.
	@param[in] name		The name to use for the disk. The DS1 file does not have a name for the 
						disk so you can optionally supply one. The default is the empty string.

	@internal The parameters are a bit awkward for this function. If the filepath is a parameter, it 
	would seem redundant to also have buf as a parameter since I could just read the file contents 
	using the filepath. But since it was already read in once I didn't want to read it again.
*/
void Disk::FromNative(vector<uint8_t>& buf, string folderpath, const std::string& systemName)
{
	assert(buf.size() == 11979);
	m_name = systemName;
	//vector<string> allMsNames;

	for (int systemIndex = 0; systemIndex <= 3; systemIndex++)
	{
		if (SystemParams::HasSystem(systemIndex, buf))
		{
			m_systemParamList.push_back(SystemParams(systemIndex,buf));

			// Collect multisound names from this system
			// auto msNames = m_systemParamList[m_systemParamList.size() - 1].GetUsedMultisoundNames();
			// allMsNames.insert(allMsNames.end(),msNames.begin(),msNames.end());
		}
	}

	// Get list of MS names for the whole disk. We could collect these from the m_systemParamList we just 
	// created. But then we'd have to sort to filter out duplicates, etc.
	int index = 1024;
	uint8_t numMultisounds = buf[index++];
	for (int i = 0; i < numMultisounds; ++i)
	{
		string multisoundName = string((char*)&buf[index],8);
		string filename = multisoundName;
		std::replace(filename.begin(), filename.end(), '?', '!');

		assert(filename.find('\x02F') == string::npos);
		assert(filename.find('\x03A') == string::npos);
		assert(filename.find('\x03C') == string::npos);
		assert(filename.find('\x03E') == string::npos);
		assert(filename.find('\x05C') == string::npos);
		assert(filename.find('\x07C') == string::npos);
		assert(filename.find('\x07C') == string::npos);
		//assert(filename.find('\x07E') == string::npos);	// Tested. Needs no translation
		//assert(filename.find('\x07F') == string::npos);	// Tested. Needs no translation

		filename.erase(std::remove(filename.begin(),filename.end(),' '), filename.end());
		auto filepath = folderpath + filename;
		filepath.append(".DMS");

		// try load the multisound
		m_multisoundList.push_back(std::make_unique<Multisound>(filepath, i, multisoundName ));

		// In native files, the multisound length is specified in two places. One is in the .DS1 
		// file in the master list. The other is the .DSM file right after the name. Sometimes 
		// these two values don't agree, particularly when the multisound is "?NO-MSND".
		// Since the value in the DMS file seems to alway look like the correct length, we'll 
		// correct the one in the multisound name list.
		for (size_t sp = 0; sp < m_systemParamList.size(); ++sp)
		{
			string name = m_multisoundList.back()->GetName();

			if (m_systemParamList[sp].UsesMultisound(name) &&
				(m_systemParamList[sp].GetMultisoundLengthInSamples(name) != uint32_t(m_multisoundList.back()->GetPCMDataLengthInSamples())))
			{
				m_systemParamList[sp].SetMultisoundLengthInSamples(
					m_multisoundList.back()->GetName(),
					m_multisoundList.back()->GetPCMDataLengthInSamples());
			}
		}

		index += 11;
	}
}

uint32_t Disk::GetNumberOfSystems()
{
	return m_systemParamList.size();
}


vector<string> Disk::GetSystemNameList()
{
	vector<string> list;

	for (auto& sp : m_systemParamList)
	{
		list.push_back(sp.GetSystemName());
	}

	return list;
}

string Disk::GetSystemName(uint32_t index)
{
	assert(index < m_systemParamList.size());

	string result;
	if (index < m_systemParamList.size())
	{
		result = m_systemParamList[index].GetSystemName();
	}
	return result;
}

void Disk::SetSystemName(uint32_t index, const std::string& name)
{
	assert(index < m_systemParamList.size());

	if (index < m_systemParamList.size())
	{
		string currentName = m_systemParamList[index].GetSystemName();

		if (name != currentName)
		{
			m_systemParamList[index].SetSystemName(name);
			m_isModified = true;
		}
	}
}

void Disk::SwapSystemOrder(uint32_t systemIndex1, uint32_t systemIndex2)
{
	if ((m_systemParamList.size() < 2) || (systemIndex1 == systemIndex2))
	{
		assert(false);
		return;
	}
	if (systemIndex1 >= m_systemParamList.size() || systemIndex2 >= m_systemParamList.size())
	{
		assert(false);
		return;
	}

	SystemParams temp = m_systemParamList[systemIndex1];
	m_systemParamList[systemIndex1] = m_systemParamList[systemIndex2];
	m_systemParamList[systemIndex2] = temp;
	m_isModified = true;
}



#if defined(_DEBUG)
void Disk::Validate()
{
	// Ensure that for each multisound that is pointed to by a System, that multisound exists in the Disk.
	for (auto& sysParams : m_systemParamList)
	{
		auto systemMultisoundNames = sysParams.GetUsedMultisoundNames();
		for (auto& multisoundName : systemMultisoundNames)
		{
			auto found = std::find_if(m_multisoundList.begin(), m_multisoundList.end(), [&multisoundName](const MultisoundUPtr& pMultisound) { return multisoundName == pMultisound->GetName(); });
			assert(found != m_multisoundList.end());
		}
	}

	DBG("Validate system multisounds:");
	// Ensure that for each multisound in the Disk, it is pointed to by some System.
	for (auto& pMultisound : m_multisoundList)
	{
		bool found = false;
		for (auto& sysParams : m_systemParamList)
		{
			DBG(pMultisound->GetName());
			if (sysParams.UsesMultisound(pMultisound->GetName()))
			{
				found = true;
				break;
			}
		}
		if (!found)
		{
			DBG("   ... not found");
		}

		// In the case of native files, not all multisounds in the disk are used by programs. Example is factory Disk 1.
		// assert(found);
	}

}
#endif
