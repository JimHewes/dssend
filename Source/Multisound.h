/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef MULTISOUND_H_INCLUDED
#define MULTISOUND_H_INCLUDED

#include <vector>
#include <string>
#include <cstdint>
#include "MidiDevice.h"
#include "DSS1.h"
#include "bson.h"

class Sound;

/**	A multisound represents a DSS-1 multisound.

	Start Address of PCM Data.\n
	Should a Multisound know the start address for its PCM data? The start address
	is ONLY meaningful when loading and saving to the DSS-1 device. A multisound could
	discover its own address by loading the multisound list, but that address would
	become invalid if the Multisound is removed from one System and put into another,
	or if it's being handled outside of a System. We could give a Multisound a reference
	to the System it's in so it would query the System for its start address. But
	a multisound may be a part of more than one system. Anyway, there's no need to even
	need the start address unless we're saving and loading from the device. So it makes
	sense to have the start address as a parameter to the save and load functions.
	Yes, it means a little more work is required of the caller, but the model makes more
	sense.

	Since Multisounds may be large and will be moved around between Disk, System, and Program, 
	they are normal handled using unique pointers.
	
	@nosubgrouping
**/
class Multisound
{
public:
	Multisound();
	Multisound(const Multisound& other);
	Multisound(const BSONDocument& doc)
	{
		FromBSON(doc);
	}
	Multisound(const std::string& filepath, uint8_t multisoundNumber, const std::string& name)
	{
		FromNative(filepath,multisoundNumber, name);
	}

	// This seems like it might be a nice constructor to have---create a Multisound by retrieving its information from a DSS-1.
	// But the reason it's not used is because the transfer can be canceled by the callback function and there is no way for a constructor to report that.
	// You could use an exception, but canceling a transfer is considered a normally expected case. So using an exception wouldn't be a good idea.
	//
	// Multisound(DSS1* pDSS1, uint8_t multisoundIndex, uint32_t PCMDataStartAddress, ProgressCallback callback);	// construct a Multisound by streaming from the DSS1 device
	// {
	// 	FromDevice(pDSS1, multisoundIndex, PCMDataStartAddress, callback);
	// }

	void SetName(const std::string& name)
	{
		assert(name.length() == 8);
		if (name.length() >= 8)
		{
			memcpy(&m_multisoundParams[1], name.c_str(), 8);
		}
	}
	std::string GetName() const
	{
		return std::string((char*)&m_multisoundParams[1], 8);
	}

	void SetIndex(uint8_t index)
	{
		assert(index < 16);
		m_multisoundParams[0] = index;
	}

	uint8_t GetIndex() const
	{
		assert(m_multisoundParams[0] < 16);
		return m_multisoundParams[0];
	}

	/**	Compares another Multisound with this one to see if they have identical contents

		For two multisounds to be identical they must have exactly the same params and PCM data.
		This is useful when merging Programs into Systems and Systems into Disks. Redundent Multisounds are discarded.

		@param[in] pOtherMultisound	 A native pointer to the other multisound to compare with.
		@returns	true if the other multisound is exactly the same, false otherwise.
	*/
	bool Equals(Multisound* pOtherMultisound);

	/** @brief	Fills the contents of this Multisound by streaming from the DSS1 device.

		@param[in] pDSS1				A pointer to the DSS1 device to stream the data from.
		@param[in] multisoundIndex		The multisound index (zero-based, i.e. multisound number - 1) in the DSS1 from which to read the data.
		@param[in] PCMDataStartAddress	A function of type ProgressCallback that will be called periodically to report the progress of the transfer.
		@param[in] callback				A function that will be called during the transfer to report progress. This can be NULL.

		@retval true	if the process completed.
		@retval false	if it was canceled by the callback function.

	*/
	bool FromDevice(DSS1* pDSS1, uint8_t multisoundIndex, uint32_t PCMDataStartAddress, ProgressCallback callback);

	/** @brief	Sends the contents of this Multisound to the DSS1 device.

		@param[in] pDSS1				A pointer to the DSS1 device to stream the data to.
		@param[in] multisoundIndex		The multisound index (zero-based, i.e. multisound number - 1). (This should already be stored as part of the Multisound params.)
		@param[in] PCMDataStartAddress	A function of type ProgressCallback that will be called periodically to report the progress of the transfer.
		@param[in] callback				A function that will be called during the transfer to report progress. This can be NULL.

		@retval true	if the process completed.
		@retval false	if it was canceled by the callback function.
		*/
	bool ToDevice(DSS1* pDSS1, uint8_t multisoundIndex, uint32_t PCMDataStartAddress, ProgressCallback callback);

	/** Returns multisound PCM data length in samples; Each sample is two bytes.

		@return multisound PCM data length in samples.
	*/
	int GetPCMDataLengthInSamples();


	/** Returns the number of sounds in the multisound.

		@return The number of sounds in the multisound.
	*/
	uint8_t GetNumberOfSounds();

	/** Given the original MIDI key, this function finds the highest top key sued by any other sound an returns the note abouve that.
	
		DSS-1 sounds only define the top key of a range and not the bottom key. The bottom key is assumed to be one greater than the 
		top key of the sound below it. So you need to look at the next lower sound to find the bottom key.
		Sounds are normally kept in order bottom to top. But just in case they are not, this function searches for the next lower sound.
	*/
	//uint8_t GetSoundBottomMIDIKey(uint8_t originalKey);


	/** Returns a copy of the Sound at the given index.

		@param index	The index of the Sound to get. Must be less than the number of Sounds in the multisound.
		@returns		The Sound at the index specified by the index parameter.
	*/
	Sound GetSound(uint8_t index);
	
	bool GetLoopEnabled();

	BSONDocument ToBSON();
	void FromBSON(const BSONDocument& doc);
	void FromNative(const std::string& filepath,uint8_t multisoundNumber, const std::string& name);

	void ToSoundfont(std::ostream& os);

private:
	int SoundWordLength(int soundIndex);

	/** The SoundPlayStartAddress is the offset within the multisound data where the sound starts to play, in terms of samples.

		This is not necessarily where the PCM data for that sound starts, it's just where the sound starts playing from.
		These are usually the same but not always. For example, the first piano sample on the first disk starts at offset 2 
		rather than 0. So two samples are actually wasted there for some reason.\n
		To get the real start of a sound's sample data you need to add up the lengths of all the sounds that came before 
		it in the multisound.
	*/
	int SoundPlayStartAddress(int soundIndex);

	/** The SoundPCMStartAddress is the offset within the multisound data where the sound's PCM data starts, in terms of samples.
	
		This is not necessarily where the sound starts playing from.
	*/
	uint32_t SoundPCMStartAddress(int soundIndex);


	void DumpParams(uint32_t absolutePCMDataStartAddress);

	std::vector<uint8_t> m_multisoundParams;
	std::vector<uint8_t> m_multisoundPCMData;
};

using MultisoundUPtr = std::unique_ptr<Multisound>;

// The MultisoundList class only exists to allow copying, since you cannot copy a vector of MultisoundUPtr.
class MultisoundList : public std::vector < MultisoundUPtr >
{
public:
	MultisoundList(){};
	MultisoundList(const MultisoundList& other)	// copy
	{
		for (auto& m : other)
		{
			push_back(std::make_unique<Multisound>(*m));
		}
	}

	MultisoundList& operator=(const MultisoundList& _Right)
	{
		if (this != &_Right)
		{
			for (auto& m : _Right)
			{
				push_back(std::make_unique<Multisound>(*m));
			}
		}
		return (*this);
	}

};


#endif  // MULTISOUND_H_INCLUDED
