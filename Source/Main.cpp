/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include "../JuceLibraryCode/JuceHeader.h"
#include "MainComponent.h"


//==============================================================================
class DSSendApplication :	public JUCEApplication
{
public:
    //==============================================================================
    DSSendApplication() {}

    const String getApplicationName()       { return ProjectInfo::projectName; }
    const String getApplicationVersion()    { return ProjectInfo::versionString; }
    bool moreThanOneInstanceAllowed()       { return false; }

    //==============================================================================
    void initialise (const String& /*commandLine*/)
    {
        // This method is where you should put your application's initialisation code..

        mainWindow = new MainWindow();
    }

    void shutdown()
    {
        // Add your application's shutdown code here..

        mainWindow = nullptr; // (deletes our window)
    }

    //==============================================================================
    void systemRequestedQuit()
    {
        // This is called when the app is being asked to quit: you can ignore this
        // request and let the app carry on running, or call quit() to allow the app to close.
        quit();
    }

    void anotherInstanceStarted (const String& /*commandLine */)
    {
        // When another instance of the app is launched while this one is running,
        // this method is invoked, and the commandLine parameter tells you what
        // the other instance's command-line arguments were.
    }


	ApplicationCommandTarget* getNextCommandTarget() override
	{
		return nullptr;
	}

	void getAllCommands(Array<CommandID>& commands) override
	{
		commands.add(StandardApplicationCommandIDs::quit);
	}

	void getCommandInfo(CommandID commandID, ApplicationCommandInfo& result) override
	{
		if (commandID == StandardApplicationCommandIDs::quit)
		{
			result.setInfo(TRANS("Quit"),
				TRANS("Quits the application"),
				"Application", 0);

			result.defaultKeypresses.add(KeyPress('q', ModifierKeys::commandModifier, 0));
		}
	}

	bool perform(const InvocationInfo& info) override
	{
		if (info.commandID == StandardApplicationCommandIDs::quit)
		{
			systemRequestedQuit();
			return true;
		}

		return false;
	}




    //==============================================================================
    /*
        This class implements the desktop window that contains an instance of
        our MainContentComponent class.
    */
    class MainWindow    : public DocumentWindow
    {
    public:
        MainWindow()  : DocumentWindow ("DSSend",
                                        Colours::lightgrey,
                                        DocumentWindow::allButtons)
        {
			m_pAppCommandManager = std::make_unique<ApplicationCommandManager>();

			// this lets the command manager use keypresses that arrive in our window to send out commands
			addKeyListener(m_pAppCommandManager->getKeyMappings());

			MainContentComponent* pMainContentComponent = new MainContentComponent(m_pAppCommandManager.get());
			setContentOwned(pMainContentComponent, true);	// pMainContentComponent will be deleted by DocumentWindow.

			m_pAppCommandManager->registerAllCommandsForTarget(pMainContentComponent);
			m_pAppCommandManager->registerAllCommandsForTarget(DSSendApplication::getInstance());

            centreWithSize (getWidth(), getHeight());

			setTitleBarButtonsRequired(TitleBarButtons::closeButton | TitleBarButtons::minimiseButton, false);

            setVisible (true);
        }

        void closeButtonPressed()
        {
            // This is called when the user tries to close this window. Here, we'll just
            // ask the app to quit when this happens, but you can change this to do
            // whatever you need.
			bool okToQuit = true;
			if (((MainContentComponent*)getContentComponent())->IsModified())
			{
				okToQuit = AlertWindow::showOkCancelBox(AlertWindow::QuestionIcon,
					"Changes not saved!",
					"Your current Disk has not been saved since it was last modified. Are you sure you want to quit the program and lose your changes?",
					"Yes",
					"No",
					this,
					nullptr);
			}
			if (okToQuit)
			{
				JUCEApplication::getInstance()->systemRequestedQuit();
			}

        }


        /* Note: Be careful if you override any DocumentWindow methods - the base
           class uses a lot of them, so by overriding you might break its functionality.
           It's best to do all your work in your content component instead, but if
           you really have to override any DocumentWindow methods, make sure your
           subclass also calls the superclass's method.
        */

    private:
		std::unique_ptr<ApplicationCommandManager> m_pAppCommandManager;

		TooltipWindow tooltipWindow;	// to add tooltips to an application, you
										// just need to create one of these and leave it
										// there to do its work..

        JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainWindow)
    };

private:
    ScopedPointer<MainWindow> mainWindow;
};

//==============================================================================
// This macro generates the main() routine that launches the app.
START_JUCE_APPLICATION (DSSendApplication)
