/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

// This file is Windows-specific.

#include <Windows.h>
#include <mmsystem.h>

#include <vector>
#include <memory>
#include <exception>
#include <boost/scope_exit.hpp>
#include "MidiDevice.h"

using std::string;
using std::vector;
using MIDIHDRUPtr = std::unique_ptr<MIDIHDR>;

// Free up the memory a vector is holding without destroying it.
template<typename T>
inline void FreeVector(std::vector<T>& v)
{
	std::vector<T> t;
	t.swap(v);
}

std::string UTF16ToUTF8(const std::wstring& source)
{
	int length = (int)source.length();
	std::vector<char> asciiString(2 * (length + 1));	// mult by 2 since some chars may still take up more than one byte.

	WideCharToMultiByte(CP_UTF8, 				// code page: UTF8 text
		0,						// no special flags
		source.c_str(),			// input string
		-1,
		&asciiString[0],
		(int)asciiString.size(),
		0,
		0);

	return &asciiString[0];
}

class MidiInBuffer
{
private:
	HMIDIIN			m_hMidiIn;	// Just a copy. MidiInBuffer is not the owner
	MIDIHDRUPtr		m_pHdr;
	std::vector<unsigned char>	m_pBuffer;
	int				m_bufferLength;
	int				m_recordBufferLength;
	bool			m_isAdded;

public:
	MidiInBuffer(HMIDIIN hMidiIn, unsigned int length);
	~MidiInBuffer();
	void AddBuffer();
	bool IsAdded(){ return m_isAdded; }
	void RemoveBuffer();
	bool IsDone() { return ((m_pHdr->dwFlags & MHDR_DONE) != 0); }
	void CopyData(std::vector<unsigned char>& buffer, uint32_t startIndex, uint32_t& bytesRecorded);
};

MidiInBuffer::MidiInBuffer(HMIDIIN hMidiIn, unsigned int bufferLength)
	:m_hMidiIn(hMidiIn)
	, m_bufferLength(bufferLength)
	, m_isAdded(false)
{}

MidiInBuffer::~MidiInBuffer()
{
	RemoveBuffer();
}

void MidiInBuffer::RemoveBuffer()
{
	if (m_isAdded)
	{
		midiInUnprepareHeader(m_hMidiIn, m_pHdr.get(), sizeof(MIDIHDR));
		m_pHdr.reset();
		FreeVector(m_pBuffer);
		m_isAdded = false;
	}
}

void MidiInBuffer::AddBuffer()
{
	// Allocate memory
	// These memory allocations may fail.		
	MIDIHDRUPtr pHdr = std::make_unique<MIDIHDR>();
	m_pBuffer.resize(m_bufferLength);

	m_pHdr = std::move(pHdr);	// since buffer resize succeeded, assign pointer.

	ZeroMemory(m_pHdr.get(), sizeof(MIDIHDR));

	m_pHdr->lpData = (LPSTR)&m_pBuffer[0];
	m_pHdr->dwBufferLength = m_bufferLength;
	m_recordBufferLength = m_bufferLength;	// saved for use in receive to calculate timeout.

	MMRESULT res = midiInPrepareHeader(m_hMidiIn, m_pHdr.get(), sizeof(MIDIHDR));
	if (res != MMSYSERR_NOERROR)
	{
		m_pHdr.reset();
		FreeVector(m_pBuffer);
		throw std::runtime_error("Error preparing MIDI In header.");
	}

	res = midiInAddBuffer(m_hMidiIn, m_pHdr.get(), sizeof(MIDIHDR));
	if (res != MMSYSERR_NOERROR)
	{
		midiInUnprepareHeader(m_hMidiIn, m_pHdr.get(), sizeof(MIDIHDR));
		m_pHdr.reset();
		FreeVector(m_pBuffer);
		throw std::runtime_error("Error adding MIDI In buffer.");
	}
	m_isAdded;
}

void MidiInBuffer::CopyData(std::vector<unsigned char>& buffer, uint32_t startIndex, uint32_t& bytesRecorded)
{
	// copy data to other buffer
	int lengthToCopy = m_pHdr->dwBytesRecorded;

	if (buffer.size() < startIndex + lengthToCopy)
	{
		buffer.resize(startIndex + lengthToCopy);
	}

	memcpy(&buffer[startIndex], m_pHdr->lpData, lengthToCopy);
	bytesRecorded = lengthToCopy;
}


////////////////////////////////////////////////////////////////////////
//
//			MidiInDeviceImpl
//
//
///////////////////////////////////////////////////////////////////////

class MidiInDeviceImpl
{
public:
	MidiInDeviceImpl();
	~MidiInDeviceImpl();

	std::vector<std::string> GetDeviceList();

	void Open(const std::string deviceName);
	void Open(uint32_t deviceIndex);
	void Close();

	bool IsOpen() const throw()
	{
		return m_isOpen;
	}

	void RecordingOn(int bufferLength);
	void RecordingOff();
	bool IsRecordingOn()
	{
		return m_recordEnabled;
	}

	bool ReceiveData(std::vector<unsigned char>& buffer, uint32_t startIndex, uint32_t& bytesRecorded);

private:	// member variables
	HMIDIIN			m_hMidiIn;
	bool			m_isOpen;
	bool			m_recordEnabled;
	//MIDIHDR*		m_pHdr;
	//unsigned char*	m_pBuffer;
	int				m_recordBufferLength;

	// The reason we declare a pointer to an array here instead of just an array 
	// is because C++/CLI will complain we cannot use mixed types.
	// A managed class doesn't allow a native array as a member, but it will allow a pointer to one.
	//typedef MidiInBuffer **MidiInBufferArray;
	//static MidiInBufferArray m_pMidiInBufferArray = &g_MidiInBufferArray;
	using MidiInBufferUPtr = std::unique_ptr<MidiInBuffer>;
	std::vector<MidiInBufferUPtr> m_midiInBufferArray;
};

MidiInDeviceImpl::MidiInDeviceImpl()
	:m_isOpen(false)
	, m_recordEnabled(false)
	, m_hMidiIn((HMIDIIN)INVALID_HANDLE_VALUE)
{
}

MidiInDeviceImpl::~MidiInDeviceImpl()
{
	Close();
}

vector<string> MidiInDeviceImpl::GetDeviceList()
{
	UINT numMidiInDevices = midiInGetNumDevs();

	vector<string> deviceNames(numMidiInDevices);

	MIDIINCAPS midiInCaps;

	for (UINT i = 0; i < numMidiInDevices; i++)
	{
		midiInGetDevCaps(i, &midiInCaps, sizeof(midiInCaps));
		deviceNames[i] = UTF16ToUTF8(midiInCaps.szPname);
	}

	return deviceNames;
}

void MidiInDeviceImpl::Open(const string deviceName)
{
	vector<string> deviceList = GetDeviceList();

	bool found = false;

	for (size_t i = 0; i < deviceList.size(); i++)
	{
		if (0 == deviceList[i].compare(deviceName))
		{
			Open(i);
			found = true;
			break;
		}
	}

	if (!found)
	{
		throw std::runtime_error("Midi Input device not found.");
	}
}


void MidiInDeviceImpl::Open(uint32_t deviceIndex)
{
	if (m_isOpen)
	{
		Close();
	}

	UINT numMidiInDevices = midiInGetNumDevs();

	if (deviceIndex >= numMidiInDevices)
	{
		throw std::runtime_error("The MIDI Input index parameter is out of range.");
	}

	HMIDIIN tempHandle;
	// m_hMidiIn is part of the garbage-collected MidiInDevice class. So we can't 
	// pass in a pointer to it. Use a temp variable and then assign afterwards.
	MMRESULT result = midiInOpen(&tempHandle, deviceIndex, NULL, NULL, NULL);

	if (result == MMSYSERR_NOERROR)
	{
		m_hMidiIn = tempHandle;
		m_isOpen = true;
	}
	else
	{
		string exceptionMsg = "Error when opening MIDI Input: ";
		switch (result)
		{
		case MMSYSERR_ALLOCATED:
			exceptionMsg += "The specified resource is already allocated.";
			break;
		case MMSYSERR_BADDEVICEID:
			exceptionMsg += "The specified device identifier is out of range.";
			break;
		case MMSYSERR_INVALFLAG:
			exceptionMsg += "The flags specified by dwFlags are invalid.";
			break;
		case MMSYSERR_INVALPARAM:
			exceptionMsg += "The specified pointer or structure is invalid.";
			break;
		case MMSYSERR_NOMEM:
			exceptionMsg += "The system is unable to allocate or lock memory.";
			break;
		default:
			exceptionMsg += "Unknown error.";
			break;
		};

		throw std::runtime_error(exceptionMsg);
	}

}

void MidiInDeviceImpl::Close()
{
	if (m_isOpen)
	{
		RecordingOff();

		// MMRESULT res = 
		midiInClose(m_hMidiIn);

		m_hMidiIn = (HMIDIIN)INVALID_HANDLE_VALUE;
		m_isOpen = false;
	}
}


// I tried to find a way to use pinned GC memory as a buffer for recording.
// But there doesn't seem to be a way. I need to keep the memory pinned 
// across function calls. But I'm not allowed to use the pin_ptr type
// as a member variable. Any local pin_ptr that I make will go out of scope 
// and unpin the memory at the end of this function.
// So it would seem that I need to use unmanaged memory and then copy the 
// data to a managed buffer.
void MidiInDeviceImpl::RecordingOn(int bufferLength)
{
	if (!m_isOpen)
	{
		throw std::runtime_error("Device not open.");
	}
	assert(m_hMidiIn != (HMIDIIN)INVALID_HANDLE_VALUE);

	if ((bufferLength < 0) || (bufferLength > 65536))
	{
		throw std::runtime_error("Parameter is out of range.");
	}

	if (m_recordEnabled)
	{
		// It's not an error to enable recording when it's already enabled.
		return;
	}

	//m_midiInBufferArray = gcnew array<MidiInBuffer*>(1);

	MidiInBufferUPtr buf = std::make_unique<MidiInBuffer>(m_hMidiIn, bufferLength);
	buf->AddBuffer();
	m_midiInBufferArray.push_back(std::move(buf));

	MMRESULT res = midiInStart(m_hMidiIn);
	if (res != MMSYSERR_NOERROR)
	{
		FreeVector(m_midiInBufferArray);	// Doesn't exactly do the same thing as the two lines below, but OK for now.
		//delete m_midiInBufferArray[0];
		//m_midiInBufferArray[0] = NULL;
		throw std::runtime_error("Error starting MIDI In recording.");
	}

	m_recordEnabled = true;

	return;
}

void MidiInDeviceImpl::RecordingOff() 
{
	assert(m_isOpen);
	assert(m_hMidiIn != (HMIDIIN)INVALID_HANDLE_VALUE);

	if (!m_isOpen || !m_recordEnabled) // It's not an error to disable recording when it's already disabled.
	{
		return;
	}

	// Note that the MidiInStop() does not mark queued buffers as done if they 
	// are empty. To do that we need to use MidiInReset().
	// MMRESULT res = 
	midiInReset(m_hMidiIn);

	FreeVector(m_midiInBufferArray);	// Doesn't exactly do the same thing as the line below, but OK for now.
	//delete m_midiInBufferArray[0];

	m_recordEnabled = false;
	return;
}


bool MidiInDeviceImpl::ReceiveData(vector<unsigned char>& buffer, uint32_t startIndex, uint32_t& bytesRecorded)
{

	if (!m_isOpen)
	{
		return false;
	}
	assert(m_hMidiIn != (HMIDIIN)INVALID_HANDLE_VALUE);

	if (!m_recordEnabled)
	{
		return false;
	}
	if ((startIndex + 1) > buffer.size())
	{
		assert(false);
		return false;
	}

	bytesRecorded = 0;

	// The timeout to wait will depend on the size of the data expected.
	// Each byte takes 0.320 milliseconds according to the MIDI spec.
	float timeInMs = m_recordBufferLength * 0.320f;

	// Let's wait at least 200% of the expected time. And at least 5 seconds.
	timeInMs *= 2.0f;
	int waitTimeInMs = max((int)timeInMs, 5000);	// maximum wait time in ms

	// wait for buffer to be finished.
	while (!m_midiInBufferArray[0]->IsDone() && (waitTimeInMs > 0))
	{
		Sleep(50);
		waitTimeInMs -= 50;
	}
	if (!m_midiInBufferArray[0]->IsDone())
	{
		// If it timed out, return an error.
		return false;
	}

	m_midiInBufferArray[0]->CopyData(buffer, startIndex, bytesRecorded);

	return true;

}

////////////////////////////////////////////////////////////////////////
//
//			MidiInDevice
//
//
///////////////////////////////////////////////////////////////////////

MidiInDevice::MidiInDevice()
{
	m_pImpl = std::make_unique<MidiInDeviceImpl>();
}
MidiInDevice::~MidiInDevice()
{
	m_pImpl.reset();
}

std::vector<std::string> MidiInDevice::GetDeviceList()
{	return m_pImpl->GetDeviceList();	}
void MidiInDevice::Open(const std::string deviceName)
{	m_pImpl->Open(deviceName);	}
void MidiInDevice::Open(uint32_t deviceIndex)
{	m_pImpl->Open(deviceIndex);	}
void MidiInDevice::Close()
{	m_pImpl->Close();	}
bool MidiInDevice::IsOpen() const throw()
{	return m_pImpl->IsOpen();	}
void MidiInDevice::RecordingOn(int bufferLength)
{	return m_pImpl->RecordingOn(bufferLength);	}
void MidiInDevice::RecordingOff()
{	return m_pImpl->RecordingOff();	}
bool MidiInDevice::IsRecordingOn()
{	return m_pImpl->IsRecordingOn();	}
bool MidiInDevice::ReceiveData(std::vector<unsigned char>& buffer, uint32_t startIndex, uint32_t& bytesRecorded)
{	return m_pImpl->ReceiveData(buffer, startIndex, bytesRecorded);	}

////////////////////////////////////////////////////////////////////////
//
//			MidiOutDeviceImpl
//
//
///////////////////////////////////////////////////////////////////////

class MidiOutDeviceImpl
{
public:
	MidiOutDeviceImpl();
	~MidiOutDeviceImpl();

	std::vector<std::string> GetDeviceList();
	void Open(const std::string deviceName);
	void Open(uint32_t deviceIndex);
	void Close();

	bool SendSysEx(std::vector<unsigned char>& msg, uint32_t lengthToSend);

	bool IsOpen() const noexcept
	{
		return m_isOpen;
	}

private:
	HMIDIOUT	m_hMidiOut;
	bool		m_isOpen;

};

MidiOutDeviceImpl::MidiOutDeviceImpl()
	:m_isOpen(false),
	m_hMidiOut((HMIDIOUT)INVALID_HANDLE_VALUE)
{
}

MidiOutDeviceImpl::~MidiOutDeviceImpl()
{

	if (m_isOpen)
	{
		midiOutClose(m_hMidiOut);
		m_hMidiOut = (HMIDIOUT)INVALID_HANDLE_VALUE;
		m_isOpen = false;
	}
}

vector<string> MidiOutDeviceImpl::GetDeviceList()
{
	UINT numMidiOutDevices = midiOutGetNumDevs();

	vector<string> deviceNames(numMidiOutDevices);

	MIDIOUTCAPS midiOutCaps;

	for (UINT i = 0; i < numMidiOutDevices; i++)
	{
		midiOutGetDevCaps(i, &midiOutCaps, sizeof(midiOutCaps));

		deviceNames[i] = UTF16ToUTF8(midiOutCaps.szPname);

	}

	return deviceNames;
}

void MidiOutDeviceImpl::Open(const string deviceName)
{
	vector<string> deviceList = GetDeviceList();

	bool found = false;

	for (size_t i = 0; i < deviceList.size(); i++)
	{
		if (0 == deviceList[i].compare(deviceName))
		{
			Open(i);
			found = true;
			break;
		}
	}

	if (!found)
	{
		throw std::runtime_error("MIDI Output device not found.");
	}
}

void MidiOutDeviceImpl::Open(uint32_t deviceIndex)
{
	if (m_isOpen)
	{
		return;
		//Close();
	}

	UINT numMidiOutDevices = midiOutGetNumDevs();

	if (deviceIndex >= numMidiOutDevices)
	{
		throw std::runtime_error("The MIDI Output index parameter is out of range.");
	}

	HMIDIOUT tempHandle;
	// m_hMidiOut is part of the garbage-collected MidiOutDevice class. So we can't 
	// pass in a pointer to it. Use a temp variable and then assign afterwards.
	MMRESULT result = midiOutOpen(&tempHandle, deviceIndex, NULL, NULL, NULL);

	if (result == MMSYSERR_NOERROR)
	{
		m_hMidiOut = tempHandle;
		m_isOpen = true;
	}
	else
	{
		string exceptionMsg = "Error when opening MIDI Input: ";
		switch (result)
		{
		case MMSYSERR_ALLOCATED:
			exceptionMsg += "The specified resource is already allocated.";
			break;
		case MMSYSERR_BADDEVICEID:
			exceptionMsg += "The specified device identifier is out of range.";
			break;
		case MMSYSERR_INVALFLAG:
			exceptionMsg += "The flags specified by dwFlags are invalid.";
			break;
		case MMSYSERR_INVALPARAM:
			exceptionMsg += "The specified pointer or structure is invalid.";
			break;
		case MMSYSERR_NOMEM:
			exceptionMsg += "The system is unable to allocate or lock memory.";
			break;
		default:
			exceptionMsg += "Unknown error.";
			break;
		};

		throw std::runtime_error(exceptionMsg);

	}

}

void MidiOutDeviceImpl::Close()
{
	if (m_isOpen)
	{
		MMRESULT res = midiOutReset(m_hMidiOut);
		if (res != MMSYSERR_NOERROR)
		{
			if (res == MMSYSERR_INVALHANDLE)
			{
				throw std::runtime_error("Invalid handle to MidiOutReset.");
			}
			else
			{
				throw std::runtime_error("Unknown MidiOutReset error.");
			}
		}

		res = midiOutClose(m_hMidiOut);
		if (res == MMSYSERR_NOERROR)
		{
			m_hMidiOut = (HMIDIOUT)INVALID_HANDLE_VALUE;
			m_isOpen = false;
		}
		else
		{
			string exceptionMsg = "MIDI Out Close error:";
			switch (res)
			{
			case MIDIERR_STILLPLAYING:
				exceptionMsg += "Buffers are still in the queue.";
				break;
			case MMSYSERR_INVALHANDLE:
				exceptionMsg += "The specified device handle is invalid.";
				break;
			case MMSYSERR_NOMEM:
				exceptionMsg += "The system is unable to load mapper string description. (Memory error)";
				break;
			default:
				exceptionMsg += "Unknown.";
				break;
			};

			throw std::runtime_error(exceptionMsg);
		}	// end of: if(res == MMSYSERR_NOERROR)
	}	// end of:  if(m_isOpen)
}


bool MidiOutDeviceImpl::SendSysEx(vector<unsigned char>& msg, uint32_t lengthToSend)
{
	if (!m_isOpen)
	{
		return false;
	}

	assert(m_hMidiOut);
	assert(msg.size() >= lengthToSend);

	if (lengthToSend == 0)
	{
		return true;
	}
	if ((lengthToSend < 0) || (lengthToSend > 65536))
	{
		return false;
	}
	if (lengthToSend > msg.size())
	{
		return false;
	}

	MIDIHDR hdr;
	ZeroMemory(&hdr, sizeof(hdr));

	//pin_ptr<byte> pMsg = &msg[0];   // the msg array is now pinned until pMsg goes 
	// out of scope or is set to nullptr.

	hdr.lpData = (LPSTR)&msg[0];
	hdr.dwBufferLength = lengthToSend;

	MMRESULT res = midiOutPrepareHeader(m_hMidiOut, &hdr, sizeof(hdr));
	if (res != MMSYSERR_NOERROR)
	{
		return false;
	}

	res = midiOutLongMsg(m_hMidiOut, &hdr, sizeof(hdr));

	if (res != MMSYSERR_NOERROR)
	{
		midiOutUnprepareHeader(m_hMidiOut, &hdr, sizeof(hdr));
		return false;
	}

	// The timeout to wait will depend on the size of the data to send.
	// Each byte takes 0.320 milliseconds according to the MIDI spec.
	float timeInMs = lengthToSend * 0.320f;

	// Let's wait at least 200% of the expected time. And at least 7 seconds.
	timeInMs *= 2.0f;
	int waitTimeInMs = max((int)timeInMs,7000);	// maximum wait time in ms

	// Wait for the data to be sent
	while (((hdr.dwFlags & MHDR_DONE) == 0) && (waitTimeInMs > 0))
	{
		Sleep(50);
		waitTimeInMs -= 50;
	}
	if ((hdr.dwFlags & MHDR_DONE) == 0)
	{
		// If it timed out, return an error.
		midiOutUnprepareHeader(m_hMidiOut, &hdr, sizeof(hdr));
		return false;
	}

	res = midiOutUnprepareHeader(m_hMidiOut, &hdr, sizeof(hdr));
	if (res != MMSYSERR_NOERROR)
	{
		return false;
	}

	return true;
}



////////////////////////////////////////////////////////////////////////
//
//			MidiOutDevice
//
//
///////////////////////////////////////////////////////////////////////

MidiOutDevice::MidiOutDevice()
{
	m_pImpl = std::make_unique<MidiOutDeviceImpl>();
}
MidiOutDevice::~MidiOutDevice()
{
	m_pImpl.reset();
}
std::vector<std::string> MidiOutDevice::GetDeviceList()
{
	return m_pImpl->GetDeviceList();
}
void MidiOutDevice::Open(const std::string deviceName)
{
	m_pImpl->Open(deviceName);
}
void MidiOutDevice::Open(uint32_t deviceIndex)
{
	m_pImpl->Open(deviceIndex);
}
void MidiOutDevice::Close()
{
	m_pImpl->Close();
}
bool MidiOutDevice::SendSysEx(std::vector<unsigned char>& msg, uint32_t lengthToSend)
{
	return m_pImpl->SendSysEx(msg, lengthToSend);
}

bool MidiOutDevice::IsOpen() const noexcept
{
	return m_pImpl->IsOpen();
}


MidiIoDevice::~MidiIoDevice()
{
	// It's important to Close the device here because if MidiIoDevice destructs without being closed first,
	// the m_eventSignal can get permanently signaled and the transfer thread will just run infintely without 
	// anything in the queue. This can cause a crash.
	Close();
}

void MidiIoDevice::Open(string inputDeviceName, string outputDevicename)
{
	m_numCancelRequests = 0;
	m_quit = false;
	bool commit = false;

	m_midiInDevice.Open(inputDeviceName);
	BOOST_SCOPE_EXIT_ALL(&, this) { if(!commit) this->m_midiInDevice.Close(); };	// For some reason BOOST_SCOPE_EXIT(&commit, this_) won't compile here in VS2013

	m_midiOutDevice.Open(outputDevicename);
	BOOST_SCOPE_EXIT_ALL(&, this) { if (!commit) this->m_midiOutDevice.Close(); };

	// A semaphore is used to keep count of how many transfer requests are 
	// currently in the queue.
	m_eventSignal = std::make_unique<boost::interprocess::interprocess_semaphore>(0);

	// start worker thread
	auto threadFunc = [this](){	TransferThreadFunction(); };
	m_pWorkerThread = std::make_unique<boost::thread>(threadFunc);	// makes use of boost::thread move constructor
	commit = true;
}


bool MidiIoDevice::IsOpen() const noexcept
{
	return m_midiInDevice.IsOpen() && m_midiOutDevice.IsOpen();
}


void MidiIoDevice::Close()
{
	if (IsOpen())
	{
		m_midiInDevice.Close();
		m_midiOutDevice.Close();

		// Tell thread to quit.
		m_quit = true;
		m_eventSignal->post();

		// Wait for the thread to exit. If the thread still did not exit by 
		// this time there's nothing we can do anyway and so we just continue.
		// The only reason we wait is to at least try for a nice cleanup.
		m_pWorkerThread->join();
		m_pWorkerThread.reset();
		m_eventSignal.reset();
	}
}


void MidiIoDevice::TransferThreadFunction()
{
	bool done = false;
	while (!done)
	{
		m_eventSignal->wait();

		if (m_quit)
		{
			done = true;
			continue;
		}

		TransferRequestSPtr pTransReq;

		if (!m_queue.empty())
		{
			pTransReq = m_queue.front();
			m_queue.pop();

			if (m_numCancelRequests > 0)
			{
				if (pTransReq->cancelRequest)
				{
					--m_numCancelRequests;

				}
				// If there is at least one cancel request in the queue,
				// then ignore all transfer requests until we reach the
				// cancel request.
				continue;
			}
			else	// if the queue is empty, loop back up and wait for a new signal.
			{
				m_numCancelRequests = 0; // in case this gets out of sync somehow.

				continue;
			}
		}

		// Now process the request. Any of the functions in ProcessRequest may throw 
		// an exception.
		try
		{
			ProcessRequest(*pTransReq);
			pTransReq->Success = true;
		}
		catch (...)
		{
			pTransReq->Success = false;
		}

		// Return the result to the originator of the request
		//pTransReq->ReportBack();
		if (pTransReq->Callback != nullptr)
		{

			pTransReq->Callback(pTransReq);
		}

	}

}

void MidiIoDevice::ProcessRequest(TransferRequest& transReq)
{
	if (!transReq.ReceiveBuffer.empty())
	{
		Write(transReq);
	}
	else
	{
		//Request = transReq.SendBuffer;

		transReq.NumberOfBytesRead = Read(transReq);
	}
}

void MidiIoDevice::SubmitRequest(TransferRequestSPtr transReq)
{
	// check that all required fields are filled out.
	// ReceiveBuffer might be null if we're sending a play mode request.
	if ((transReq->Callback == nullptr) || (transReq->SendBuffer.empty()))
	{
		throw std::runtime_error("Bad field in read request");
	}

	m_queue.push(transReq);

	if (transReq->cancelRequest)
	{
		++m_numCancelRequests;
	}

	// Signal the worker thread to wakeup because there's a new request in the queue.
	m_eventSignal->post();
	
}

int MidiIoDevice::Read(TransferRequest& transReq)
{
	boost::mutex::scoped_lock lock(m_mutex);

	transReq.Success = false;
	uint32_t bytesRecorded = 0;

	if (transReq.ReceiveBuffer.empty())
	{
		throw std::runtime_error("Read buffer length is zero.");
	}
	if ((transReq.ReceiveStartIndex + 1) > transReq.ReceiveBuffer.size())
	{
		throw std::runtime_error("startIndex is past end of buffer.");
	}

	m_midiInDevice.RecordingOn(transReq.ReceiveBuffer.size());
	BOOST_SCOPE_EXIT(this_) { this_->m_midiInDevice.RecordingOff(); } BOOST_SCOPE_EXIT_END

	bool res = m_midiOutDevice.SendSysEx(transReq.SendBuffer, transReq.SendBuffer.size());

	res = m_midiInDevice.ReceiveData(transReq.ReceiveBuffer, transReq.ReceiveStartIndex, bytesRecorded);
	if (!res)
	{
		throw std::runtime_error("Read error.");
	}

	transReq.NumberOfBytesRead = bytesRecorded;
	transReq.Success = true;

	return bytesRecorded;
}

void MidiIoDevice::Write(TransferRequest& transReq)
{
	boost::mutex::scoped_lock lock(m_mutex);
	transReq.Success = false;

	bool res = m_midiOutDevice.SendSysEx(transReq.SendBuffer, transReq.SendBuffer.size());

	if (!res)
	{
		throw std::runtime_error("Error writing to device.");
	}
	transReq.Success = true;


}