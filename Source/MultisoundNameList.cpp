/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include "MultisoundNameList.h"
#include "DSS1.h"
#include "bson.h"

#if defined(_DEBUG)
#include "../JuceLibraryCode/JuceHeader.h"	// include just for DBG()
#else
#define DBG(x)
#endif

void MultisoundNameList::FromDevice(DSS1* pDSS1)
{
	vector<uint8_t> buf = pDSS1->GetMultisoundNameList();
	uint8_t numMs = buf[0];
	assert(buf.size() == (size_t)(1 + numMs * 14));

	if (numMs > 0)
	{
		uint8_t* pName = &buf[1];
		for (int i = 0; i < numMs; ++i)
		{
			push_back(MultisoundNameListItem((char*)pName, ToLength(pName + 8)));
			pName += 14;
		}
	}
}

void MultisoundNameList::ToDevice(DSS1* pDSS1)
{
	vector<uint8_t> buf;
	buf.resize(size() * 14 + 1);
	buf[0] = (uint8_t)size();
	int index = 1;
	for (uint8_t i = 0; i < size(); ++i)
	{
		memcpy(&buf[index], at(i).name.c_str(), 8);
		index += 8;
		FromLength(&buf[index], at(i).length);
		index += 6;
	}
	pDSS1->SendMultisoundNameList((uint8_t)size(), buf);
}

uint32_t MultisoundNameList::GetPCMDataSizeInSamples()
{
	uint32_t result = 0;
	for (auto& item : *this)
	{
		result += item.length;
	}
	return result;
}


BSONDocument MultisoundNameList::ToBSON()
{
	BSONDocument doc;

	for (size_t i = 0; i < size(); ++i)
	{
		BSONDocument itemDoc;
		itemDoc.AddElementString("Name", at(i).name);
		itemDoc.AddElementInt32("Length", at(i).length);

		// We're adding MultisoundName items as array elements. So the name of each element is a text representation of the array index.
		doc.AddElementDocument(i, itemDoc);
	}

	return doc;
}

void MultisoundNameList::FromBSON(const BSONDocument& doc)
{
	uint8_t numMultisoundItems = (uint8_t)doc.GetNumElements();
	for (size_t i = 0; i < numMultisoundItems; ++i)
	{
		BSONElementDocument* pMultisoundItemElemDoc = dynamic_cast<BSONElementDocument*>(doc.GetElement(i));
		if (pMultisoundItemElemDoc)
		{
			BSONElementString* pNameElement = dynamic_cast<BSONElementString*>(pMultisoundItemElemDoc->GetDoc().GetElement("Name"));
			BSONElementInt32* pLengthElement = dynamic_cast<BSONElementInt32*>(pMultisoundItemElemDoc->GetDoc().GetElement("Length"));
			assert(pNameElement && pLengthElement);
			if (pNameElement && pLengthElement)
			{
				push_back(MultisoundNameListItem(pNameElement->GetValue(), pLengthElement->GetValue()));
				DBG(pNameElement->GetValue());
			}
		}
	}
}

void MultisoundNameList::FromNative(std::vector<uint8_t>& buf)
{
	int offset = 1024;
	m_numberOfMultisoundNames = buf[offset++];

	for (int i = 0; i < m_numberOfMultisoundNames; ++i)
	{
		std::string name((char*)&buf[offset], 8);
		uint32_t dataLen = buf[offset + 8] + (buf[offset + 9] << 8) + (buf[offset + 10] << 16);
		push_back(MultisoundNameListItem(name, dataLen));
		offset += 11;
	}
}

