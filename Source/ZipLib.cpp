/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/


// Disable Warning for unreferenced formal parameter when using BOOST_LOG_TRIVIAL macro.
#pragma warning (push)
#pragma warning( disable : 4100 )
#include <boost/log/trivial.hpp>	// For debug logging
#pragma warning (pop)

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <string>
#include <vector>
#include <cassert>
#include "ZipLib.h"
#include "zip.h"

// Disable warning that a function marked as __forceinline was not inlined (in boost library)
#pragma warning( disable : 4714 )

using boost::posix_time::ptime;
using boost::posix_time::second_clock;
using boost::gregorian::day_clock;
using std::string;
using std::vector;

ZipLib::ZipLib(void)
	:m_compressionLevel(6)
	,m_storeWithPath(false)
{
}

bool ZipLib::CreateLibrary(std::string libraryName)
{
	if(m_fileList.size() == 0)
	{
		return false;
	}

	// Create and open the zip file
	zipFile zf = zipOpen64(libraryName.c_str(), APPEND_STATUS_CREATE);
	if(zf == nullptr)
	{
		BOOST_LOG_TRIVIAL(debug) << "CreateLibrary couldn't open the zip library: " << libraryName.c_str();
		return false;
	}

	// Allocate file buffer
	vector<unsigned char>fileBuf(128*1024);
	string filename;
	string filenameInZip;
	zip_fileinfo zipInfo;

	int zipErr = ZIP_OK;

	// Get the current time, used for what is called the modified time of the zip file.
	// Get the UTC time.
	ptime nowUtc(day_clock::universal_day(), second_clock::universal_time().time_of_day());

	for(FileListType::iterator iter = m_fileList.begin(); iter != m_fileList.end(); ++iter)
	{

		filename = *iter;

		// Generate file name used for the file entry in the zip file
		filenameInZip = filename;
		if(! m_storeWithPath)
		{
			std::string::size_type lastPathSeparator = filename.find_last_of("/\\");
			if(lastPathSeparator != std::string::npos)
			{
				filenameInZip = filename.substr(lastPathSeparator + 1);
			}
		}

		memset(&zipInfo, 0, sizeof(zipInfo));
		zipInfo.tmz_date.tm_sec = nowUtc.time_of_day().seconds();
		zipInfo.tmz_date.tm_min = nowUtc.time_of_day().minutes();
		zipInfo.tmz_date.tm_hour = nowUtc.time_of_day().hours();
		zipInfo.tmz_date.tm_mday = nowUtc.date().day();
		zipInfo.tmz_date.tm_mon = nowUtc.date().month() - 1;
		zipInfo.tmz_date.tm_year = nowUtc.date().year() - 1980;

		// Create a new entry in the zip file
		zipErr = zipOpenNewFileInZip3_64(zf, filenameInZip.c_str(), &zipInfo,
						NULL, 0, NULL, 0, NULL/*comment*/,
						(m_compressionLevel != 0) ? Z_DEFLATED : 0,
						m_compressionLevel, 0,
						-MAX_WBITS, DEF_MEM_LEVEL, Z_DEFAULT_STRATEGY,
						NULL, 0L, 0);

		if(zipErr != ZIP_OK)
		{
			break;
		}

		// Open the file to be added
		FILE* srcFile;
		int err = fopen_s(&srcFile, filename.c_str(), "rb");
		if(err != 0)
		{
			BOOST_LOG_TRIVIAL(debug) << "CreateLibrary could not open file for reading: " << filename.c_str();
			zipErr = ZIP_ERRNO;
			break;
		}

		// Write the file to the zip library
		while(feof(srcFile) == 0)
		{
			size_t bytesInBuf = fread(&fileBuf[0], 1, fileBuf.size(), srcFile);

			if(ferror(srcFile))
			{
				BOOST_LOG_TRIVIAL(debug) << "CreateLibrary opened file " << filename.c_str() << " but there was a read error.";
				zipErr = ZIP_ERRNO;
				break;
			}

			if(bytesInBuf > 0)
			{
				zipErr = zipWriteInFileInZip(zf, &fileBuf[0], bytesInBuf);

				if(zipErr != ZIP_OK)
				{	
					BOOST_LOG_TRIVIAL(debug) << "CreateLibrary got an error writing the file " << filename.c_str() << " to the zip file.";
					break;
				}
			}
		}

		fclose(srcFile);

		zipCloseFileInZip(zf);

		if(zipErr != ZIP_OK)
		{
			break;
		}
	}

	zipClose(zf, NULL);

	return (zipErr == ZIP_OK);

}

void ZipLib::AddFile(std::string filename)
{
	m_fileList.push_back(filename);
}

bool ZipLib::SetDirectory(std::string directory)
{
	m_directory = directory;
	return true;
}

bool ZipLib::SetCompressionLevel(int level)
{
	assert((level >= 0) && (level <= 9));
	bool result = false;
	if((level >= 0) && (level <= 9))
	{
		m_compressionLevel = level;
		result = true;
	}
	return result;
}

void ZipLib::SetStoreWithPath(bool storeWithPath)
{
	m_storeWithPath = storeWithPath;
}

