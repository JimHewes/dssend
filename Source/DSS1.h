/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#ifndef DSS1_H_INCLUDED
#define DSS1_H_INCLUDED
#include <functional>
#include "MidiDevice.h"

using ProgressCallback = std::function<bool(std::string objectBeingRead, uint32_t bytesSoFar, uint32_t totalBytes, bool isDone)>;


#if 0
struct MultisoundNameListItem
{
	MultisoundNameListItem(){};
	MultisoundNameListItem(const char* pName, uint32_t len)
		:name(pName, 8)
		, length(len)
	{}

	std::string name;
	uint32_t	length;
};
#endif // 0



/**	The DSS1 class represents the DSS-1 keyboard.

	A design decision was where to put the code that sends and retrieves stuff from the DSS-1. 
	I decided that the code would go in the DSS1 class. The other classes System, Multisound, 
	and Program would hold the data and would give up the data in raw form when asked as oppoesed to 
	those class knowing how to save themselves to the DSS1 (essentially the MIDI device though). 
	This has the following advantages. Those three oobject can be passed around the program as data and 
	can be thought of as independent of a device. It also adds some consistency in that funtions like GetMode() 
	and SetPlayMode() have to go in the DSS1 class anyway. And now all types of transfers can be in DSS1.
	Further, only the DSS1 class has to know about the MidiIODevice and MIDI channel number, not all the 
	data classes. Yes, it seems more object oriented to have data class stream their own data out themselves. 
	So for example, a Program could stream it's own data out to either the device or a file or wherever. 
	But so far I think the advantages of having the transfer code in the DSS1 class outweigh that.

	std::vector<uint8_t> ReceiveProgramParamData(int programIndex);
	void SendProgramParamData(int programIndex, string name, std::vector<uint8_t>);

	std::vector<uint8_t> ReceiveMultisoundParamData(int multisoundIndex);
	void SendMultisoundParamData(int multisoundIndex, std::vector<uint8_t>);

	std::vector<uint8_t> ReceivePCMData(int programIndex);
	void SendPCMData(std::vector<uint8_t>, uint32_t startAddress, uint32_t length);


	ProgramParamsRetrieve
	MultisoundSend
	MultisoundRetrieve

*/
class DSS1
{
public:
	DSS1(MidiIoDevice* pDevice, uint8_t midiChannel = 0);

	/** @brief Retrieves the current mode of the DSS-1
		
		Mode numbers are as follows: @par
		0 = play\n
		1 = sample\n
		2 = edit\n
		3 = create waveform\n
		4 = multi\n
		5 = midi\n
		6 = system\n
		7 = disk utility\n
		8 = program parameter\n
		
		@throws InvalidOperationException	If the device is not open or a connection to the DSS-1
											cannot be made. The message parameter will tell you which.
		@return The current mode number.
	*/	
	int GetMode();
	void SetPlayMode();


	bool IsConnected();
	DSS1& operator=(const DSS1& other) = delete;	// DSS1 don't need to be copyable. It's easy to create one as it's rather lightweight. Lots of code but not data.


	/**	@brief Retrieves all the data currently in the DSS1 and returns it in the form of a System.

		The system is allocated on the heap and the caller becomes the owner of the system returned.
		The "system" returned is just made up of whatever is currently in the DSS1. So the systen doesn't have a 
		designation A, B, C, or D.

		@param[in] callback		A callback function of type ProgressCallback that will be called to report progress.

		@returns	A unique pointer the the system created. The caller becomes the owner.
	*/
	//SystemUPtr ReceiveSystem(ProgressCallback callback);


	/**	@brief Sends all the data in a System to the DSS1, replacing whatever was currently in its memory.

	@param[in] pSystem		A pointer to the System to send.
	@param[in] callback		A callback function of type ProgressCallback that will be called to report progress.
							The callback function must not alter the system during transmission.
	*/
	///void SendSystem(System* pSystem, ProgressCallback callback){ pSystem; callback; };


	std::vector<std::string> DSS1::GetProgramNameList();

	//std::vector<MultisoundNameListItem> GetMultisoundNameList();
	std::vector<uint8_t> GetMultisoundNameList();
	std::vector<uint8_t> GetProgramParamData(int programIndex);
	std::vector<uint8_t> GetMultisoundParamData(int multisoundIndex);

	void SendMultisoundNameList(uint8_t numberOfMultisounds, std::vector<uint8_t> nameList);
	void SendProgramParamData(uint8_t programIndex, const std::string& name, const std::vector<uint8_t>& paramData);
	void SendMultisoundParamData(const std::vector<uint8_t>& paramData);

	/** @brief Retrieves a block of PCM data from the DSS-1
		
		@param[in] startAddress		the starting sample address to get.
		@param[in] length			the length in samples to get.  (A sample is 2 bytes)
		@param[out] outBuffer		a buffer that will receive the PCM data
		@param[in] startIndex		the byte index within the outBuffer to put the data.
	**/
	void GetPCMData(uint32_t startAddress, uint32_t lengthInSamples, std::vector<uint8_t>& outBuffer, uint32_t startIndex = 0);
	
	/** @brief Sends a block of PCM data to the DSS-1

		@param[in] startAddress		the starting sample address where the data should be put in DSS-1 memory.
		@param[in] length			the length in samples to send.  (A sample is 2 bytes)
		@param[in] inBuffer			a buffer containing the PCM data to send.
		@param[in] startIndex		the byte index within the inBuffer where the data to be sent starts.
	**/
	void SendPCMData(uint32_t startAddress, uint32_t lengthInSamples, std::vector<uint8_t>& inBuffer, uint32_t startIndex = 0);

	void ProgramChange(uint8_t programNumber);
private:
	//std::vector<uint8_t> GetProgramData(uint8_t programNumber);


	MidiIoDevice* m_pDevice;
	uint8_t m_midiChannel;

};

/** @brief Given a pointer to six bytes of data that holds DSS-1 a length or address, returns the actual value.

*/
static inline uint32_t ToAddress(uint8_t* pLen)
{
	uint32_t value = (*pLen++ & 0x7F) << 1;
	value += (*pLen++ & 0x40) >> 6;
	value += (*pLen++ & 0x7F) << 9;
	value += (*pLen++ & 0x40) << 2;
	value += (*pLen++ & 0x03) << 17;
	value += (*pLen++ & 0x40) << 10;

	return value;
}
static inline uint32_t ToLength(uint8_t* pLen)
{
	return ToAddress(pLen);
}

static inline uint16_t ToWord(uint8_t* pLen)
{
	return (*pLen >> 2) + (*(pLen + 1) << 5);
}


static inline void FromAddress(uint8_t* pBuffer, uint32_t value)
{
	*pBuffer++ = (uint8_t)((value & 0xFE) >> 1);
	*pBuffer++ = (uint8_t)((value & 0x01) << 6);
	*pBuffer++ = (uint8_t)((value & 0xFE00) >> 9);
	*pBuffer++ = (uint8_t)((value & 0x0100) >> 2);
	*pBuffer++ = (uint8_t)((value & 0x060000) >> 17);
	*pBuffer = (uint8_t)((value & 0x010000) >> 10);
}

static inline void FromLength(uint8_t* pBuffer, uint32_t value)
{
	FromAddress(pBuffer, value);
}

#endif  // DSS1_H_INCLUDED
