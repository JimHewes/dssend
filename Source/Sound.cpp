/*
  ================================================================================

  This file is part of DSSend, a program for sending and receiving systems on the 
  Korg DSS-1 synthesizer.
  Copyright (c) 2016 - Jim Hewes

  DSSend is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  DSSend is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with DSSend.  If not, see <http://www.gnu.org/licenses/>.

  ===============================================================================
*/

#include <cassert>
#include <math.h>
#include "Sound.h"
#include "DSS1.h"	// for ToAddress and ToLength

using std::vector;
using std::string;

Sound::Sound()
	:m_params(36)
{}

Sound::Sound(const Sound& other)
	: m_name(other.m_name)
	, m_params(other.m_params)
	, m_pcmData(other.m_pcmData)
{
	assert(m_params.size() == 36);
}

Sound::Sound(const Sound&& other)
	: m_name(std::move(other.m_name))
	, m_params(std::move(other.m_params))
	, m_pcmData(std::move(other.m_pcmData))
{
	assert(m_params.size() == 36);
}

Sound::Sound(const string& name, uint32_t PCMOffset, const vector<uint8_t>& params, const vector<uint8_t>& pcmData)
	: m_name(name)
	, m_params(params)
	, m_pcmData(pcmData)
{

	uint32_t oldLoopStart = ToAddress(&m_params[23]);
	FromAddress(&m_params[23], oldLoopStart - PCMOffset);
	assert(m_params.size() == 36);
}

Sound::Sound(string&& name, uint32_t PCMOffset, vector<uint8_t>&& params, vector<uint8_t>&& pcmData)
	: m_name(std::move(name))
	, m_params(std::move(params))
	, m_pcmData(std::move(pcmData))
{
	uint32_t oldLoopStart = ToAddress(&m_params[23]);
	FromAddress(&m_params[23], oldLoopStart - PCMOffset);
	assert(m_params.size() == 36);
}


void Sound::SetParams(const std::vector<uint8_t>& params)
{
	assert(params.size() == 36);
	m_params = params;
}

void Sound::SetParams(const std::vector<uint8_t>&& params)
{
	assert(params.size() == 36);
	m_params = std::move(params);
}

void Sound::SetPCMData(const std::vector<uint8_t>& pcmData)
{
	m_pcmData = pcmData;
}

void Sound::SetPCMData(const std::vector<uint8_t>&& pcmData)
{
	m_pcmData = std::move(pcmData);
}

void Sound::SetName(const std::string& name)
{
	m_name = name;
}

void Sound::SetName(std::string&& name)
{
	m_name = std::move(name);
}

std::string Sound::GetName()
{
	return m_name;
}

uint32_t Sound::GetSamplesPerSecond()
{
	assert(m_params.size() == 36);
	uint8_t rate = m_params[35] & 0x3F;
	assert(rate < 4);
	return	(rate == 0) ? 32000 :
			(rate == 1) ? 24000 :
			(rate == 2) ? 16000 : 48000;
}

/** Returns the MIDI note number of the sound.
*/
uint8_t Sound::GetOriginalMIDIKey()
{
	return m_params[1];
}

/** Returns the highest MIDI note number at which this sound plays.
*/
uint8_t Sound::GetTopMIDIKey()
{
	return m_params[0];
}

int8_t Sound::GetRelativeTune()
{
	// The manual states that the range of -63 to +63 is equivalent to -50 cents and +50 cents.
	// So here we convert to cents and round off. The scale we're given is:
	// 1 = -63
	// 64 = 0
	// 127 = +63
	return (int8_t)round((float(m_params[3]) - 64) * 50.0/63.0);
}

int8_t Sound::GetRelativeLevel()
{
	return m_params[4];
}

bool Sound::IsTransposed()
{
	assert(m_params.size() == 36);
	return (m_params[35] & 0xC0) == 0;
}


uint32_t Sound::GetLoopStart()
{
	return ToAddress(&m_params[23]);
}

uint32_t Sound::GetLoopLength()
{
	return ToLength(&m_params[29]);
}


std::vector<int16_t> Sound::GetPCMData()
{
	std::vector<int16_t> tmp;

	for (size_t i = 0; i < m_pcmData.size(); i += 2)
	{
		// store as little-endian, signed 16 bit values
		int16_t val = (((int16_t)m_pcmData[i + 1] & 0x7F) << 5) + (((int16_t)m_pcmData[i] & 0x7C) >> 2);
		val -= 2048;	// "normalize"
		val *= 16;		// convert 12-bit to 16-bit
		tmp.push_back(val);
	}
	return tmp;
}

void Sound::ToWave(std::ostream& os)
{

	os << "RIFF";
	
	// We can write the length of the whole file now because can predict (calculate) what it will be right away.
	// 4 for the rest of the RIFF header
	// 24 for the "fmt " header subchunk
	// 8 plus the size in bytes of PCM data for the "data" subchunk
	uint32_t chunkSize = m_pcmData.size() + 4 + 24 + 8;
	os.write((char*)&chunkSize, sizeof(uint32_t));
	os << "WAVE";	// RIFF type

	uint32_t sampleRate =  GetSamplesPerSecond();
	uint32_t avgBytesPerSecond = sampleRate * 2;	// average bytes per second
	uint16_t compressionType = 1;		// compression type
	uint16_t numChannels = 1;			// number of channels. Sounds in DSS-1 are always mono.
	uint16_t blockAlign = 2;			// Block align.   SignificantBitsPerSample / 8 * NumChannels 
	uint16_t bitsPerSample = 16;		// bits per sample.

	// Format subchunk
	os << "fmt ";
	os.write((char*)&(chunkSize = 16), sizeof(uint32_t));		// chunk data size
	os.write((char*)&compressionType, sizeof(uint16_t));
	os.write((char*)&numChannels, sizeof(uint16_t));
	os.write((char*)&sampleRate, sizeof(uint32_t));
	os.write((char*)&avgBytesPerSecond, sizeof(uint32_t));
	os.write((char*)&blockAlign, sizeof(uint16_t));
	os.write((char*)&bitsPerSample, sizeof(uint16_t));

	// Data subchunk
	os << "data";
	os.write((char*)&(chunkSize = m_pcmData.size()), sizeof(uint32_t));		// chunk data size

	std::vector<int16_t> pcmData = GetPCMData();
	os.write((char*)&pcmData[0], pcmData.size() * sizeof(int16_t));
}
